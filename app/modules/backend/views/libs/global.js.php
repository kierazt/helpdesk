<script>
    var fnLoadingImg = function (gif) {
        return '<img class="page-loading" src="' + static_url + 'images/' + gif + '"></img>';
    };

    var fnClearBind = function (e) {
        if (sessionStorage.token != "abide") {
            // call api
        }
    };

    var fnBeforeLoadPage = function (txt) {
        console.log('load');
        $(window).bind('beforeunload', function () {
            if (txt) {
                return txt;
            } else {
                return 'are you sure you want to leave?';
            }
        });
    };

    var fnToStr = function (value, key, to) {
        if (dev_status) {
            switch (key) {
                case 'success':
                    toastr.success(value, key, {timeOut: to});
                    break;
                case 'warning':
                    toastr.warning(value, key, {timeOut: to});
                    break;
                case 'info':
                    toastr.info(value, key, {timeOut: to});
                    break;
                case 'error':
                    toastr.error(value, key, {timeOut: to});
                    break;
            }
        }
    };

    var fnCloseModal = function () {
        App.startPageLoading();
        $(".modal").hide();
        $('.modal').modal('hide');
        fnResetBtn();
        $("form")[0].reset();
        //fnRefreshDataTable();
        App.stopPageLoading();
    };

    var fnResetBtn = function () {
        App.startPageLoading();
        $("#opt_delete").attr("disabled", true);
        $("#opt_delete").addClass("disabled");

        $("#opt_remove").attr("disabled", true);
        $("#opt_remove").addClass("disabled");

        $("#opt_add").attr("disabled", false);
        $("#opt_add").removeClass("disabled");

        $("#opt_edit").attr("disabled", true);
        $("#opt_edit").addClass("disabled");
        App.stopPageLoading();
    };

    var fnCloseBootbox = function () {
        App.startPageLoading();
        $(".bootbox").hide();
        $(".modal-backdrop").hide();
        fnRefreshDataTable();
    };

    var fnRefreshDataTable = function () {
        App.startPageLoading();
        $(".table").DataTable().ajax.reload();
        $('input[id="select_all"]').prop('checked', false);
        App.stopPageLoading();
    };

    var fnActionId = function (url_post, id, options) {
        $.ajax({
            url: url_post,
            method: "POST", //First change type to method here
            data: formdata,
            success: function (response) {
                fnToStr(options + ' is successfully!', 'success');
                fnRefreshDataTable();
            },
            error: function () {
                fnToStr(options + ' is failed, please try again or call superuser to help!', 'error');
                fnRefreshDataTable();
            }
        });
        return false;
    };

    var fnGetTimtikUser = function () {
        var url_post = base_backend_url + 'prefferences/user/get_user/1';
        $.ajax({
            url: url_post,
            method: "POST",
            success: function (response) {
                $('.timtik').html(response);
            }
        });
        return false;
    };

    var fnGetVendorUser = function () {
        var url_post = base_backend_url + 'prefferences/user/get_user/2';
        $.ajax({
            url: url_post,
            method: "POST",
            success: function (response) {
                $('.vendor').html(response);
            }
        });
        return false;
    };

    var fnTicketAuth = function (status) {
        switch (status) {
            case 'request-to-close':
                $('#sbmt_message').attr('disabled', 'disabled');
                $('#req_to_close').attr('disabled', 'disabled');
                $('#req_to_close').attr('title', 'This ticket already request to be close...');
                break;
            case 'close':
                $('#re_open').show();
                $('#sbmt_message').attr('disabled', 'disabled');
                $('#req_to_close').hide();
                $('.caption-helper').css('color:red');
                break;
            case 'progress':
                $('#re_open').hide();
                $('#mark_as_solve').attr('disabled', 'disabled');
                break;
            case 'open':
                $('#re_open').hide();
                break;
        }
    };

    var fnCheckTicketOpen = function (id) {
        var formdata = {
            id: (id)
        };
        $.ajax({
            url: base_backend_url + 'tickets/master/check_ticket_open/',
            method: "POST", //First change type to method here
            data: formdata,
            success: function (response) {
                if (response == 'false') {
                    fnToStr('Ticket is already open by other user', 'warning');
                    fnCloseModal();
                    return false;
                }
            },
            error: function (response) {
                return false;
            }
        });
    };

    var GlobalAjax = function () {
        return {
            //main function to initiate the module
            init: function () {
                setInterval(function () {
                    var id = $('input[name="ticket_code"]').val();
                    if (id) {
                        var formdata = {
                            ticket_code: Base64.encode(id)
                        };
                        var url_post = base_backend_url + 'tickets/master/check_ticket_timeout/';
                        $.ajax({
                            url: url_post,
                            method: "POST",
                            data: formdata,

                            success: function (response) {
                                // console.log(response);
                                if (response == 'false') {
                                    fnCloseModal();
                                    window.location.reload();
                                }
                            }
                        });
                    }
                }, 15000);
                fnToStr('Global js ready!!!', 'success', 2000);

                $('span#Open').html(open_total);
                $('span#Progress').html(progress_total);
                $('span#Close').html(close_total);
                $('span#CloseRequest').html(close_request_total);

                $('button[type="button"]').on('click', function () {
                    var dismiss = $(this).attr('data-dismiss');
                    App.startPageLoading();
                    switch (dismiss) {
                        case 'modal':
                            $('.modal').modal('hide');
                            fnRefreshDataTable();
                            fnResetBtn();
                            break;
                    }
                    App.stopPageLoading();
                });

                $('#group').on('change', function () {
                    var id = $(this).val();
                    if (id == 1) {
                        $('#timtik_frm').fadeIn();
                        $('#vendor_frm').fadeOut();
                        fnGetTimtikUser();
                    } else if (id == 2) {
                        $('#vendor_frm').fadeIn();
                        $('#timtik_frm').fadeOut();
                        fnGetVendorUser();
                    }
                });

                $('table.table').on('click', '#select_all', function () {
                    var is_checked = $(this).is(':checked');
                    if (is_checked == true) {
                        $('input[type="checkbox"]').prop('checked', true);
                    } else {
                        $('input[type="checkbox"]').prop('checked', false);
                        $('input[id="select_all"]').prop('checked', false);
                    }
                });

                $('table.table').on('click', '.md-check', function () {
                    var id = $(this).attr('data-id');
                    var is_checked = $('input.select_tr').is(':checked');
                    if (is_checked == true) {
                        var count = $('input.select_tr').filter(':checked').length;
                        if (id) {
                            $('input[name="id"]').val(id);
                        }
                        if (count > 1)
                        {
                            $("#opt_delete").attr("disabled", false);
                            $("#opt_delete").removeClass("disabled");

                            $("#opt_remove").attr("disabled", false);
                            $("#opt_remove").removeClass("disabled");

                            $("#opt_add").attr("disabled", true);
                            $("#opt_add").addClass("disabled");

                            $("#opt_edit").attr("disabled", true);
                            $("#opt_edit").addClass("disabled");
                        } else {
                            $("#opt_delete").attr("disabled", false);
                            $("#opt_delete").removeClass("disabled");

                            $("#opt_remove").attr("disabled", false);
                            $("#opt_remove").removeClass("disabled");

                            $("#opt_add").attr("disabled", true);
                            $("#opt_add").addClass("disabled");

                            $("#opt_edit").attr("disabled", false);
                            $("#opt_edit").removeClass("disabled");
                        }
                    } else {
                        $("#opt_delete").attr("disabled", true);
                        $("#opt_delete").addClass("disabled");

                        $("#opt_remove").attr("disabled", true);
                        $("#opt_remove").addClass("disabled");

                        $("#opt_add").attr("disabled", false);
                        $("#opt_add").removeClass("disabled");

                        $("#opt_edit").attr("disabled", true);
                        $("#opt_edit").addClass("disabled");
                    }
                });

                $('table.table').on('click', 'td a.btn', function () {
                    var id = $(this).attr('data-id');
                    var href = $(this).attr('href');
                    var formdata = {
                        ticket_id: (id)
                    };
                    if (href == '#response') {
                        fnCheckTicketOpen(id);
                    }
                    $.ajax({
                        url: base_backend_url + 'tickets/master/get_ticket_detail/',
                        method: "POST", //First change type to method here
                        data: formdata,
                        success: function (data) {
                            var row = JSON.parse(data);
                            var status_ = false;
                            if (row.is_active == 1) {
                                status_ = true;
                            }
                            if (href == '#response') {
                                $.ajax({
                                    url: base_backend_url + 'tickets/master/set_open/',
                                    method: "POST", //First change type to method here
                                    data: formdata,
                                    success: function (response) {
                                        //assign into response view
                                        $('input[name="ticket_id"]').val(row.id);
                                        $('input[name="ticket_code"]').val(row.code);
                                        $('#code').html(row.code);
                                        $('#create_date').html(row.create_date);
                                        $('#content').html(row.content);
                                        return false;
                                    },
                                    error: function () {
                                        return false;
                                    }
                                });
                            } else if (href == '#detail') {
                                //assign into detail view
                                $('input[name="create_date"]').val(row.create_date);
                                $('input[name="code"]').val(row.code);
                                $('input[name="ticket_status"]').val(row.ticket_status);
                                $('input[name="category_name"]').val(row.category_name);
                                $('input[name="job_category_name"]').val(row.job_category_name);
                                $('textarea[name="content"]').val(row.content);
                                $('textarea[name="description"]').val(row.description);
                                $('.handle_by').html('This ticket is handle by <b>' + row.handle_by + '</b>');
                            } else if (href == '#transfer') {
                                $('input[name="code"]').val(row.code);
                            }
                        },
                        error: function (data) {
                            toastr.success('Failed response this ticket!');
                            return false;
                        }
                    });
                });
                $('#response').on('shown.bs.modal', function (e) {
                    console.log(e);
                    var id = $('input[name="ticket_id"]').val();
                    //alert();return false;
                });
                $("#response").on("hide.bs.modal", function () {
                    var id = $('input[name="ticket_id"]').val();
                    if (id) {
                        var formdata = {
                            ticket_id: Base64.encode(id)
                        };
                        $.ajax({
                            url: base_backend_url + 'tickets/master/set_close/',
                            method: "POST", //First change type to method here
                            data: formdata,
                            success: function (response) {
                                window.removeEventListener("beforeunload", fnClearBind());
                                return false;
                            },
                            error: function (response) {
                                return false;
                            }
                        });
                    }
                });

                $('#sbmt_form_transfer').on('click', function () {
                    alert('wew');
                });

                $('#sbmt_form').on('click', function () {
                    App.startPageLoading();
                    var uri = base_backend_url + 'tickets/master/response_ticket';
                    var ticket_code = $('input[name="ticket_code"]').val();
                    var formdata = {
                        ticket_id: Base64.encode($('input[name="ticket_id"]').val()),
                        ticket_code: ticket_code,
                        message: $('textarea[name="message"]').val()
                    };
                    $.ajax({
                        url: uri,
                        method: "POST", //First change type to method here
                        data: formdata,
                        success: function (response) {
                            App.stopPageLoading();
                            toastr.success('Successfully add new ticket data ');
                            fnCloseModal();
                            fnRefreshDataTable();
                            //window.location.href = base_backend_url + 'tickets/master/tracking/' + Base64.encode(ticket_code);
                            return false;
                        },
                        error: function () {
                            App.stopPageLoading();
                            toastr.success('Failed add new ticket data ');
                            return false;
                        }

                    });
                    return false;
                });

                $('input[type="checkbox"][name="insert_by"]').on('click', function () {
                    var id = $(this).prop("checked");
                    if (id == true) {
                        $.post(base_backend_url + 'tickets/master/get_issue_suggest/' + id, function (data) {
                            $('#issue_template').html(data);
                            $('#insert_from_temp').fadeIn();
                        });
                    } else {
                        $('#insert_from_temp').fadeOut();
                    }
                });

                $('#issue_template').on('change', function () {
                    var value = $(this).val();
                    $('textarea[name="message"]').val(value);
                    $('#issue_template').prop('selectedIndex', 0);
                });

                $('input[type="checkbox"][name="agree"]').on('click', function () {
                    var id = $(this).prop("checked");
                    if (id == true) {
                        $('#sbmt_form').fadeIn();
                    } else {
                        $('#sbmt_form').fadeOut();
                    }
                });

                $('#finish_solving_ticket').on('click', function () {
                    var uri = base_backend_url + 'tickets/master/close_ticket_request';
                    var arr = [];
                    $('.md-check:checked').each(function () {
                        var id = $(this).data('id');
                        arr[id] = $(this).val();
                    });
                    var formdata = {
                        ticket_id: $('input[name="ticket_id"]').val(),
                        msg_job_list: arr,
                        message: $('textarea[name="msg_close_ticket"]').val()
                    };
                    $.ajax({
                        url: uri,
                        type: "post",
                        data: formdata,
                        success: function (response) {
                            App.stopPageLoading();
                            fnCloseModal();
                            window.location = base_url + 'backend/dashboard';
                            return false;
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            App.stopPageLoading();
                            console.log(textStatus, errorThrown);
                            return false;
                        }
                    });
                });
            }
        };
    }();

    jQuery(document).ready(function () {
        GlobalAjax.init();
    });
</script>
