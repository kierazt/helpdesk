<script>
    var getDate = function (element) {
        var date;
        try {
            date = $.datepicker.parseDate(dateFormat, element.value);
        } catch (error) {
            date = null;
        }

        return date;
    };

     var fnReset = function () {
            $("#report_btn_table")[0].reset();   
    }; 

    var Ajax = function () {
        return {
            //main function to initiate the module
            init: function () {
                var dateFormat = "mm/dd/yy",
                        from = $("#from")
                        .datepicker({
                            defaultDate: "+1w",
                            changeMonth: true,
                            numberOfMonths: 1
                        })
                        .on("change", function () {
                            to.datepicker("option", "minDate", getDate(this));
                        }),
                        to = $("#to").datepicker({
                    defaultDate: "+1w",
                    changeMonth: true,
                    numberOfMonths: 1
                })
                .on("change", function () {
                    from.datepicker("option", "maxDate", getDate(this));
                });
                
                $("#report_btn_table").submit(function () {
                    var from_date = $('#from').val();
                    var to_date = $('#to').val();
                    $('#datatable_ajax').fadeIn();

                    var table = $('#datatable_ajax').DataTable({
                        "bDestroy": true,
                        "dom": 'Bfrtip',
                        "scrollX": true,
                        "buttons": [
                            {
                                title: export_file_name,
                                extend: 'print',
                                exportOptions: {
                                    columns: ':visible'
                                }
                            },
                            {
                                title: export_file_name,
                                messageTop: 'Ticket Report',
                                extend: 'pdf',
                                exportOptions: {
                                    columns: ':visible'
                                }
                            },
                            {
                                title: export_file_name,
                                messageTop: 'Ticket Report',
                                extend: 'csv',
                                exportOptions: {
                                    columns: ':visible'
                                }
                            },
                            {
                                title: export_file_name,
                                messageTop: 'Ticket Report',
                                extend: 'excel',
                                exportOptions: {
                                    columns: ':visible'
                                }
                            },
                            'colvis'
                        ],
                        "columnDefs": [{
                                targets: -1,
                                visible: false
                            }],
                        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
                        "sPaginationType": "bootstrap",
                        "paging": true,
                        "pagingType": "full_numbers",
                        "ordering": true,
                        "serverSide": true,
                        "ajax": {
                            url: base_backend_url + 'reports/ticket/get_list/',
                            type: 'POST',
                            data: {
                                "param2": {
                                    "from_date": from_date,
                                    "to_date": to_date
                                }
                            }
                        },
                        "columns": [
                            {"data": "num"},
                            {"data": "code"},
                            {"data": "content"},
                            {"data": "status"},
                            {"data": "priority"},
                            {"data": "category"},
                            {"data": "open_date"},
                            {"data": "open_time"},
                            {"data": "response_date"},
                            {"data": "response_time"},
                            {"data": "closing_date"},
                            {"data": "closing_time"},
                            {"data": "active"}
                        ],
                        "drawCallback": function () {
                            $('.make-switch').bootstrapSwitch();
                        }
                    });
                    return false;
                });

                $('#cancel').on('click',  function () {
                    fnReset();
                });
            }
        };
    }();
    jQuery(document).ready(function () {
        Ajax.init();
    });</script>