<script>
    var fnReset = function () {
            $("#report_btn_table")[0].reset();   
    }; 
    
    var Ajax = function () {
        return {
            //main function to initiate the module
            init: function () {
                $("#report_btn_table").submit(function () {
                    var ticket_status = $('#ticket_status').val();
                    var ticket_priority = $('#ticket_priority').val();
                    var ticket_category = $('#ticket_category').val();
                    $('#datatable_ajax').fadeIn();
                    var table = $('#datatable_ajax').DataTable({
                        "bDestroy": true,
                        "dom": 'Bfrtip',
                        "scrollX": true,
                        "buttons": [
                            {
                                title: export_file_name,
                                extend: 'print',
                                exportOptions: {
                                    columns: ':visible'
                                }
                            },
                            {
                                title: export_file_name,
                                messageTop: 'Ticket Report',
                                extend: 'pdf',
                                exportOptions: {
                                    columns: ':visible'
                                }
                            },
                            {
                                title: export_file_name,
                                messageTop: 'Ticket Report',
                                extend: 'csv',
                                exportOptions: {
                                    columns: ':visible'
                                }
                            },
                            {
                                title: export_file_name,
                                messageTop: 'Ticket Report',
                                extend: 'excel',
                                exportOptions: {
                                    columns: ':visible'
                                }
                            },
                            'colvis'
                        ],
                        "columnDefs": [{
                                targets: -1,
                                visible: false
                            }],
                        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
                        "sPaginationType": "bootstrap",
                        "paging": true,
                        "pagingType": "full_numbers",
                        "ordering": true,
                        "serverSide": true,
                        "ajax": {
                            url: base_backend_url + 'reports/ticket/get_list/',
                            type: 'POST',
                            data: {
                                "param1": {
                                    "ticket_status": ticket_status,
                                    "ticket_priority": ticket_priority,
                                    "ticket_category": ticket_category
                                }
                            }
                        },
                        "columns": [
                            {"data": "num"},
                            {"data": "code"},
                            {"data": "content"},
                            {"data": "status"},
                            {"data": "priority"},
                            {"data": "category"},
                            {"data": "response"},
                            {"data": "closing"},
                            {"data": "vendor"},
                            {"data": "open_date"},
                            {"data": "open_time"},
                            {"data": "response_date"},
                            {"data": "response_time"},
                            {"data": "closing_date"},
                            {"data": "closing_time"},
                            {"data": "active"}
                        ],
                        "drawCallback": function () {
                            $('.make-switch').bootstrapSwitch();
                        }
                    });
                    return false;
                });

                $('#cancel').on('click',  function () {
                    fnReset();
                });
            }
        };
    }();
    jQuery(document).ready(function () {
        Ajax.init();
    });</script>