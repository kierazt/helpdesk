<div class="row">
    <div class="col-md-12">
        <!-- Begin: life time stats -->
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject font-dark sbold uppercase">{view-header-title}</span>
                </div>
                <div class="actions">
                    <div class="btn-group">
                        <a style="font-size:10px; text-align:center" title="Insert new vendor user" class="btn dark btn-outline sbold col-ms-2" data-toggle="modal" data-id="add" href="#modal_add_edit" id="opt_add">
                            <i class="fa fa-plus-square"></i>
                        </a>
                        <a style="font-size:10px; text-align:center" title="Update exist vendor user" class="btn dark btn-outline sbold disabled col-ms-2" data-toggle="modal" data-id="edit" href="#modal_add_edit" id="opt_edit" disabled="">
                            <i class="fa fa-pencil-square-o"></i>
                        </a>
                        <a style="font-size:10px; text-align:center" title="Remove vendor user" class="btn dark btn-outline sbold disabled col-ms-2" data-value="remove" data-id="remove" id="opt_remove" disabled="">
                            <i class="fa fa-remove"></i>
                        </a>
                        <a style="font-size:10px; text-align:center" title="Delete vendor user" class="btn dark btn-outline sbold disabled col-ms-2" data-value="delete" data-id="delete" id="opt_delete" disabled="">
                            <i class="fa fa-trash"></i>
                        </a>
                        <a style="font-size:10px; text-align:center" title="Refresh" class="btn dark btn-outline sbold col-ms-2" data-value="refresh" data-id="refresh" id="opt_refresh">
                            <i class="fa fa-refresh"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="portlet-body">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#tab_reversed_1_1" data-toggle="tab"> Vendor </a>
                    </li>
                    <li>
                        <a href="#tab_reversed_1_2" data-toggle="tab"> Vendor user </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade active in" id="tab_reversed_1_1">
                        <div class="table-container">
                            <table class="table table-striped table-bordered table-hover table-checkable" id="data_vendor">
                                <thead>
                                    <tr role="row" class="heading">
                                        <th width="2%"> No </th>
                                        <th width="15%"> User Name </th>
                                        <th width="15%"> Phone Number </th>
                                        <th width="15%"> Fax </th>
                                        <th width="15%"> Email </th>
                                        <th width="15%"> Status </th>
                                        <th width="15%"> Action </th>
                                    </tr>							
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="tab_reversed_1_2">
                        <div class="table-container">
                            <table class="table table-striped table-bordered table-hover table-checkable" id="data_vendor_user">
                                <thead>
                                    <tr role="row" class="heading">
                                        <th width="2%">
                                            <div class="form-group form-md-checkboxes">
                                                <div class="md-checkbox-list">
                                                    <div class="md-checkbox">
                                                        <input type="checkbox" id="select_all" name="select_all" class="md-check">
                                                        <label for="select_all">
                                                            <span></span>
                                                            <span class="check" style="left:20px;"></span>
                                                            <span class="box" style="left:14px;"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </th>
                                        <th width="5%"> # </th>
                                        <th width="15%"> User Name </th>
                                        <th width="15%"> Vendor Code </th>
                                        <th width="15%"> Vendor Name </th>
                                        <th width="15%"> Email </th>
                                        <th width="15%"> Group Name </th>
                                        <th width="15%"> Action </th>
                                    </tr>							
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
</div>
<!-- /.modal -->
<div id="modal_add_edit" class="modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form method="POST" id="add_edit">
                <div class="modal-header">
                    <button type="button" class="close" data-action="close-modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="title_mdl"></h4>
                </div>
                <div class="modal-body">
                    <div class="scroller" style="height:300px" data-always-visible="1" data-rail-visible1="1">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">NIK</label>
                                    <div class="input-icon right">
                                        <i class="fa fa-info-circle tooltips" data-container="body"></i>
                                        <input class="form-control" type="text" name="nik" /> 
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">User Name</label>
                                    <div class="input-icon right">
                                        <i class="fa fa-info-circle tooltips"  data-container="body"></i>
                                        <input class="form-control" type="text" name="user_name" /> 
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">First Name</label>
                                    <div class="input-icon right">
                                        <i class="fa fa-info-circle tooltips" data-container="body"></i>
                                        <input class="form-control" type="text" name="first_name" /> 
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Last Name</label>
                                    <div class="input-icon right">
                                        <i class="fa fa-info-circle tooltips" data-container="body"></i>
                                        <input class="form-control" type="text" name="last_name" /> 
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Email</label>
                                    <div class="input-icon right">
                                        <i class="fa fa-info-circle tooltips" data-original-title="Email address" data-container="body"></i>
                                        <input class="form-control" type="text" name="email" /> 
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                 <div class="form-group">
                                    <label class="control-label">Phone Number</label>
                                    <div class="input-icon right">
                                        <i class="fa fa-info-circle tooltips" data-container="body"></i>
                                        <input class="form-control" type="text" name="phone" /> 
                                    </div>
                                </div>
                                <div class="form-group" id="div_password">
                                    <label class="control-label">Set a password</label>
                                    <div class="input-icon right">
                                        <i class="fa fa-info-circle tooltips" data-container="body"></i>
                                        <input class="form-control" type="text" name="password" /> 
                                    </div>
                                </div>
                                <div class="form-group" id="div_user_group">
                                    <label class="control-label">User Group</label>
                                    <div class="input-icon right">
                                        <i class="fa fa-info-circle tooltips" data-original-title="Vendor" data-container="body"></i>
                                        <input class="form-control" type="text" name="group" placeholder="Vendor" readonly=""/> 
                                    </div>
                                </div>
                                <div class="form-group" id="div_vendor">
                                    <label>Vendor</label>
                                    <select class="form-control" name="vendor" id="vendor">
                                        <option>-- select one--</option>
                                    </select>
                                </div>
                                <div class="form-group" style="height:30px">
                                    <label>Active</label><br/>
                                    <input type="checkbox" class="make-switch" data-size="small" name="status"/>
                                </div><br/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="text" name="id" hidden />
                    <button type="button" data-action="close-modal" class="btn dark btn-outline">Close</button>
                    <button type="submit" class="btn green" id="saveBtn">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- /.modal -->
<div class="modal fade bs-modal-lg" id="contract" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><?php echo $this->lang->line('global_contract'); ?></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label"><?php echo $this->lang->line('global_ticket_code'); ?></label>
                            <div class="input-icon right">
                                <i class="fa fa-info-circle tooltips" data-original-title="<?php echo $this->lang->line('global_ticket_code'); ?>" data-container="body"></i>
                                <input class="form-control" type="text" name="code" readonly=""/> 
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label"><?php echo $this->lang->line('global_ticket_create_date'); ?></label>
                            <div class="input-icon right">
                                <i class="fa fa-info-circle tooltips" data-original-title="<?php echo $this->lang->line('global_ticket_create_date'); ?>" data-container="body"></i>
                                <input class="form-control" type="text" name="create_date"  readonly=""/> 
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label"><?php echo $this->lang->line('global_ticket_status'); ?></label>
                            <div class="input-icon right">
                                <i class="fa fa-info-circle tooltips" data-original-title="<?php echo $this->lang->line('global_ticket_status'); ?>" data-container="body"></i>
                                <input class="form-control" type="text" name="ticket_status"  readonly=""/> 
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label><?php echo $this->lang->line('global_content'); ?></label>
                            <textarea class="form-control" rows="3" name="content" readonly=""></textarea>
                        </div>
                        <div class="form-group">
                            <label><?php echo $this->lang->line('global_description'); ?></label>
                            <textarea class="form-control" rows="3" name="description" readonly=""></textarea>
                        </div>
                    </div>
                </div>
            </div>
			<div class="modal-footer">
				<input type="text" name="id" hidden />
				<button type="button" data-action="close-modal" class="btn dark btn-outline">Close</button>
				<button type="submit" class="btn green" id="saveBtn">Save changes</button>
			</div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade bs-modal-lg" id="job_category" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><?php echo $this->lang->line('global_job_category'); ?></h4>
            </div>
            <div class="modal-body">
                <div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label">NIK</label>
							<div class="input-icon right">
								<i class="fa fa-info-circle tooltips" data-container="body"></i>
								<input class="form-control" type="text" name="nik" /> 
							</div>
						</div>
						<div class="form-group">
							<label class="control-label">User Name</label>
							<div class="input-icon right">
								<i class="fa fa-info-circle tooltips"  data-container="body"></i>
								<input class="form-control" type="text" name="user_name" /> 
							</div>
						</div>
						<div class="form-group">
							<label class="control-label">First Name</label>
							<div class="input-icon right">
								<i class="fa fa-info-circle tooltips" data-container="body"></i>
								<input class="form-control" type="text" name="first_name" /> 
							</div>
						</div>
						<div class="form-group">
							<label class="control-label">Last Name</label>
							<div class="input-icon right">
								<i class="fa fa-info-circle tooltips" data-container="body"></i>
								<input class="form-control" type="text" name="last_name" /> 
							</div>
						</div>
						<div class="form-group">
							<label class="control-label">Email</label>
							<div class="input-icon right">
								<i class="fa fa-info-circle tooltips" data-original-title="Email address" data-container="body"></i>
								<input class="form-control" type="text" name="email" /> 
							</div>
						</div>
					</div>
					<div class="col-md-6">
                        <div class="form-group">
							<label>Category</label>
							<select class="form-control category" name="category">
								<option>-- select one --</option>
								<?php if(isset($category) && !empty($category)):?>
									<?php foreach($category AS $key => $value):?>
										<option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
									<?php endforeach;?>
								<?php endif;?>
							</select>
						</div>
						<div class="form-group">
							<label>Job</label>
							<select class="form-control job" name="job" id="job">
								<option>-- select category first --</option>
							</select>
						</div>
                    </div>
                    
                </div>
            </div>			
			<div class="modal-footer">
				<input type="text" name="id" hidden />
				<button type="submit" class="btn green" id="assignBtn">Assign this Category to user</button>
			</div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>