<script>
    var tbl_1 = function (id) {
        if (id == 1) {
            var table = $('#data_vendor').DataTable({
                "bDestroy": true,
                "lengthMenu": [[10, 25, 50], [10, 25, 50]],
                "sPaginationType": "bootstrap",
                "paging": true,
                "pagingType": "full_numbers",
                "ordering": false,
                "serverSide": true,
                "ajax": {
                    url: base_backend_url + 'accounts/vendor/get_list/',
                    type: 'POST'
                },
                "columns": [
                    {"data": "num"},
                    {"data": "name"},
                    {"data": "phone_number"},
                    {"data": "fax"},
                    {"data": "email"},
                    {"data": "active"},
                    {"data": "action"}
                ],
                "drawCallback": function () {
                    $('.make-switch').bootstrapSwitch();
                }
            });
        } else {
            var table = $('#data_vendor_user').DataTable({
                "bDestroy": true,
                "lengthMenu": [[10, 25, 50], [10, 25, 50]],
                "sPaginationType": "bootstrap",
                "paging": true,
                "pagingType": "full_numbers",
                "ordering": false,
                "serverSide": true,
                "ajax": {
                    url: base_backend_url + 'accounts/vendor/get_vendor_user_list/',
                    type: 'POST'
                },
                "columns": [
                    {"data": "rowcheck"},
                    {"data": "num"},
                    {"data": "username"},
                    {"data": "vendor_code"},
                    {"data": "vendor_name"},
                    {"data": "email"},
                    {"data": "group_name"},
                    {"data": "action"}
                ],
                "drawCallback": function () {
                    $('.make-switch').bootstrapSwitch();
                }
            });
        }
    };

    var Ajax = function () {
        return {
            //main function to initiate the module
            init: function () {
                $('table.display').dataTable();
                tbl_1(1);
                $('a').on('click', function () {
                    var toggle = $(this).data('toggle');
                    var href = $(this).attr('href');
                    if (toggle && toggle == 'tab') {
                        if (href == '#tab_reversed_1_2') {
                            tbl_1(2);
                        }
                    }
                });
                $('.category').on('change', function () {
                    var id = $(this).val();
                    var formdata = {
                        id: Base64.encode(id)
                    };
                    $.ajax({
                        url: base_backend_url + 'accounts/vendor/get_category/',
                        method: "POST", //First change type to method here
                        data: formdata,
                        success: function (response) {
                            $('.job').html(response);
                        },
                        error: function () {
                            toastr.error('Failed ' + response);
                            return false;
                        }

                    });
                });
                $('table#data_vendor_user').on('click', 'td a.btn', function () {
                    var id = $(this).attr('data-id');
                    var formdata = {
                        id: (id)
                    };
                    $.ajax({
                        url: base_backend_url + 'accounts/vendor/get_data/',
                        method: "POST", //First change type to method here
                        data: formdata,
                        success: function (response) {
                            var row = JSON.parse(response);
                            $('input[name="id"]').val(row.id);
                            $('input[name="nik"]').val(row.nik);
                            $('input[name="user_name"]').val(row.username);
                            $('input[name="first_name"]').val(row.first_name);
                            $('input[name="last_name"]').val(row.last_name);
                            $('input[name="email"]').val(row.email);
                            return false;
                        },
                        error: function () {
                            toastr.error('Failed ' + response);
                            return false;
                        }

                    });
                });

                $('#datatable_ajax').on('switchChange.bootstrapSwitch', 'input[name="status"]', function (event, state) {
                    bootbox.confirm("Are you sure?", function (result) {
                        var id = $(this).attr('data-id');
                        var formdata = {
                            id: Base64.encode(id),
                            active: state
                        };
                        $.ajax({
                            url: base_backend_url + 'accounts/vendor/update_status/',
                            method: "POST", //First change type to method here
                            data: formdata,
                            success: function (response) {
                                toastr.success('Successfully ' + response);
                                return false;
                            },
                            error: function () {
                                toastr.error('Failed ' + response);
                                return false;
                            }
                        });
                    });
                });

                $('a.btn').on('click', function () {
                    var action = $(this).attr('data-id');
                    var count = $('input.select_tr:checkbox').filter(':checked').length;
                    switch (action) {
                        case 'add':
                            $('.modal-title').html('Insert New User Vendor');
                            $.ajax({
                                url: base_backend_url + 'accounts/vendor/get_vendors/',
                                method: "POST", //First change type to method here
                                success: function (response) {
                                    $('#vendor').html(response);
                                },
                                error: function () {
                                    fnToStr('Error is occured, please contact administrator.', 'error');
                                }
                            });
                            break;

                        case 'edit':
                            $('.modal-title').html('Update Exist User');
                            var status_ = $(this).hasClass('disabled');
                            var id = $('input.select_tr:checkbox:checked').attr('data-id');
                            console.log(status_);
                            if (status_ == 0) {
                                var formdata = {
                                    id: Base64.encode(id)
                                };
                                $.ajax({
                                    url: base_backend_url + 'accounts/vendor/get_data/',
                                    method: "POST", //First change type to method here
                                    data: formdata,
                                    success: function (response) {
                                        var row = JSON.parse(response);
                                        var status_ = false;
                                        if (row.is_active == 1) {
                                            status_ = true;
                                        }
                                        $('#div_password').fadeOut();
                                        $('#div_user_group').fadeOut();
                                        $('#div_vendor').fadeOut();
                                        console.log(row);
                                        $('input[name="id"]').val(row.id);
                                        $('input[name="nik"]').val(row.nik);
                                        $('input[name="user_name"]').val(row.username);
                                        $('input[name="first_name"]').val(row.first_name);
                                        $('input[name="last_name"]').val(row.last_name);
                                        $('input[name="email"]').val(row.email);
                                        $('input[name="phone"]').val(row.phone_number);
                                        $("[name='status']").bootstrapSwitch('state', status_);
                                        $('#modal_add_edit').modal('show');
                                    },
                                    error: function () {
                                        fnToStr('Error is occured, please contact administrator.', 'error');
                                    }
                                });
                                return false;
                            }
                            break;

                        case 'remove':
                            bootbox.confirm("Are you sure to remove this id?", function (result) {
                                if (result == true) {
                                    var uri = base_backend_url + 'accounts/vendor/remove/';
                                    if (count > 1) {
                                        var ids = [];
                                        $("input.select_tr:checkbox:checked").each(function () {
                                            ids.push($(this).data("id"));
                                        });
                                    } else {
                                        var ids = $('input.select_tr:checkbox:checked').attr('data-id');
                                    }
                                    fnActionId(uri, ids, 'remove');
                                    fnRefreshDataTable();
                                    fnResetBtn();
                                } else {
                                    fnToStr('You re cancelling remove this id', 'info');
                                    fnRefreshDataTable();
                                    fnResetBtn();
                                }
                            });
                            break;

                        case 'delete':
                            bootbox.confirm("Are you sure to delete this id?", function (result) {
                                if (result == true) {
                                    var uri = base_backend_url + 'accounts/vendor/delete/';
                                    if (count > 1) {
                                        id = [];
                                        $("input.select_tr:checkbox:checked").each(function () {
                                            id.push($(this).data("id"));
                                        });
                                    }
                                    fnActionId(uri, id, 'remove');
                                    fnRefreshDataTable();
                                    fnResetBtn();
                                } else {
                                    fnToStr('You re cancelling delete this id', 'info');
                                    fnRefreshDataTable();
                                    fnResetBtn();
                                }
                            });
                            break;

                        case 'refresh':
                            fnRefreshDataTable();
                            break;
                    }
                });
                $('#assignBtn').on('click', function (e) {
                    var id = Base64.encode($('input[name="id"]').val());
                    var uri = base_backend_url + 'accounts/vendor/assign_category_user/';
                    var formdata = {
                        id: id,
                        job: $('#job').val()
                    }
                    console.log(formdata);
                    $.ajax({
                        url: uri,
                        method: "POST", //First change type to method here
                        data: formdata,
                        success: function (response) {
                            toastr.success('Successfully');
                            fnCloseModal();
                            fnRefreshDataTable();
                        },
                        error: function () {
                            toastr.error('Failed');
                            fnCloseModal();
                            fnRefreshDataTable();
                        }
                    });
                });

                $("#add_edit").submit(function (e) {
                    e.preventDefault();
                    var id = Base64.encode($('input[name="id"]').val());
                    var is_active = $("[name='status']").bootstrapSwitch('state');
                    var uri = base_backend_url + 'accounts/vendor/insert_user/';
                    var formdata = {
                        nik: $('input[name="nik"]').val(),
                        username: $('input[name="user_name"]').val(),
                        first_name: $('input[name="first_name"]').val(),
                        last_name: $('input[name="last_name"]').val(),
                        email: $('input[name="email"]').val(),
                        phone: $('input[name="phone"]').val(),
                        password: Base64.encode($('input[name="password"]').val()),
                        group: 3,
                        vendor: $('#vendor').val(),
                        active: is_active
                    };
                    if (id) {
                        uri = base_backend_url + 'accounts/vendor/update_user/';
                        formdata = {
                            id: id,
                            nik: $('input[name="nik"]').val(),
                            username: $('input[name="user_name"]').val(),
                            first_name: $('input[name="first_name"]').val(),
                            last_name: $('input[name="last_name"]').val(),
                            email: $('input[name="email"]').val(),
                            phone: $('input[name="phone"]').val(),
                            active: is_active
                        }
                    }
                    $.ajax({
                        url: uri,
                        method: "POST", //First change type to method here
                        data: formdata,
                        success: function (response) {
                            toastr.success('Successfully');
                            fnCloseModal();
                            fnRefreshDataTable();
                        },
                        error: function () {
                            toastr.error('Failed');
                            fnCloseModal();
                            fnRefreshDataTable();
                        }
                    });
                    return false;
                });
            }
        };

    }();

    jQuery(document).ready(function () {
        Ajax.init();
    });
</script>