<script>
    var TableDatatablesAjax = function () {
        return {
            //main function to initiate the module
            init: function () {
                var table = $('#datatable_ajax').DataTable({
                    "lengthMenu": [[10, 25, 50], [10, 25, 50]],
                    "sPaginationType": "bootstrap",
                    "paging": true,
                    "pagingType": "full_numbers",
                    "ordering": false,
                    "serverSide": true,
                    "ajax": {
                        url: base_backend_url + 'accounts/admin/get_list/',
                        type: 'POST'
                    },
                    "columns": [
                        {"data": "rowcheck"},
                        {"data": "num"},
                        {"data": "nik"},
                        {"data": "username"},
                        {"data": "first_name"},
                        {"data": "last_name"},
                        {"data": "email"},
                        {"data": "group_name"},
                        {"data": "active"}
                    ],
                    "drawCallback": function (master) {
                        $('.make-switch').bootstrapSwitch();
                    }
                });

                $('#datatable_ajax').on('switchChange.bootstrapSwitch', 'input[name="status"]', function (event, state) {
                    bootbox.confirm("Are you sure?", function (result) {
                        var id = $(this).attr('data-id');
                        var formdata = {
                            id: Base64.encode(id),
                            active: state
                        };
                        $.ajax({
                            url: base_backend_url + 'accounts/admin/update_status/',
                            method: "POST", //First change type to method here
                            data: formdata,
                            success: function (response) {
                                toastr.success('Successfully ' + response);
                                return false;
                            },
                            error: function (response) {
                                toastr.error('Failed ' + response);
                                return false;
                            }
                        });
                    });
                });

                $('a.btn').on('click', function () {
                    var action = $(this).attr('data-id');
                    var count = $('input.select_tr:checkbox').filter(':checked').length;
                    switch (action) {
                        case 'add':
                            $('.modal-title').html('Insert New Admin');
                            break;

                        case 'edit':
                            $('.modal-title').html('Update Exist Admin');
                            var status_ = $(this).hasClass('disabled');
                            var id = $('input.select_tr:checkbox:checked').attr('data-id');
                            if (status_ == 0) {
                                var formdata = {
                                    id: Base64.encode(id)
                                };
                                $.ajax({
                                    url: base_backend_url + 'accounts/admin/get_data/',
                                    method: "POST", //First change type to method here
                                    data: formdata,
                                    success: function (response) {
                                        var row = JSON.parse(response);
                                        var status_ = false;
                                        if (row.is_active == 1) {
                                            status_ = true;
                                        }
                                        $('#pswd_frm').hide();
                                        $('input[name="id"]').val(row.id);
                                        $('input[name="nik"]').val(row.nik);
                                        $('input[name="username"]').val(row.username);
                                        $('input[name="first_name"]').val(row.first_name);
                                        $('input[name="last_name"]').val(row.last_name);
                                        $('input[name="email"]').val(row.email);
                                        $("[name='status']").bootstrapSwitch('state', status_);
                                        $('textarea[name="description"]').val(row.description);
                                        $('#modal_add_edit').modal('show');
                                    },
                                    error: function () {
                                        fnToStr('Error is occured, please contact administrator.', 'error');
                                    }
                                });
                                return false;
                            }
                            break;

                        case 'remove':
                            bootbox.confirm("Are you sure to remove this id?", function (result) {
                                if (result == true) {
                                    var uri = base_backend_url + 'accounts/admin/remove/';
                                    if (count > 1) {
                                        var ids = [];
                                        $("input.select_tr:checkbox:checked").each(function () {
                                            ids.push($(this).data("id"));
                                        });
                                    } else {
                                        var ids = $('input.select_tr:checkbox:checked').attr('data-id');
                                    }
                                    fnActionId(uri, ids, 'remove');
                                    fnRefreshDataTable();
                                    fnResetBtn();
                                } else {
                                    fnToStr('You re cancelling remove this id', 'info');
                                    fnRefreshDataTable();
                                    fnResetBtn();
                                }
                            });
                            break;

                        case 'delete':
                            bootbox.confirm("Are you sure to delete this id?", function (result) {
                                if (result == true) {
                                    var uri = base_backend_url + 'accounts/admin/delete/';
                                    if (count > 1) {
                                        id = [];
                                        $("input.select_tr:checkbox:checked").each(function () {
                                            id.push($(this).data("id"));
                                        });
                                    }
                                    fnActionId(uri, id, 'remove');
                                    fnRefreshDataTable();
                                    fnResetBtn();
                                } else {
                                    fnToStr('You re cancelling delete this id', 'info');
                                    fnRefreshDataTable();
                                    fnResetBtn();
                                }
                            });
                            break;

                        case 'refresh':
                            fnRefreshDataTable();
                            break;
                    }
                });

                $("#add_edit").submit(function () {
                    var id = $('input[name="id"]').val();
                    var is_active = $("[name='status']").bootstrapSwitch('state');
                    var uri = base_backend_url + 'accounts/admin/insert/';
                    var txt = 'add new group';
                    var formdata = {
                        nik: $('input[name="nik"]').val(),
                        username: $('input[name="username"]').val(),
                        first_name: $('input[name="first_name"]').val(),
                        last_name: $('input[name="last_name"]').val(),
                        password: Base64.encode($('input[name="password"]').val()),
                        email: $('input[name="email"]').val(),
                        active: is_active
                    };
                    if (id)
                    {
                        uri = base_backend_url + 'accounts/admin/update/';
                        txt = 'update group';
                        formdata = {
                            id: Base64.encode(id),
                            nik: $('input[name="nik"]').val(),
                            username: $('input[name="username"]').val(),
                            first_name: $('input[name="first_name"]').val(),
                            last_name: $('input[name="last_name"]').val(),
                            email: $('input[name="email"]').val(),
                            active: is_active
                        };
                    }
                    $.ajax({
                        url: uri,
                        method: "POST", //First change type to method here
                        data: formdata,
                        success: function (response) {
                            toastr.success('Successfully ' + txt);
                            fnCloseModal();
                            fnRefreshDataTable();
                        },
                        error: function () {
                            toastr.error('Failed ' + txt);
                            fnCloseModal();
                            fnRefreshDataTable();
                        }

                    });
                    return false;
                });
            }
        };

    }();

    jQuery(document).ready(function () {
        TableDatatablesAjax.init();
    });
</script>