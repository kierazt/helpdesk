<script>
    var Ajax = function () {
        return {
            //main function to initiate the module
            init: function () {
                toastr.success('Dashboard js ready!!!');
                var table = $('#ticket_dttable').DataTable({
                    "lengthMenu": [[10, 25, 50], [10, 25, 50]],
                    "sPaginationType": "bootstrap",
                    "paging": true,
                    "pagingType": "full_numbers",
                    "ordering": false,
                    "processing": true,
                    "language": {
                        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
                    },
                    "serverSide": true,
                    "ajax": {
                        url: base_backend_url + 'tickets/master/get_list/',
                        type: 'POST'
                    },
                    "columns": [
                        {"data": "num"},
                        {"data": "code"},
                        {"data": "content"},
                        {"data": "category_name"},
                        {"data": "job_category_name"},
                        {"data": "create"},
                        {"data": "status"},
                        {"data": "action"}
                    ],
                    "drawCallback": function (master) {
                        $('.make-switch').bootstrapSwitch();
                    }
                });
                var table2 = $('#history_dttable').DataTable({
                    "lengthMenu": [[10, 25, 50], [10, 25, 50]],
                    "sPaginationType": "bootstrap",
                    "paging": true,
                    "pageLength": 10,
                    "autoWidth": false,
                    "pagingType": "full_numbers",
                    "ordering": false,
                    "processing": true,
                    "language": {
                        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
                    },
                    "serverSide": true,
                    "ajax": {
                        url: base_backend_url + 'prefferences/user/get_history/',
                        type: 'POST'
                    },
                    "columns": [
                        {"data": "activities", "width": "80%"},
                        {"data": "date", "width": "20%"}
                    ]
                });

            }
        };

    }();

    jQuery(document).ready(function () {
        Ajax.init();
    });

</script>
