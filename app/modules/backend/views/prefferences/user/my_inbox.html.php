<div class="inbox">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-2">
                <div class="inbox-sidebar">
                    <a href="javascript:;" data-title="Compose" class="btn red compose-btn btn-block">
                        <i class="fa fa-edit"></i> Compose 
                    </a>
                    <ul class="inbox-nav">
                        <li class="active">
                            <a href="javascript:;" data-type="inbox" data-title="Inbox"> Inbox
                                <span class="badge badge-success">3</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;" data-type="important" data-title="Inbox"> Important </a>
                        </li>
                        <li>
                            <a href="javascript:;" data-type="sent" data-title="Sent"> Sent </a>
                        </li>
                        <li>
                            <a href="javascript:;" data-type="draft" data-title="Draft"> Draft
                                <span class="badge badge-danger">8</span>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="javascript:;" class="sbold uppercase" data-title="Trash"> Trash
                                <span class="badge badge-info">23</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;" data-type="inbox" data-title="Promotions"> Promotions
                                <span class="badge badge-warning">2</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;" data-type="inbox" data-title="News"> News </a>
                        </li>
                    </ul>                
                </div>
            </div>
            <div class="col-md-10">
                <div class="inbox-body">
					<div class="table-container">
						<table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax_all">
							<thead>
								<tr>
									<th colspan="1">
										<div class="btn-group input-actions">
											<a class="btn btn-sm blue btn-outline dropdown-toggle sbold" href="javascript:;" data-toggle="dropdown"> Actions
												<i class="fa fa-angle-down"></i>
											</a>
											<ul class="dropdown-menu">
												<li>
													<a href="javascript:;">
														<i class="fa fa-pencil"></i> Mark as Read </a>
												</li>
												<li>
													<a href="javascript:;">
														<i class="fa fa-ban"></i> Spam </a>
												</li>
												<li class="divider"> </li>
												<li>
													<a href="javascript:;">
														<i class="fa fa-trash-o"></i> Delete </a>
												</li>
											</ul>
										</div>
									</th>
									<th class="pagination-control" colspan="5">
										<span class="pagination-info"> 1-30 of 789 </span>
										<a class="btn btn-sm blue btn-outline">
											<i class="fa fa-angle-left"></i>
										</a>
										<a class="btn btn-sm blue btn-outline">
											<i class="fa fa-angle-right"></i>
										</a>
									</th>
								</tr>
							</thead>
							<thead>
								<tr role="row" class="heading">
									<th width="2%"><input type="checkbox" data-checkbox="icheckbox_minimal-grey" class="group-checkable" name="select_all"/></th>
									<th width="18%"> Sender </th>
									<th width="35%"> Messages </th>
									<th width="20%"> Attachment </th>
									<th width="15%"> Date Time </th>
								</tr>							
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</div>
            </div>
        </div>
    </div>
</div>