<style>
    #history_dttable_length{
        display:none;
    }

    #history_dttable_filter{
        display:none;
    }

    #history_dttable_info{
        display:none;
    }

    #history_dttable_paginate{
        display:none;
    }
</style>
<div class="row">
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat blue">
            <div class="visual">
                <i class="fa fa-comments"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup"><?php echo isset($_ajax_var_ticket->open) ? $_ajax_var_ticket->open : 0; ?></span>
                </div>
                <div class="desc"> Open Tickets </div>
            </div>
            <a class="more" href="<?php echo base_backend_url('tickets/master/view/open'); ?>" target="__blank"> View more
                <i class="m-icon-swapright m-icon-white"></i>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat red">
            <div class="visual">
                <i class="fa fa-bar-chart-o"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup"><?php echo isset($_ajax_var_ticket->progress) ? $_ajax_var_ticket->progress : 0; ?></span> </div>
                <div class="desc"> Progress Tickets </div>
            </div>
            <a class="more" href="<?php echo base_backend_url('tickets/master/view/progress'); ?>" target="__blank"> View more
                <i class="m-icon-swapright m-icon-white"></i>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat green">
            <div class="visual">
                <i class="fa fa-shopping-cart"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup"><?php echo isset($_ajax_var_ticket->transfer) ? $_ajax_var_ticket->transfer : 0; ?></span>
                </div>
                <div class="desc"> Transfer Tickets </div>
            </div>
            <a class="more" href="<?php echo base_backend_url('tickets/master/view/transfer'); ?>" target="__blank"> View more
                <i class="m-icon-swapright m-icon-white"></i>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat purple">
            <div class="visual">
                <i class="fa fa-globe"></i>
            </div>
            <div class="details">
                <div class="number"> +
                    <span data-counter="counterup"><?php echo isset($_ajax_var_ticket->close) ? $_ajax_var_ticket->close : 0; ?></span> </div>
                <div class="desc"> Close Tickets </div>
            </div>
            <a class="more" href="<?php echo base_backend_url('tickets/master/view/close'); ?>" target="__blank"> View more
                <i class="m-icon-swapright m-icon-white"></i>
            </a>
        </div>
    </div>
    <!-- -->
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat yellow">
            <div class="visual">
                <i class="fa fa-comments"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup"><?php echo isset($_ajax_var_total_employee) ? $_ajax_var_total_employee : 0; ?></span>
                </div>
                <div class="desc"> Employee </div>
            </div>
            <a class="more" href="<?php echo base_backend_url('accounts/immigration/view'); ?>" target="__blank"> View more
                <i class="m-icon-swapright m-icon-white"></i>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat green">
            <div class="visual">
                <i class="fa fa-bar-chart-o"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup"><?php echo isset($_ajax_var_total_kanim) ? $_ajax_var_total_kanim : 0; ?></span></div>
                <div class="desc"> Office Branch </div>
            </div>
            <a class="more" href="<?php echo base_backend_url('tickets/immigration_branch/view'); ?>" target="__blank"> View more
                <i class="m-icon-swapright m-icon-white"></i>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat grey">
            <div class="visual">
                <i class="fa fa-shopping-cart"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup"><?php echo isset($_ajax_var_total_vendor) ? $_ajax_var_total_vendor : 0; ?></span>
                </div>
                <div class="desc"> Vendors </div>
            </div>
            <a class="more" href="<?php echo base_backend_url('accounts/vendor/view'); ?>" target="__blank"> View more
                <i class="m-icon-swapright m-icon-white"></i>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat blue">
            <div class="visual">
                <i class="fa fa-globe"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup"><?php echo isset($_ajax_var_total_ticket_category) ? $_ajax_var_total_ticket_category : 0; ?></span></div>
                <div class="desc"> Categories </div>
            </div>
            <a class="more" href="<?php echo base_backend_url('tickets/category/view'); ?>" target="__blank"> View more
                <i class="m-icon-swapright m-icon-white"></i>
            </a>
        </div>
    </div>

    <div class="col-md-12 col-sm-12">
        <div class="portlet light tasks-widget bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i><?php echo $this->lang->line('global_ticket'); ?>
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"> </a>
                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover table-checkable" id="ticket_dttable">
                    <thead>
                        <tr role="row" class="heading">
                            <th width="5%"> # </th>
                            <th width="15%"> Ticket Code </th>
                            <th width="15%"> Issue </th>
                            <th width="15%"> Category </th>
                            <th width="15%"> Job Category </th>
                            <th width="15%"> Date </th>
                            <th width="15%"> Status </th>
                            <th width="15%"> Action </th>
                        </tr>							
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-5 col-sm-5">
        <div class="portlet light tasks-widget bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i><?php echo $this->lang->line('global_recent_activities'); ?>
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"> </a>
                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover table-checkable" id="history_dttable">
                    <thead>
                        <tr role="row" class="heading">
                            <th></th><th></th>
                        </tr>							
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php
$this->load->view('includes/tools/detail_ticket.html.php');
$this->load->view('includes/tools/response_ticket.html.php');
$this->load->view('includes/tools/transfer_ticket.html.php');
