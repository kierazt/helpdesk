<script>
    var table = $('#datatable_ajax').DataTable({
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        "sPaginationType": "bootstrap",
        "paging": true,
        "pagingType": "full_numbers",
        "ordering": false,
        "serverSide": true,
        "ajax": {
            url: base_backend_url + 'tickets/master/get_list/',
            type: 'POST'
        },
        "columns": [
            {"data": "rowcheck"},
            {"data": "num"},
            {"data": "code"},
            {"data": "content"},
            {"data": "status"},
            {"data": "active"},
            {"data": "description"},
            {"data": "action"}
        ],
        "drawCallback": function (master) {
            $('.make-switch').bootstrapSwitch();
        }
    });

    var fnAutoLoadMessage = function () {
        $.ajaxSetup({cache: false}); // This part addresses an IE bug.  without it, IE will only load the first number and will never refresh

        App.startPageLoading();
        var uri = base_backend_url + 'tickets/master/get_content/';
        var formdata = {
            ticket_id: Base64.encode(ticket_id)
        };
        //setInterval(function () {
        $.ajax({
            url: uri,
            type: "post",
            data: formdata,
            success: function (response) {
                App.stopPageLoading();
                $('.timeline').html(response);
                return false;
            },
            error: function (jqXHR, textStatus, errorThrown) {
                App.stopPageLoading();
                console.log(textStatus, errorThrown);
                return false;
            }
        });
        App.stopPageLoading();
        //}, 5000);
    };

    var fnLoadDetailTicket = function () {
        var uri = base_backend_url + 'tickets/master/get_ticket_detail/';
        var formdata = {
            ticket_id: Base64.encode(ticket_id)
        };
        $.ajax({
            url: uri,
            type: "post",
            data: formdata,
            success: function (response) {
                var row = JSON.parse(response);
                $('input[name="ticket_id"]').val(row.id);
                $('input[name="create_date"]').val(row.create_date);
                $('input[name="code"]').val(row.code);
                $('input[name="ticket_status"]').val(row.ticket_status);
                $('input[name="job_id"]').val(row.job_id);
                $('textarea[name="content"]').val(row.content);
                $('textarea[name="description"]').val(row.description);
                return false;
            },
            error: function (jqXHR, textStatus, errorThrown) {
                App.stopPageLoading();
                console.log(textStatus, errorThrown);
                return false;
            }
        });
    };

    var index = function () {
        return {
            //main function to initiate the module
            init: function () {
                toastr.success('ticket view js ready!!!');
                table;
                fnAutoLoadMessage();
                fnLoadDetailTicket();
                if (ticket_status) {
                    fnTicketAuth(ticket_status);
                }
                $('a.btn').on('click', function () {
                    var toggle = $(this).data('toggle');
                    var href = $(this).attr('href');
                    if (toggle == 'modal' && href == '#request') {
                        var job_id = $('input[name="job_id"]').val();
                        var uri = base_backend_url + 'tickets/master/get_job_desc';
                        var formdata = {
                            job_id: job_id
                        };
                        $.ajax({
                            url: uri,
                            type: "post",
                            data: formdata,
                            success: function (response) {
                                App.stopPageLoading();
                                $('#job_list_').html(response);
                                return false;
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                App.stopPageLoading();
                                console.log(textStatus, errorThrown);
                                return false;
                            }
                        });
                    }
                });
                $('#sbmt_message').on('click', function () {
                    var uri = base_backend_url + 'tickets/master/insert_message';
                    var formdata = {
                        ticket_id: Base64.encode(ticket_id),
                        ticket_code: code,
                        message: $('textarea[name="message"]').val()
                    };
                    $.ajax({
                        url: uri,
                        type: "post",
                        data: formdata,
                        success: function (response) {
                            App.stopPageLoading();
                            fnAutoLoadMessage();
                            $('textarea[name="message"]').val('');
                            return false;
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            App.stopPageLoading();
                            console.log(textStatus, errorThrown);
                            return false;
                        }
                    });
                });

            }
        };

    }();

    jQuery(document).ready(function () {
        index.init();
    });

</script>
