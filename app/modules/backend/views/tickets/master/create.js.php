<script>
    var Ajax = function () {
        return {
            //main function to initiate the module
            init: function () {
                toastr.success('ticket create js ready!!!' + code);
                $("div#droparea").dropzone({url: base_backend_url + 'tickets/master/action/upload/' + Base64.encode(code)});
                $('#code').html(code);
                $('select.first_ctg').on('change', function () {
                    App.startPageLoading();
                    var id = $(this).val();
                    var uri = base_backend_url + 'tickets/master/get_category/';
                    var formdata = {
                        id: Base64.encode(id)
                    };
                    $.ajax({
                        url: uri,
                        type: "post",
                        data: formdata,
                        success: function (response) {
                            App.stopPageLoading();
                            if (response) {
                                $('select.second_ctg').html(response);
                            }
                            return false;
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            App.stopPageLoading();
                            console.log(textStatus, errorThrown);
                            return false;
                        }
                    });
                    return false;
                });

                $('#issue_ticket').on('click', function () {
                    App.startPageLoading();
                    var uri = base_backend_url + 'tickets/master/insert/';
                    var formdata = {
                        code: code,
                        category: $('#first_ctg').val(),
                        job: $('#second_ctg').val(),
                        priority: $("input[name='priority']:checked").val(),
                        branch: $('#branch').val(),
                        issue: $('textarea[name="issue"]').val()
                    };
                    $.ajax({
                        url: uri,
                        type: "post",
                        data: formdata,
                        success: function (response) {
                            App.stopPageLoading();
                            toastr.success('Successfully add new ticket data ');
                            if (group_id == 1) {
                                window.location.href = base_backend_url + 'tickets/master/detail/' + Base64.encode(code);
                            } else {
                                window.location.href = base_backend_url + 'tickets/master/tracking/' + Base64.encode(code);
                            }
                            return false;
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            App.stopPageLoading();
                            toastr.success('Failed add new ticket data ');
                            return false;
                        }
                    });
                    return false;
                });
            }
        };

    }();

    jQuery(document).ready(function () {
        Ajax.init();
    });

</script>
