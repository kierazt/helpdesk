<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Admin
 *
 * @author SuperUser
 */
class Admin extends MY_Controller{

    //put your code here

    public function __construct() {
        parent::__construct();
        $this->load->model(array('Tbl_users'));
    }

    public function index() {
        redirect(base_backend_url('accounts/admin/view/'));
    }

    public function view() {
        $data['title_for_layout'] = 'welcome';
        $data['view-header-title'] = 'View Admin List';
        $data['content'] = 'ini kontent web';
        $js_files = array(
            static_url('templates/metronics/assets/global/scripts/datatable.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/datatables.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js'),
        );
        $this->load_js($js_files);
        $this->parser->parse('layouts/pages/metronic.phtml', $data);
    }

    public function get_list() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $this->load->library('pagination');
            //init config for datatables
            $draw = $post['draw'];
            $start = $post['start'];
            $length = $post['length'];
            $search = trim($post['search']['value']);

            $cond_count = array();
            $cond['table'] = $cond_count['table'] = 'Tbl_users';
            if (isset($search) && !empty($search)) {
                $cond['like'] = $cond_count['like'] = array('a.name', $search);
            }
			$cond['conditions'] = $cond_count['conditions'] = array('c.group_id' => 1);
            $cond['fields'] = array('a.*','b.nik','d.name group_name');
            $cond['limit'] = array('perpage' => $length, 'offset' => $start);
			$cond['group'] = array('a.id');
			$cond['joins'] = $cond_count['joins'] = array(
				array(
                    'table' => 'tbl_helpdesk_timtik_users b',
                    'conditions' => 'b.user_id = a.id',
                    'type' => 'left'
                ),
                array(
                    'table' => 'tbl_user_groups c',
                    'conditions' => 'c.user_id = a.id',
                    'type' => 'left'
                ),
                array(
                    'table' => 'tbl_groups d',
                    'conditions' => 'd.id = c.group_id',
                    'type' => 'left'
                )
            );
            $total_rows = $this->Tbl_users->find('count', $cond_count);
            $config = array(
                'base_url' => base_backend_url('accounts/admin/get_list/'),
                'total_rows' => $total_rows,
                'per_page' => $length,
            );
            $this->pagination->initialize($config);
            $res = $this->Tbl_users->find('all', $cond);
            $arr = array();
            if (isset($res) && !empty($res)) {
                $i = $start + 1;
                foreach ($res as $d) {
                    $status = '';
                    if ($d['is_active'] == 1) {
                        $status = 'checked';
                    }
                    $action_status = '<div class="form-group">
                        <div class="col-md-9" style="height:30px">
                            <input type="checkbox" class="make-switch" data-size="small" data-value="' . $d['is_active'] . '" data-id="' . $d['id'] . '" name="status" ' . $status . '/>
                        </div>
                    </div>';
                    $data['rowcheck'] = '
                    <div class="form-group form-md-checkboxes">
                        <div class="md-checkbox-list">
                            <div class="md-checkbox">
                                <input type="checkbox" id="select_tr' . $d['id'] . '" class="md-check select_tr" name="select_tr[' . $d['id'] . ']" data-id="' . $d['id'] . '" />
                                <label for="select_tr' . $d['id'] . '">
                                    <span></span>
                                    <span class="check" style="left:20px;"></span>
                                    <span class="box" style="left:14px;"></span>
                                </label>
                            </div>
                        </div>
                    </div>';
					$data['num'] = $i;
                    $data['nik'] = $d['nik']; //optional
                    $data['username'] = $d['username']; //optional	
                    $data['first_name'] = $d['first_name']; //optional	
                    $data['last_name'] = $d['last_name']; //optional	
                    $data['email'] = $d['email']; //optional	
                    $data['group_name'] = $d['group_name']; //optional	
                    $data['active'] = $action_status; //optional	
                    $arr[] = $data;
                    $i++;
                }
            }
            $output = array(
                'draw' => $draw,
                'recordsTotal' => $total_rows,
                'recordsFiltered' => $total_rows,
                'data' => $arr,
            );
            //output to json format
            echo json_encode($output);
        } else {
            echo json_encode(array());
        }
    }

    public function get_data() {
		$post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $res = $this->Tbl_users->find('first', array(
				'fields' => array('a.*','b.nik','d.name group_name'),
                'conditions' => array('a.id' => base64_decode($post['id'])),
				'joins' => array(
					array(
						'table' => 'tbl_helpdesk_timtik_users b',
						'conditions' => 'b.user_id = a.id',
						'type' => 'left'
					),
					array(
						'table' => 'tbl_user_groups c',
						'conditions' => 'c.user_id = a.id',
						'type' => 'left'
					),
					array(
						'table' => 'tbl_groups d',
						'conditions' => 'd.id = c.group_id',
						'type' => 'left'
					)
				)
            ));
            if (isset($res) && !empty($res)) {
                echo json_encode($res);
            } else {
                echo null;
            }
        }
    }

    public function insert() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
			$this->load->model(array('Tbl_user_groups','Tbl_helpdesk_timtik_users'));
            $status = 0;
            if ($post['active'] == 'true') {
                $status = 1;
            }
            $arr_user = array(
                'username' => $post['username'],
                'first_name' => $post['first_name'],
                'last_name' => $post['last_name'],
                'email' => $post['email'],
                'password' => $this->oreno_auth->hash_password(base64_decode($post['password'])),
                'status' => 3,
                'is_active' => $status,
                'is_logged_in' => 0,
                'created_by' => (int) base64_decode($this->auth_config->user_id),
                'create_date' => date_now()
            );
            $user_id = $this->Tbl_users->insert_return_id($arr_user);
            if ($user_id) {
                $arr_user_group = array(
                    'user_id' => $user_id,
                    'group_id' => 1,
                    'is_active' => $status,
                    'created_by' => (int) base64_decode($this->auth_config->user_id),
                    'create_date' => date_now()
                );
                $res = $this->Tbl_user_groups->insert($arr_user_group);
				if ($res) {
					$arr_timtik = array(
						'nik' => $post['nik'],
						'name' =>$post['username'],
						'email' => $post['email'],
						'user_id' => $user_id,
						'is_active' => $status,
						'created_by' => (int) base64_decode($this->auth_config->user_id),
						'create_date' => date_now()
					);
					$this->Tbl_helpdesk_timtik_users->insert($arr_timtik);
					echo 'success';
				} else {
					echo 'failed';
				}
            }
        } else {
            echo 'failed';
        }
    }

    public function update() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $status = 0;
            if ($post['active'] == "true") {
                $status = 1;
            }
            $arr = array(
                'name' => $post['name'],
                'description' => $post['description'],
                'is_active' => $status,
            );
            $res = $this->Tbl_users->update($arr, base64_decode($post['id']));
            if ($res == true) {
                echo 'success';
            } else {
                echo 'failed';
            }
        } else {
            echo 'failed';
        }
    }

    public function update_status() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $id = base64_decode($post['id']);
            $status = 0;
            if ($post['active'] == "true") {
                $status = 1;
            }
            $arr = array(
                'is_active' => $status
            );
            $res = $this->Tbl_users->update($arr, $id);
            if ($res == true) {
                echo 'success';
            } else {
                echo 'failed';
            }
        }
    }

    public function remove() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
			if(is_array($post['id'])){
				$arr_res = 1;
				foreach($post['id'] AS $key => $val){
					$arr_res = $this->Tbl_users->remove($val);
				}
				if($arr_res == true){
					echo 'success';
				} else {
					echo 'failed';
				}
			}else{
				$id = base64_decode($post['id']);
				$res = $this->Tbl_users->remove($id);
				if ($res == true) {
					echo 'success';
				} else {
					echo 'failed';
				}
			}
        }
    }

    public function delete() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
			if(is_array($post['id'])){
				$arr_res = 1;
				foreach($post['id'] AS $key => $val){
					$arr_res = $this->Tbl_users->delete($val);
				}
				if($arr_res == true){
					echo 'success';
				} else {
					echo 'failed';
				}
			}else{
				$id = base64_decode($post['id']);
				$res = $this->Tbl_users->delete($id);
				if ($res == true) {
					echo 'success';
				} else {
					echo 'failed';
				}
			}
        }
    }

}
