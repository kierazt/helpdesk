<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Master
 *
 * @author SuperUser
 */
class Master extends MY_Controller {

    //put your code here

    public function __construct() {
        parent::__construct();
        $this->load->model(array('Tbl_helpdesk_tickets'));
    }

    public function index() {
        redirect(base_backend_url('tickets/master/view/'));
    }

    public function view($key = 'open') {
        $data['title_for_layout'] = 'welcome';
        $data['view-header-title'] = 'View Master List';
        $data['content'] = 'ini kontent web'; //load ajax var
        $var = array(
            array(
                'keyword' => 'key',
                'value' => $key
            )
        );
        $this->load_ajax_var($var);
        $js_files = array(
            static_url('templates/metronics/assets/global/scripts/datatable.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/datatables.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js'),
        );
        $this->load_js($js_files);
        $this->parser->parse('layouts/pages/metronic.phtml', $data);
    }

    public function get_list($key = null) {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $this->load->model('Tbl_helpdesk_imigration_branchs');
            $this->load->library('pagination');
            //init config for datatables
            $draw = $post['draw'];
            $start = $post['start'];
            $length = $post['length'];
            $search = trim($post['search']['value']);

            $cond_count = array();
            $cond['table'] = $cond_count['table'] = 'tbl_helpdesk_tickets';
            if (isset($search) && !empty($search)) {
                $cond['like'] = $cond_count['like'] = array('a.name', $search);
            }
            if (isset($key) && !empty($key)) {
                if ($key == 'request-to-close') {
                    $key = 'close_request';
                }
                $cond['conditions'] = $cond_count['conditions'] = array('c.name' => $key);
            }
            //$cond['conditions'] = $cond_count['conditions'] = array('a.created_by' => (int) base64_decode($this->auth_config->user_id));
            $cond['fields'] = array('a.*', 'b.*', 'c.name ticket_status', 'd.response_time_start', 'd.response_time_stop', 'd.transfer_time_start', 'd.transfer_time_stop', 'd.solving_time_start', 'd.solving_time_stop', 'd.is_open', 'e.name category_name', 'f.name job_category_name', 'g.user_id response_by');
            $cond['order'] = array('key' => 'a.create_date', 'type' => 'DESC');
            $cond['limit'] = array('perpage' => $length, 'offset' => $start);
            $cond['group'] = array('a.id');
            $cond['joins'] = $cond_count['joins'] = array(
                array(
                    'table' => 'tbl_helpdesk_ticket_transactions b',
                    'conditions' => 'b.ticket_id = a.id',
                    'type' => 'left'
                ),
                array(
                    'table' => 'tbl_helpdesk_ticket_status c',
                    'conditions' => 'c.id = b.status_id',
                    'type' => 'left'
                ),
                array(
                    'table' => 'tbl_helpdesk_activities d',
                    'conditions' => 'd.ticket_id = a.id',
                    'type' => 'left'
                ),
                array(
                    'table' => 'tbl_helpdesk_ticket_categories e',
                    'conditions' => 'e.id = b.category_id',
                    'type' => 'left'
                ),
                array(
                    'table' => 'tbl_helpdesk_ticket_categories f',
                    'conditions' => 'f.id = b.job_id',
                    'type' => 'left'
                ),
                array(
                    'table' => 'tbl_helpdesk_ticket_handlers g',
                    'conditions' => 'g.ticket_id = a.id',
                    'type' => 'left'
                )
            );
            $total_rows = $this->Tbl_helpdesk_tickets->find('count', $cond_count);
            $config = array(
                'base_url' => base_backend_url('tickets/master/get_list/' . $key),
                'total_rows' => $total_rows,
                'per_page' => $length,
            );
            $this->pagination->initialize($config);
            $res = $this->Tbl_helpdesk_tickets->find('all', $cond);
            $arr = array();
            if (isset($res) && !empty($res)) {
                $i = $start + 1;
                foreach ($res as $d) {
                    $status = '';
                    if ($d['is_active'] == 1) {
                        $status = 'checked';
                    }
                    $action_status = '<div class="form-group">
                    <div class="col-md-9" style="height:30px">
                        <input type="checkbox" class="make-switch" data-size="small" data-value="' . $d['is_active'] . '" data-id="' . $d['id'] . '" name="status" ' . $status . '/>
                    </div>
                </div>';
                    $branch_code = $this->Tbl_helpdesk_imigration_branchs->get_code($d['branch_id']);
                    if ($this->auth_config->group_id == 1) {
                        $branch_code = 'Tim TIK';
                    }
                    $data['rowcheck'] = '
                <div class="form-group form-md-checkboxes">
                    <div class="md-checkbox-list">
                        <div class="md-checkbox">
                            <input type="checkbox" id="select_tr' . $d['id'] . '" class="md-check select_tr" name="select_tr[' . $d['id'] . ']" data-id="' . $d['id'] . '" />
                            <label for="select_tr' . $d['id'] . '">
                                <span></span>
                                <span class="check" style="left:20px;"></span>
                                <span class="box" style="left:14px;"></span>
                            </label>
                        </div>
                    </div>
                </div>';
                    $data['num'] = $i;
                    $data['code'] = $d['code']; //optional
                    $data['branch_code'] = $branch_code; //optional	
                    $data['content'] = substr($d['content'], 0, 80); //optional	
                    $data['status'] = $this->get_btn_ticket_status($d['ticket_status']); //optional	
                    $data['category_name'] = $d['category_name']; //optional
                    $data['job_category_name'] = $d['job_category_name']; //optional
                    $data['create'] = idn_date($d['create_date']); //optional	
                    $data['description'] = $d['description']; //optional

                    $dsbl = $dsbl2 = '';
                    if ($d['is_open'] == 1) {
                        $dsbl = ' disabled=""';
                    }

                    if ($d['ticket_status'] == 'open') {
                        $action = '
                        <a class="btn red btn-outline sbold" data-toggle="modal" href="#detail" title="Detail" data-id="' . base64_encode($d['id']) . '"> <i class="fa fa-search-plus"></i> </a>					
                        <a class="btn red btn-outline sbold" data-toggle="modal" href="#response" title="Response" data-id="' . base64_encode($d['id']) . '"' . $dsbl . '> <i class="fa fa-pencil-square-o"></i> </a>
                    ';
                    } elseif ($d['ticket_status'] == 'progress') {
                        if ((int) base64_decode($this->auth_config->user_id) == $d['response_by']) {// || $this->auth_config->group_id == 1) {
                            $action = '
                            <a class="btn red btn-outline sbold" data-toggle="modal" href="#detail" title="Detail" data-id="' . base64_encode($d['id']) . '"> <i class="fa fa-search-plus"></i> </a>					
                            <a class="btn red btn-outline sbold" data-toggle="modal" href="#transfer" title="Transfer" data-id="' . base64_encode($d['id']) . '"><i class="fa fa-mail-forward"></i></a>
                            <a id="' . $d['id'] . '" class="btn red btn-outline sbold" data-toggle="modal" href="' . base_backend_url('tickets/master/tracking/' . base64_encode($d['code'])) . '" title="Tracking" data-id="' . $d['id'] . '"> <i class="fa fa-tripadvisor"></i> </a>    
                        ';
                        } else {
                            $action = '
                                <a class="btn red btn-outline sbold" data-toggle="modal" href="#detail" title="Detail" data-id="' . base64_encode($d['id']) . '"> <i class="fa fa-search-plus"></i> </a>					
                            ';
                        }
                    } elseif ($d['ticket_status'] == 'close') {
                        $action = '
                            <a class="btn red btn-outline sbold" title="re Open this ticket" data-toggle="modal" href="#re_open" data-id="' . $d['id'] . '"> <i class="fa fa-folder-open-o"></i> </a>					
                           ';
                    } elseif ($d['ticket_status'] == 'close_request') {
                        $action = '<a id="' . $d['id'] . '" class="btn red btn-outline sbold" data-toggle="modal" href="' . base_backend_url('tickets/master/tracking/' . base64_encode($d['code'])) . '" title="Tracking" data-id="' . $d['id'] . '"> <i class="fa fa-tripadvisor"></i> </a>    ';
                    }
                    $data['action'] = $action;
                    $arr[] = $data;
                    $i++;
                }
            }
            $output = array(
                'draw' => $draw,
                'recordsTotal' => $total_rows,
                'recordsFiltered' => $total_rows,
                'data' => $arr,
            );
            //output to json format
            echo json_encode($output);
        } else {
            echo json_encode(array());
        }
    }

    public function get_data() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $res = $this->Tbl_helpdesk_tickets->find('first', array(
                'conditions' => array('id' => base64_decode($post['id']))
            ));
            if (isset($res) && !empty($res)) {
                echo json_encode($res);
            } else {
                echo null;
            }
        }
    }

    public function insert() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $sess = $this->_session_auth($this->config->session_name);
            $arr_insert = array(
                'code' => $post['code'],
                'content' => $post['issue'],
                'description' => '-',
                'is_active' => 1,
                'created_by' => (int) base64_decode($this->auth_config->user_id),
                'create_date' => date_now()
            );
            $ticket_id = $this->Tbl_helpdesk_tickets->insert_return_id($arr_insert);
            if ($ticket_id) {
                $this->load->model(array('Tbl_helpdesk_ticket_transactions', 'Tbl_helpdesk_ticket_rules', 'Tbl_helpdesk_activities', 'Tbl_hepldesk_ticket_numbers'));
                $rule_id = $this->Tbl_helpdesk_ticket_rules->find('first', array('conditions' => array('id' => (int) $post['priority'])));
                $arr_trans = array(
                    'ticket_id' => (int) $ticket_id,
                    'category_id' => (int) $post['category'],
                    'job_id' => (int) $post['job'],
                    'status_id' => 1,
                    'branch_id' => (int) $sess['office_id'],
                    'priority_id' => (int) $post['priority'],
                    'rule_id' => $rule_id['id'],
                    'is_active' => 1,
                    'created_by' => (int) base64_decode($this->auth_config->user_id),
                    'create_date' => date_now()
                );
                $this->Tbl_helpdesk_ticket_transactions->insert($arr_trans);
                $arr_activity = array(
                    'ticket_id' => $ticket_id,
                    'response_time_start' => date_now('Y-m-d H:i:s'),
                    'response_time_stop' => '0000-00-00 00:00:00',
                    'transfer_time_start' => '0000-00-00 00:00:00',
                    'transfer_time_stop' => '0000-00-00 00:00:00',
                    'solving_time_start' => '0000-00-00 00:00:00',
                    'solving_time_stop' => '0000-00-00 00:00:00',
                    'is_open' => 0,
                    'created_by' => (int) base64_decode($this->auth_config->user_id),
                    'create_date' => date_now()
                );
                $res = $this->Tbl_helpdesk_activities->insert($arr_activity);
                if ($res == true) {
                    $arr_upd = array(
                        'is_active' => 0,
                    );
                    $res = $this->Tbl_hepldesk_ticket_numbers->update_by($arr_upd, $post['code'], 'code');
                    echo 'success';
                }
            }
        }
    }

    public function update() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $status = 0;
            if ($post['active'] == "true") {
                $status = 1;
            }
            $arr = array(
                'name' => $post['name'],
                'description' => $post['description'],
                'is_active' => $status,
            );
            $res = $this->Tbl_helpdesk_tickets->update($arr, base64_decode($post['id']));
            if ($res == true) {
                echo 'success';
            } else {
                echo 'failed';
            }
        } else {
            echo 'failed';
        }
    }

    public function update_status() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $id = base64_decode($post['id']);
            $status = 0;
            if ($post['active'] == "true") {
                $status = 1;
            }
            $arr = array(
                'is_active' => $status
            );
            $res = $this->Tbl_helpdesk_tickets->update($arr, $id);
            if ($res == true) {
                echo 'success';
            } else {
                echo 'failed';
            }
        }
    }

    public function remove() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            if (is_array($post['id'])) {
                $arr_res = 1;
                foreach ($post['id'] AS $key => $val) {
                    $arr_res = $this->Tbl_helpdesk_tickets->remove($val);
                }
                if ($arr_res == true) {
                    echo 'success';
                } else {
                    echo 'failed';
                }
            } else {
                $id = base64_decode($post['id']);
                $res = $this->Tbl_helpdesk_tickets->remove($id);
                if ($res == true) {
                    echo 'success';
                } else {
                    echo 'failed';
                }
            }
        }
    }

    public function delete() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            if (is_array($post['id'])) {
                $arr_res = 1;
                foreach ($post['id'] AS $key => $val) {
                    $arr_res = $this->Tbl_helpdesk_tickets->delete($val);
                }
                if ($arr_res == true) {
                    echo 'success';
                } else {
                    echo 'failed';
                }
            } else {
                $id = base64_decode($post['id']);
                $res = $this->Tbl_helpdesk_tickets->delete($id);
                if ($res == true) {
                    echo 'success';
                } else {
                    echo 'failed';
                }
            }
        }
    }

    public function get_ticket_detail() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $this->load->model('Tbl_helpdesk_tickets');
            $id = base64_decode($post['ticket_id']);
            $res = $this->Tbl_helpdesk_tickets->find('first', array(
                'fields' => array('a.*', 'b.job_id', 'b.created_by handler', 'c.name ticket_status', 'e.name category_name', 'f.name job_category_name'),
                'conditions' => array('a.id' => $id),
                'order' => array('key' => 'a.create_date', 'type' => 'ASC'),
                'joins' => array(
                    array(
                        'table' => 'tbl_helpdesk_ticket_transactions b',
                        'conditions' => 'b.ticket_id = a.id',
                        'type' => 'left'
                    ),
                    array(
                        'table' => 'tbl_helpdesk_ticket_status c',
                        'conditions' => 'c.id = b.status_id',
                        'type' => 'left'
                    ),
                    array(
                        'table' => 'tbl_helpdesk_vendor_users d',
                        'conditions' => 'd.category_id = b.category_id',
                        'type' => 'left'
                    ),
                    array(
                        'table' => 'tbl_helpdesk_ticket_categories e',
                        'conditions' => 'e.id = b.category_id',
                        'type' => 'left'
                    ),
                    array(
                        'table' => 'tbl_helpdesk_ticket_categories f',
                        'conditions' => 'f.id = b.job_id',
                        'type' => 'left'
                    )
                )
            ));

            if (isset($res) && !empty($res)) {
                if ($res['create_date']) {
                    $res['create_date'] = idn_date(strtotime($res['create_date']));
                }
                if ($id) {
                    $this->load->model('Tbl_helpdesk_ticket_handlers');
                    $handler = $this->Tbl_helpdesk_ticket_handlers->find('first', array(
                        'fields' => array('a.*', 'b.email handler_email'),
                        'conditions' => array('ticket_id' => $id),
                        'joins' => array(
                            array(
                                'table' => 'tbl_users b',
                                'conditions' => 'b.id = a.user_id',
                                'type' => 'left'
                            )
                        )
                            )
                    );
                    $r = array(
                        'handle_by' => $handler['handler_email']
                    );
                    $res = array_merge($res, $r);
                }
                echo json_encode($res);
            } else {
                echo null;
            }
        }
    }

    public function get_issue_suggest() {
        $this->load->model('Tbl_helpdesk_ticket_issue_suggestions');
        $res = $this->Tbl_helpdesk_ticket_issue_suggestions->find('all', array(
            'conditions' => array('a.is_active' => 1)
        ));
        if (isset($res) && !empty($res)) {
            $ar = '<option>-- select one --</option>';
            foreach ($res AS $key => $val) {
                $txt = '';
                if ($_SESSION['_lang']) {
                    if ($_SESSION['_lang'] == 'english') {
                        $txt = $val['value_eng'];
                    } elseif ($_SESSION['_lang'] == 'indonesian') {
                        $txt = $val['value_ina'];
                    }
                }
                $ar .= '<option value="' . $txt . '">' . $txt . '</option>';
            }
            echo ($ar);
        } else {
            echo null;
        }
    }

    public function set_open() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $this->load->model('Tbl_helpdesk_activities');
            $res = $this->Tbl_helpdesk_activities->update_by(array('open_time' => date_now(), 'is_open' => 1), base64_decode($post['ticket_id']), 'ticket_id');
            if (isset($res) && !empty($res)) {
                echo 'success';
            } else {
                echo 'failed';
            }
        }
    }

    public function check_ticket_timeout() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $this->load->model('Tbl_helpdesk_activities');
            $ticket = $this->Tbl_helpdesk_tickets->find('first', array('conditions' => array('code' => base64_decode($post['ticket_code']))));
            $res = $this->Tbl_helpdesk_activities->find('first', array('conditions' => array('id' => $ticket['id'])));
            $max_ticket = 1;
            $sum_ticket = (int) (fn_date_diff_ticket($res['open_time'], date_now()));
            if (($res['is_open'] == 1) && ($sum_ticket <= $max_ticket)) {
                echo 'true';
            } else {
                $res = $this->Tbl_helpdesk_activities->update_by(array('open_time' => '0000-00-00 00:00:00', 'is_open' => 0), $ticket['id'], 'ticket_id');
                echo 'false';
            }
        }
    }

    public function set_close() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $this->load->model('Tbl_helpdesk_activities');
            $res = $this->Tbl_helpdesk_activities->update_by(array('open_time' => '0000-00-00 00:00:00', 'is_open' => 0), base64_decode($post['ticket_id']), 'ticket_id');
            if (isset($res) && !empty($res)) {
                echo 'success';
            } else {
                echo 'failed';
            }
        }
    }

    public function tracking($code = null) {
        if ($code == null)
            redirect(base_backend_url('tickets/master/view/open?redirect=true&msg=Please select only registered ticket code'));
        if (strlen($code) != 28)
            redirect(base_backend_url('tickets/master/view/open?redirect=true&msg=Please select only registered ticket code'));
        $data['title_for_layout'] = 'Welcome to tracking ticket page';
        $this->load->model(array('Tbl_helpdesk_tickets', 'Tbl_helpdesk_ticket_chats'));
        $data['code'] = base64_decode($code);
        $data['view-header-title'] = 'Track your ticket';

        try {
            $ticket = $this->Tbl_helpdesk_tickets->find('first', array(
                'fields' => array('a.*', 'c.name ticket_status'),
                'conditions' => array('code' => $data['code']),
                'joins' => array(
                    array(
                        'table' => 'tbl_helpdesk_ticket_transactions b',
                        'conditions' => 'b.ticket_id = a.id',
                        'type' => 'left'
                    ),
                    array(
                        'table' => 'tbl_helpdesk_ticket_status c',
                        'conditions' => 'c.id = b.status_id',
                        'type' => 'left'
                    )
                )
                    )
            );
        } catch (exception $e) {
            $ticket = null;
        }
        if ($ticket == null || $ticket == '')
            redirect(base_url('tickets/master/view/open?redirect=true&msg=Ticket with code ' . $data['code'] . ' cannot found at db'));
        $data['chats'] = $this->Tbl_helpdesk_ticket_chats->find('all', array('conditions' => array('a.ticket_id' => $ticket['id'])));
        $var = array(
            array(
                'keyword' => 'code',
                'value' => $data['code']
            ),
            array(
                'keyword' => 'ticket_id',
                'value' => $ticket['id']
            ),
            array(
                'keyword' => 'ticket_status',
                'value' => $ticket['ticket_status']
            )
        );
        $this->load_ajax_var($var);
        $js_files = array(
            static_url('templates/metronics/assets/global/plugins/bootstrap-confirmation/bootstrap-confirmation.min.js'),
            static_url('templates/metronics/assets/global/scripts/datatable.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/datatables.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js'),
        );
        $this->load_js($js_files);
        $data['ticket'] = $ticket;
        $this->parser->parse('layouts/pages/metronic.phtml', $data);
    }

    public function get_content() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $ticket_id = base64_decode($post['ticket_id']);
            $this->load->model(array('Tbl_helpdesk_ticket_chats', 'Tbl_users'));
            $res = $this->Tbl_helpdesk_ticket_chats->find('all', array('conditions' => array('ticket_id' => $ticket_id), 'order' => array('key' => 'create_date', 'type' => 'DESC')));
            if (isset($res) && !empty($res)) {
                $arr_res = '';
                foreach ($res AS $key => $value) {
                    $user = $this->Tbl_users->find('first', array(
                        'fields' => array('a.*', 'b.img'),
                        'conditions' => array('a.id' => $value['created_by']),
                        'joins' => array(
                            array(
                                'table' => 'tbl_user_profiles b',
                                'conditions' => 'b.user_id = a.id',
                                'type' => 'left'
                            )
                        )
                            )
                    );
                    if ($value['is_vendor'] == 1) {
                        $by = 'Vendor[' . $user['email'] . ']';
                    } else {
                        $by = 'User[' . $user['email'] . ']';
                    }
                    $arr_res .= '<div class="timeline-item">
                                    <div class="timeline-badge">
                                        <img class="timeline-badge-userpic" src="' . static_url($user['img']) . '"> </div>
                                    <div class="timeline-body">
                                        <div class="timeline-body-arrow"> </div>
                                        <div class="timeline-body-head">
                                            <div class="timeline-body-head-caption">
                                                <a href="javascript:;" class="timeline-body-title font-blue-madison">' . $by . '</a>
                                                <span class="timeline-body-time font-grey-cascade">' . idn_date($value['create_date']) . '</span>
                                            </div>
                                        </div>
                                        <div class="timeline-body-content">
                                            <span class="font-grey-cascade">' . $value['messages'] . '</span>
                                        </div>
                                    </div>
                                </div>';
                }
                echo $arr_res;
            }
        }
    }

    public function insert_message() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $this->load->model(array('Tbl_helpdesk_ticket_chats'));
            $ticket_id = base64_decode($post['ticket_id']);
            $arr_insert = array(
                'messages' => $post['message'],
                'ticket_id' => $ticket_id,
                'ticket_code' => $post['ticket_code'],
                'is_vendor' => 0,
                'is_active' => 1,
                'created_by' => (int) base64_decode($this->auth_config->user_id),
                'create_date' => date_now()
            );
            $res = $this->Tbl_helpdesk_ticket_chats->insert($arr_insert);
            if ($res == true) {
                echo 'success';
            } else {
                echo 'failed';
            }
        }
    }

    public function create() {
        $this->load->model(array('Tbl_helpdesk_ticket_categories', 'Tbl_helpdesk_imigration_branchs', 'Tbl_helpdesk_ticket_priorities'));
        $data['title_for_layout'] = 'welcome';
        //load ajax var
        $var = array(
            array(
                'keyword' => 'code',
                'value' => $this->get_ticket_last_code()
            )
        );
        $this->load_ajax_var($var);
        //load js
        $js_files = array(
            "http://ajax.microsoft.com/ajax/jquery.templates/beta1/jquery.tmpl.min.js",
            static_url('lib/single/modernizr.custom.js'),
            static_url('lib/packages/dropzone/dist/dropzone.js'),
            static_url('templates/metronics/assets/global/plugins/jquery-validation/js/jquery.validate.min.js'),
            static_url('templates/metronics/assets/global/plugins/jquery-validation/js/additional-methods.min.js'),
            static_url('templates/metronics/assets/global/plugins/select2/js/select2.full.min.js'),
            static_url('templates/metronics/assets/global/plugins/backstretch/jquery.backstretch.min.js')
        );
        $this->load_js($js_files);
        $data['branch'] = $this->Tbl_helpdesk_imigration_branchs->find('all', array('conditions' => array('is_active' => 1)));
        $data['priority'] = $this->Tbl_helpdesk_ticket_priorities->find('all', array('conditions' => array('is_active' => 1)));
        $data['category'] = $this->Tbl_helpdesk_ticket_categories->find('all', array('conditions' => array('is_active' => 1, 'level' => 1)));
        $this->parser->parse('layouts/pages/metronic.phtml', $data);
    }

    public function action($key = '', $code = null) {
        $this->load->library(array('Oreno_image_upload'));
        if ($key == 'upload') {
            if (isset($_FILES) && !empty($_FILES)) {
                list($width, $height) = getimagesize($_FILES['file']['tmp_name']);
                $options = array(
                    'code' => base64_decode($code),
                    'origin_name' => isset($_FILES['file']['name']) ? $_FILES['file']['name'] : '',
                    'img_path' => $this->config->item('dir.ticket_file', 'path'),
                    'img_size_width' => array($width),
                    'img_name' => array('original')
                );
                $res = $this->oreno_image_upload->do_upload_ticket_files($_FILES['file'], $options);
                if (isset($res) && !empty($res)) {
                    $this->load->model('Tbl_helpdesk_ticket_files');
                    $arr_insert = array(
                        'code' => base64_decode($code),
                        'path' => $res['original'],
                        'description' => '-',
                        'is_active' => 1,
                        'created_by' => (int) base64_decode($this->auth_config->user_id),
                        'create_date' => date_now()
                    );
                    $result = $this->Tbl_helpdesk_ticket_files->insert($arr_insert);
                    if ($result) {
                        echo 'success';
                    } else {
                        echo 'failed';
                    }
                } else {
                    echo 'failed';
                }
            }
        }
    }

    public function get_category($id = null) {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $this->load->model(array('Tbl_helpdesk_ticket_categories'));
            $id = base64_decode($post['id']);
            $result = $this->Tbl_helpdesk_ticket_categories->find('all', array(
                'conditions' => array('is_active' => 1, 'parent_id' => $id)
                    )
            );
            if (isset($result) && !empty($result)) {
                $arr = '<option>-- select one --</option>';
                foreach ($result AS $key => $value) {
                    $arr .= '<option value="' . $value['id'] . '">' . $value['name'] . '</option>';
                }
                echo $arr;
            } else {
                echo '';
            }
        }
    }

    public function get_vendor($id = null) {
        $this->load->model('Tbl_helpdesk_ticket_vendors');
        $result = $this->Tbl_helpdesk_ticket_vendors->find('first', array(
            'fields' => array('a.*', 'b.name vendor_name'),
            'conditions' => array('category_id' => $id),
            'group' => array('a.id'),
            'joins' => array(
                array(
                    'table' => 'tbl_helpdesk_vendors b',
                    'conditions' => 'b.id = a.vendor_id',
                    'type' => 'left'
                )
            )
        ));
        if (isset($result) && !empty($result)) {
            echo '<option value="' . $result['vendor_id'] . '">' . $result['vendor_name'] . '</option>';
        } else {
            echo '';
        }
    }

    public function detail($id_ = null) {
        if ($id_ != null) {
            $data['title_for_layout'] = 'welcome';
            $data['view-header-title'] = 'View Group List';
            $data['content'] = 'ini kontent web';
            $this->load->model(array('Tbl_helpdesk_tickets', 'Tbl_helpdesk_ticket_files'));
            $id = base64_decode($id_);
            $ticket = $this->Tbl_helpdesk_tickets->find('first', array('conditions' => array('code' => $id)));
            //load ajax var
            $var = array(
                array(
                    'keyword' => 'id',
                    'value' => $ticket['id']
                )
            );
            $this->load_ajax_var($var);
            $data['files'] = $this->Tbl_helpdesk_ticket_files->find('all', array('conditions' => array('a.code' => $id)));
            $this->parser->parse('layouts/pages/metronic.phtml', $data);
        }
    }

    public function response_ticket() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            //set ticket status into progress
            $this->load->model(array('Tbl_helpdesk_ticket_chats', 'Tbl_helpdesk_ticket_handlers', 'Tbl_helpdesk_ticket_transactions', 'Tbl_helpdesk_activities'));
            $ticket_id = base64_decode($post['ticket_id']);
            $arr_insert = array(
                'messages' => $post['message'],
                'ticket_id' => $ticket_id,
                'ticket_code' => $post['ticket_code'],
                'is_vendor' => 1,
                'is_active' => 1,
                'created_by' => (int) base64_decode($this->auth_config->user_id),
                'create_date' => date_now()
            );
            $res = $this->Tbl_helpdesk_ticket_chats->insert($arr_insert);
            if ($res == true) {
                $arr_ticket = array(
                    'status_id' => 2
                );
                $this->Tbl_helpdesk_ticket_transactions->update_by($arr_ticket, $ticket_id, 'ticket_id');
                $arr_activity = array(
                    'response_time_stop' => date_now(),
                    'solving_time_start' => date_now(),
                    'is_open' => 0,
                    'created_by' => (int) base64_decode($this->auth_config->user_id),
                    'create_date' => date_now()
                );
                $res = $this->Tbl_helpdesk_activities->update_by($arr_activity, $ticket_id, 'ticket_id');
                if ($res == true) {
                    $arr_handler_ticket = array(
                        'ticket_id' => $ticket_id,
                        'user_id' => (int) base64_decode($this->auth_config->user_id),
                        'group_id' => (int) ($this->auth_config->group_id),
                        'ticket_status' => 2,
                        'is_active' => 1,
                        'created_by' => (int) base64_decode($this->auth_config->user_id),
                        'create_date' => date_now()
                    );

                    $this->Tbl_helpdesk_ticket_handlers->insert($arr_handler_ticket);
                    echo 'success';
                }
                echo 'success';
            } else {
                echo 'failed';
            }
        }
    }

    public function get_job_desc() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $this->load->model('Tbl_helpdesk_ticket_categories');
            $res = $this->Tbl_helpdesk_ticket_categories->find('all', array('conditions' => array('parent_id' => $post['job_id'])));
            if (isset($res) && !empty($res)) {
                $rr = '';
                foreach ($res AS $key => $val) {
                    $rr .= '
                        <div class="md-checkbox">
                            <input type="checkbox" data-id="' . $val['id'] . '" id="checkbox' . $val['id'] . '" class="md-check msg_job_list" name="msg_job_list[]">
                            <label for="checkbox' . $val['id'] . '">
                                <span class="inc"></span>
                                <span class="check"></span>
                                <span class="box"></span> ' . $val['name'] . ' 
                            </label>
                        </div>';
                }
                echo $rr;
            } else {
                echo 'data not found';
            }
        }
    }

    public function close_ticket_request() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $this->load->model(array('Tbl_helpdesk_ticket_requests', 'Tbl_helpdesk_ticket_categories', 'Tbl_helpdesk_activities', 'Tbl_helpdesk_activities', 'Tbl_helpdesk_ticket_transactions'));
            $job_lis = '';
            $rr = '';
            if (isset($post['msg_job_list']) && !empty($post['msg_job_list'])) {
                foreach ($post['msg_job_list'] AS $key => $val) {
                    if ($val == 'on') {
                        $txt = $this->Tbl_helpdesk_ticket_categories->find('first', array('conditions' => array('id' => $key)));
                        if (!empty($rr))
                            $rr .= ', ';
                        $rr .= $txt['name'];
                    }
                }
            }
            $arr = array(
                'ticket_id' => $post['ticket_id'],
                'job_list' => $rr,
                'message' => $post['message'],
                'is_active' => 1,
                'created_by' => (int) base64_decode($this->auth_config->user_id),
                'create_date' => date_now()
            );
            $res = $this->Tbl_helpdesk_ticket_requests->insert($arr);
            // debug($res);
            if ($res == true) {
                $this->Tbl_helpdesk_ticket_transactions->query("UPDATE `tbl_helpdesk_ticket_transactions` SET `status_id` = '4' WHERE `tbl_helpdesk_ticket_transactions`.`ticket_id` = {$post['ticket_id']};", 'insert'); //($arr_transc, 'ticket_id', $post['ticket_id']);
                $this->Tbl_helpdesk_activities->query("UPDATE tbl_helpdesk_activities SET solving_time_stop = '" . date_now() . "', `close_message` = '" . $post['message'] . "' WHERE `tbl_helpdesk_activities`.`id` = {$post['ticket_id']};", 'insert');

                echo 'success';
            }
        } else {
            echo 'failed';
        }
    }

    public function check_ticket_open() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $this->load->model('Tbl_helpdesk_activities');
            $res = $this->Tbl_helpdesk_activities->find('first', array('conditions' => array('ticket_id' => base64_decode($post['id']))));
            if ($res['is_open'] == 1) {
                echo 'false';
            } else {
                echo 'true';
            }
        }
    }

    public function mark_as_solve() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $this->load->model(array('Tbl_helpdesk_ticket_transactions', 'Tbl_helpdesk_activities', 'Tbl_helpdesk_ticket_handlers'));
            $id = base64_decode($post['ticket_id']);
            $msg = $post['message'];
            $arr = array(
                'response_time_stop' => date('Y-m-d H:i:s'),
                'solving_time_start' => date('Y-m-d H:i:s'),
                'solving_time_stop' => date('Y-m-d H:i:s'),
                'close_message' => $msg
            );
            $res = $this->Tbl_helpdesk_activities->update_by($arr, $id, 'ticket_id');
            if ($res) {
                $arr = array(
                    'status_id' => 5
                );
                $this->Tbl_helpdesk_ticket_transactions->update($arr, $id);
                $arr_ = array(
                    'ticket_id' => $id,
                    'user_id' => (int) base64_decode($this->auth_config->user_id),
                    'group_id' => (int) $this->auth_config->group_id,
                    'ticket_status' => 5,
                    'is_active' => 1,
                    'created_by' => (int) base64_decode($this->auth_config->user_id),
                    'create_date' => date_now()
                );
                $this->Tbl_helpdesk_ticket_handlers->insert($arr_);
                echo 'successfully close this ticket.';
            } else {
                echo 'failed close this ticket.';
            }
        }
    }

}
