<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Category
 *
 * @author SuperUser
 */
class Category extends MY_Controller {

    //put your code here

    public function __construct() {
        parent::__construct();
        $this->load->model(array('Tbl_helpdesk_ticket_categories','Tbl_icons'));
    }

    public function index() {
        redirect(base_backend_url('tickets/category/view/'));
    }

    public function view() {
        $data['title_for_layout'] = 'welcome';
        $data['view-header-title'] = 'View Group List';
        $data['content'] = 'ini kontent web';
		$data['icons'] = $this->Tbl_icons->find('all', array('conditions' => array('is_active' => 1)));
        
		$css_files = array(
            static_url('lib/packages/bootstrap/treeview/dist/bootstrap-treeview.min.css')
        );
        $this->load_css($css_files);
        $js_files = array(
            static_url('lib/packages/bootstrap/treeview/dist/bootstrap-treeview.min.js')
        );
        $this->load_js($js_files);
        $this->parser->parse('layouts/pages/metronic.phtml', $data);
    }

    public function get_list() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $this->load->library('pagination');
            //init config for datatables
            $draw = $post['draw'];
            $start = $post['start'];
            $length = $post['length'];
            $search = trim($post['search']['value']);

            $cond_count = array();
            $cond['table'] = 'Tbl_helpdesk_ticket_categories';
            $cond['conditions'] = $cond_count['conditions'] = array('level' => 2); //, 'is_active' => 1);
            if (isset($search) && !empty($search)) {
                $cond['like'] = $cond_count['like'] = array('name', $search);
            }
            $cond['fields'] = array('*');
            $cond['limit'] = array('perpage' => $length, 'offset' => $start);
            $total_rows = $this->Tbl_helpdesk_ticket_categories->find('count', $cond_count);
            $config = array(
                'base_url' => base_backend_url('master/group/get_list/'),
                'total_rows' => $total_rows,
                'per_page' => $length,
            );
            $this->pagination->initialize($config);
            $res = $this->Tbl_helpdesk_ticket_categories->find('all', $cond);
            $arr = array();
            if (isset($res) && !empty($res)) {
                $i = $start + 1;
                foreach ($res as $d) {
                    $status = '';
                    if ($d['is_active'] == 1) {
                        $status = 'checked';
                    }
                    $action_status = '<div class="form-group">
                        <div class="col-md-9" style="height:30px">
                            <input type="checkbox" class="make-switch" data-size="small" data-value="' . $d['is_active'] . '" data-id="' . $d['id'] . '" name="status" ' . $status . '/>
                        </div>
                    </div>';
                    $data['rowcheck'] = '
                    <div class="form-group form-md-checkboxes">
                        <div class="md-checkbox-list">
                            <div class="md-checkbox">
                                <input type="checkbox" id="select_tr' . $d['id'] . '" class="md-check select_tr" name="select_tr[' . $d['id'] . ']" data-id="' . $d['id'] . '" />
                                <label for="select_tr' . $d['id'] . '">
                                    <span></span>
                                    <span class="check" style="left:20px;"></span>
                                    <span class="box" style="left:14px;"></span>
                                </label>
                            </div>
                        </div>
                    </div>';
                    $data['num'] = $i;
                    $data['name'] = $d['name']; //optional	
                    $data['active'] = $action_status; //optional	
                    $data['description'] = $d['description']; //optional
                    $arr[] = $data;
                    $i++;
                }
            }
            $output = array(
                'draw' => $draw,
                'recordsTotal' => $total_rows,
                'recordsFiltered' => $total_rows,
                'data' => $arr,
            );
            //output to json format
            echo json_encode($output);
        } else {
            echo json_encode(array());
        }
    }

    public function get_data() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $res = $this->Tbl_helpdesk_ticket_categories->find('first', array(
                'conditions' => array('id' => base64_decode($post['id']))
            ));
            if (isset($res) && !empty($res)) {
                echo json_encode($res);
            } else {
                echo null;
            }
        }
    }

    public function insert() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $arr_insert = '';
            $status = 0;
            if ($post['active'] == 'true') {
                $status = 1;
            }
            switch ($post['level']) {
                case 0:
                    $level = 1;
                    break;
                case 1:
                    $level = 2;
                    break;
                case 2:
                    $level = 3;
                    break;
            }
            $rank = 1;
            //before insert check it first rank with same params is exist then plus 1
            $exist = $this->Tbl_helpdesk_ticket_categories->find('all', array('conditions' => array(
                    'level' => $level,
                    'parent_id' => $post['parent_id']
                )
                    )
            );
            if (isset($exist) && !empty($exist)) {
                $rank = count($exist) + 1;
            }
            $arr_insert = array(
                'name' => $post['name'],
                'rank' => $rank,
                'level' => $level,
                'icon' => $post['icon'],
                'is_active' => $status,
                'description' => $post['description'],
                'parent_id' => $post['parent_id'],
                'created_by' => (int) base64_decode($this->auth_config->user_id),
                'create_date' => date_now()
            );
            $res = $this->Tbl_helpdesk_ticket_categories->insert($arr_insert);
            if ($res == true) {
                echo 'success';
            } else {
                echo 'failed';
            }
        } else {
            echo 'failed';
        }
    }

    public function update() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $status = 0;
            if ($post['active'] == "true") {
                $status = 1;
            }
            $logged = 0;
            if ($post['logged'] == "true") {
                $logged = 1;
            }
            $arr_update = array(
                'name' => $post['name'],
                'rank' => 0,
                'level' => $post['level'],
                'icon' => $post['icon'],
                'is_active' => $status,
                'description' => $post['description'],
                'parent_id' => $post['parent_id']
            );
            $res = $this->Tbl_helpdesk_ticket_categories->update($arr_update, base64_decode($post['id']));
            if ($res == true) {
                echo 'success';
            } else {
                echo 'failed';
            }
        } else {
            echo 'failed';
        }
    }
	
    public function update_status() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $id = base64_decode($post['id']);
            $status = 0;
            if ($post['active'] == "true") {
                $status = 1;
            }
            $arr = array(
                'is_active' => $status
            );
            $res = $this->Tbl_helpdesk_ticket_categories->update($arr, $id);
            if ($res == true) {
                echo 'success';
            } else {
                echo 'failed';
            }
        }
    }

    public function remove() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            if (is_array($post['id'])) {
                $arr_res = 1;
                foreach ($post['id'] AS $key => $val) {
                    $arr_res = $this->Tbl_helpdesk_ticket_categories->remove($val);
                }
                if ($arr_res == true) {
                    echo 'success';
                } else {
                    echo 'failed';
                }
            } else {
                $id = base64_decode($post['id']);
                $res = $this->Tbl_helpdesk_ticket_categories->remove($id);
                if ($res == true) {
                    echo 'success';
                } else {
                    echo 'failed';
                }
            }
        }
    }

    public function delete() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            if (is_array($post['id'])) {
                $arr_res = 1;
                foreach ($post['id'] AS $key => $val) {
                    $arr_res = $this->Tbl_helpdesk_ticket_categories->delete($val);
                }
                if ($arr_res == true) {
                    echo 'success';
                } else {
                    echo 'failed';
                }
            } else {
                $id = base64_decode($id_);
                $res = $this->Tbl_helpdesk_ticket_categories->delete($id);
                if ($res == true) {
                    echo 'success';
                } else {
                    echo 'failed';
                }
            }
        }
    }

	public function get_category(){
		$categories = $this->category();
        if (isset($categories) && !empty($categories)) {
            $first_arr = array();
            if (isset($categories['nodes']) && !empty($categories['nodes'])) {
                $second_arr = array();
                foreach ($categories['nodes'] AS $values) {
                    $third_arr = array();
                    $parent_id = 0;
                    $parent_name = '';
                    if (isset($values['nodes']) && !empty($values['nodes'])) {
                        foreach ($values['nodes'] AS $val) {
                            $fourth_arr = array();
                            if (isset($val['nodes']) && !empty($val['nodes'])) {
                                foreach ($val['nodes'] AS $v) {
                                    $fourth_arr[] = array(
                                        'text' => $v['menu_text'],
                                        'micon' => isset($v['menu_icon']) ? $v['menu_icon'] : '#',
                                        'level' => $v['menu_level'],
                                        'parent_id' => (int) $v['menu_parent_id'],
                                        'parent_name' => $this->Tbl_helpdesk_ticket_categories->get_name($v['menu_parent_id']),
                                        'is_active' => $v['is_active'],
                                        'id' => (int) $v['menu_id']
                                    );
                                }
                            }
                            $parent_id_ = $val['menu_parent_id'];
                            $parent_name_ = $this->Tbl_helpdesk_ticket_categories->get_name($val['menu_parent_id']);
                            $third_arr[] = array(
                                'text' => $val['menu_text'],
                                'micon' => isset($val['menu_icon']) ? $val['menu_icon'] : '#',
                                'level' => $val['menu_level'],
                                'parent_id' => (int) $parent_id_,
                                'parent_name' => isset($parent_name_) ? $parent_name_ : '',
                                'id' => (int) $val['menu_id'],
                                'is_active' => $val['is_active'],
                                'nodes' => $fourth_arr
                            );
                        }
                    }
                    if (isset($values['menu_parent_id']) && $values['menu_parent_id'] != 0) {
                        $parent_id = $values['menu_parent_id'];
                        $parent_name = $this->Tbl_helpdesk_ticket_categories->get_name($values['menu_parent_id']);
                    }
                    $second_arr[] = array(
                        'text' => $values['menu_text'],
                        'micon' => isset($values['menu_icon']) ? $values['menu_icon'] : '#',
                        'level' => $values['menu_level'],
                        'id' => (int) $values['menu_id'],
                        'parent_id' => $parent_id,
                        'parent_name' => isset($parent_name) ? $parent_name : '-',
                        'is_active' => $values['is_active'],
                        'nodes' => $third_arr
                    );
                }
                $first_arr = array(
                    'text' => $categories['text'],
                    'micon' => '-',
                    'level' => $categories['level'],
                    'id' => (int) $categories['id'],
                    'parent_id' => 0,
                    'parent_name' => '-',
                    'is_active' => $categories['is_active'],
                    'nodes' => $second_arr
                );
            }
            if ($first_arr != null) {
                echo '[' . json_encode($first_arr) . ']';
            }
        } else {
            echo '[{ 
                "text" : "Ticket Category" ,
                "icon": "",
                "level" : "0",
                "id": "0",
                "parent_id": "0",
                "parent_name": "-",
                "is_active": 1,
            }]';
        }
	}
}
