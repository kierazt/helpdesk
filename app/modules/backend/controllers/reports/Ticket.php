<?php

require_once DOCUMENT_ROOT . '/var/static/lib/packages/phpexcel/autoload.php';

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\IOFactory;

/**
 * Description of Ticket
 *
 * @author SuperUser
 */
class Ticket extends MY_Controller {

//put your code here

    public function __construct() {
        parent::__construct();
        $this->load->model(array('Tbl_helpdesk_tickets', 'tbl_helpdesk_ticket_transactions', 'tbl_helpdesk_ticket_status', 'tbl_helpdesk_activities', 'tbl_helpdesk_ticket_priorities', 'tbl_helpdesk_ticket_categories'));
    }

    public function index() {
        redirect(base_backend_url('reports/ticket/category/'));
    }

    public function by_category() {
        $data['title_for_layout'] = 'Ticket reporting page by Category';
        $data['view-header-title'] = 'Ticket reporting page by Category';
        $data['content'] = 'ini kontent web';
        //load ajax var
        $var = array(
            array(
                'keyword' => 'export_file_name',
                'value' => 'export_ticket_report_' . gmdate('Y_m_d_h_i_s')
            )
        );
        $this->load_ajax_var($var);
        $css_files = array(
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/jquery.dataTables.min.css'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/buttons.dataTables.min.css'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/select.dataTables.min.css')
        );
        $this->load_css($css_files);
        $js_files = array(
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/jquery.dataTables.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/dataTables.buttons.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/buttons.flash.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/buttons.colVis.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/buttons.print.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/jszip.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/pdfmake.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/vfs_fonts.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/buttons.html5.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/dataTables.select.min.js')
        );
        $this->load_js($js_files);
        $this->load->model(array('Tbl_helpdesk_ticket_status', 'Tbl_helpdesk_ticket_priorities', 'Tbl_helpdesk_ticket_categories'));
        $data['status'] = $this->Tbl_helpdesk_ticket_status->find('list', array('conditions' => array('is_active' => 1)));
        $data['priority'] = $this->Tbl_helpdesk_ticket_priorities->find('list', array('conditions' => array('is_active' => 1)));
        $data['category'] = $this->Tbl_helpdesk_ticket_categories->find('list', array('conditions' => array('is_active' => 1, 'a.level' => 1)));
        $this->parser->parse('layouts/pages/metronic.phtml', $data);
    }

    public function by_ticket() {
        $data['title_for_layout'] = 'Ticket reporting page by Category';
        $data['view-header-title'] = 'Ticket reporting page by Category';
        $data['content'] = 'ini kontent web';
//load ajax var
        $var = array(
            array(
                'keyword' => 'export_file_name',
                'value' => 'export_ticket_report_' . gmdate('Y_m_d_h_i_s')
            )
        );
        $this->load_ajax_var($var);
        $css_files = array(
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/jquery.dataTables.min.css'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/buttons.dataTables.min.css'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/select.dataTables.min.css')
        );
        $this->load_css($css_files);
        $js_files = array(
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/jquery.dataTables.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/dataTables.buttons.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/buttons.flash.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/buttons.colVis.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/buttons.print.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/jszip.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/pdfmake.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/vfs_fonts.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/buttons.html5.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/dataTables.select.min.js')
        );
        $this->load_js($js_files);
        $this->parser->parse('layouts/pages/metronic.phtml', $data);
    }

    public function by_date() {
        $data['title_for_layout'] = 'Ticket reporting page by Date';
        $data['view-header-title'] = 'Ticket reporting page by Date';
        $data['content'] = 'ini kontent web';
        //load ajax var
        $var = array(
            array(
                'keyword' => 'export_file_name',
                'value' => 'export_ticket_report_' . gmdate('Y_m_d_h_i_s')
            )
        );
        $this->load_ajax_var($var);
        $css_files = array(
            'https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css',
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/jquery.dataTables.min.css'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/buttons.dataTables.min.css'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/select.dataTables.min.css')
        );
        $this->load_css($css_files);
        $js_files = array(
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/jquery.dataTables.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/dataTables.buttons.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/buttons.flash.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/buttons.colVis.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/buttons.print.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/jszip.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/pdfmake.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/vfs_fonts.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/buttons.html5.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/dataTables.select.min.js')
        );
        $this->load_js($js_files);
        $this->load->model(array('Tbl_helpdesk_ticket_status', 'Tbl_helpdesk_ticket_priorities', 'Tbl_helpdesk_ticket_categories'));
        $data['status'] = $this->Tbl_helpdesk_ticket_status->find('list', array('conditions' => array('is_active' => 1)));
        $data['priority'] = $this->Tbl_helpdesk_ticket_priorities->find('list', array('conditions' => array('is_active' => 1)));
        $data['category'] = $this->Tbl_helpdesk_ticket_categories->find('list', array('conditions' => array('is_active' => 1)));
        $this->parser->parse('layouts/pages/metronic.phtml', $data);
    }

    public function get_list() {
        $post = $this->input->post(NULL, TRUE);
        $output = array(
            'draw' => 0,
            'recordsTotal' => 0,
            'recordsFiltered' => 0,
            'data' => array(),
        );
        if (isset($post) && !empty($post)) {
            $this->load->library('pagination');
            //init config for datatables
            $draw = $post['draw'];
            $start = $post['start'];
            $length = $post['length'];
            $tbl_name = 'tbl_helpdesk_tickets a';
            $fields = 'a.*, b.status_id, c.name ticket_status, d.response_time_start, d.response_time_stop, d.transfer_time_start, d.transfer_time_stop, d.solving_time_start, d.solving_time_stop, d.is_open, e.name priority_name, f.name category_name, g.messages, d.close_message, h.first_name ';
            $conditions = '';
            $param3 = false;
            if (isset($post['param1']) && !empty($post['param1'])) {
                $arr_c = '';
                $key = array_keys($post['param1']);
                for ($i = 0; $i < count($key); $i++) {
                    if (!empty($arr_c) && $post['param1'][$key[$i]] != 0)
                        $arr_c .= ' AND ';
                    switch ($key[$i]) {
                        case 'ticket_status':
                            $prefix = 'c.id';
                            break;
                        case 'ticket_priority':
                            $prefix = 'e.id';
                            break;
                        case 'ticket_category':
                            $prefix = 'f.id';
                            break;
                    }
                    if ($post['param1'][$key[$i]] != 0) {
                        $arr_c .= $prefix . ' = ' . $post['param1'][$key[$i]];
                    }
                }
                $conditions = ' WHERE ' . $arr_c; //' WHERE c.id = ' .
                $post['param1']['ticket_status'] . ' OR d.id = ' . $post['param1']['ticket_priority'] . ' OR f.id = ' . $post['param1']['ticket_category'];
            } elseif (isset($post['param2']) && !empty($post['param2'])) {
                if ($post['param2']['from_date'] == $post['param2']['to_date']) {
                    $conditions = " WHERE a.create_date >= '" . date('Y-m-d H:i:s', strtotime($post['param2']['from_date'])) . "' AND a.create_date < '" . date('Y-m-d H:i:s', strtotime('+1day', strtotime($post['param2']['to_date']))) . "'";
                } else {
                    $conditions = " WHERE a.create_date >= '" . date('Y-m-d H:i:s', strtotime($post['param2']['from_date'])) . "' AND a.create_date <= '" . date('Y-m-d H:i:s', strtotime($post['param2']['to_date'])) . "'";
                }
            } elseif (isset($post['param3']) && !empty($post['param3'])) {
                $param3 = true;
                $conditions = 'WHERE a.code LIKE "%' . $post['param3']['code'] . '%" AND b.status_id = 5 GROUP BY d.created_by';
                // $group = ' ';
            }
            $joins = ' LEFT JOIN tbl_helpdesk_ticket_transactions b ON
            b.ticket_id = a.id LEFT JOIN tbl_helpdesk_ticket_status c ON c.id =
            b.status_id LEFT JOIN tbl_helpdesk_activities d ON d.ticket_id = a.id
            LEFT JOIN tbl_helpdesk_ticket_priorities e ON e.id = b.priority_id
            LEFT JOIN tbl_helpdesk_ticket_categories f ON f.id = b.category_id
            LEFT JOIN tbl_helpdesk_ticket_chats g ON g.ticket_id = a.id
            LEFT JOIN tbl_users h ON h.id = d.created_by ';
            $res = $this->Tbl_helpdesk_tickets->query("SELECT {$fields} FROM {$tbl_name} {$joins} {$conditions} LIMIT {$start}, {$length}"); // ORDER BY a.create_date DESC GROUP BY a.id  ORDER BY a.create_date DESC GROUP BY a.id
            if (isset($res) && !empty($res) && $res != null) {
                $cond_count = $this->Tbl_helpdesk_tickets->query("SELECT COUNT(*) total FROM {$tbl_name} {$joins} {$conditions}");
                $total_rows = $cond_count[0]['total'];
                $config = array(
                    'base_url' => base_url('vendor/ticket/get_list/'),
                    'total_rows' => $total_rows,
                    'per_page' => $length,
                );
                $this->pagination->initialize($config);
                $arr = array();
                if (isset($res) && !empty($res)) {
                    $i = $start + 1;
                    foreach ($res as $d) {
                        $status = '';
                        if ($d['is_active'] == 1) {
                            $status = 'checked';
                        }
                        $action_status = '<div class="form-group">
                        <div class="col-md-9" style="height:30px">
                            <input type="checkbox" class="make-switch"
                            data-size="small" data-value="' . $d['is_active'] . '" data-id="' .
                                $d['id'] . '" name="status" ' . $status . '/>
                        </div>
                    </div>';
                        $data['num'] = $i;
                        $data['code'] = $d['code']; //optional
                        $data['content'] = substr($d['content'], 0, 80); //optional
                        $data['status'] = $d['ticket_status']; //optional
                        $data['priority'] = $d['priority_name']; //optional
                        $data['category'] = $d['category_name']; //optional
                        $data['response'] = $d['messages']; //optional
                        $data['closing'] = $d['close_message']; //optional
                        $data['vendor'] = $d['first_name']; //optional
                        $data['open_date'] = date('d/m/Y', strtotime($d['create_date']));
                        $data['open_time'] = date('H:i:s', strtotime($d['create_date']));
                        $data['response_date'] = date('d/m/Y', strtotime($d['response_time_start']));
                        $data['response_time'] = date('H:i:s', strtotime($d['response_time_start']));
                        $data['closing_date'] = date('d/m/Y', strtotime($d['response_time_stop']));
                        $data['closing_time'] = date('H:i:s', strtotime($d['response_time_stop']));
                        $data['create'] = idn_date($d['create_date']); //optional   
                        $data['active'] = $action_status; //optional
                        $data['action'] = '';
                        if ($param3 == true) {
                            $data['action'] = '<a href="' . base_backend_url('reports/ticket/generate/excel/' . $d['id']) . '" title="Download file to excel"><i class="fa fa-file-excel-o"></i></a>';
                        }
                        $arr[] = $data;
                        $i++;
                    }
                }
                $output = array(
                    'draw' => $draw,
                    'recordsTotal' => $total_rows,
                    'recordsFiltered' => $total_rows,
                    'data' => $arr,
                );
//output to json format
                echo json_encode($output);
            } else {
                echo json_encode($output);
            }
        } else {
            echo json_encode($output);
        }
    }

    public function get_data() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $res = $this->Tbl_helpdesk_tickets->find('first', array(
                'conditions' => array('id' => base64_decode($post['id']))
            ));
            if (isset($res) && !empty($res)) {
                echo json_encode($res);
            } else {
                echo null;
            }
        }
    }

    public function insert() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $status = 0;
            if ($post['active'] == 'true') {
                $status = 1;
            }
            $arr_insert = array(
                'name' => $post['name'],
                'description' => $post['description'],
                'is_active' => $status,
                'created_by' => (int) base64_decode($this->auth_config->user_id),
                'create_date' => date_now()
            );
            $result = $this->Tbl_helpdesk_tickets->insert($arr_insert);
            if ($result == true) {
                echo 'success';
            } else {
                echo 'failed';
            }
        } else {
            echo 'failed';
        }
    }

    public function update() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $status = 0;
            if ($post['active'] == "true") {
                $status = 1;
            }
            $arr = array(
                'name' => $post['name'],
                'description' => $post['description'],
                'is_active' => $status,
            );
            $res = $this->Tbl_helpdesk_tickets->update($arr, base64_decode($post['id']));
            if ($res == true) {
                echo 'success';
            } else {
                echo 'failed';
            }
        } else {
            echo 'failed';
        }
    }

    public function update_status() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $id = base64_decode($post['id']);
            $status = 0;
            if ($post['active'] == "true") {
                $status = 1;
            }
            $arr = array(
                'is_active' => $status
            );
            $res = $this->Tbl_helpdesk_tickets->update($arr, $id);
            if ($res == true) {
                echo 'success';
            } else {
                echo 'failed';
            }
        }
    }

    public function remove() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            if (is_array($post['id'])) {
                $arr_res = 1;
                foreach ($post['id'] AS $key => $val) {
                    $arr_res = $this->Tbl_helpdesk_tickets->remove($val);
                }
                if ($arr_res == true) {
                    echo 'success';
                } else {
                    echo 'failed';
                }
            } else {
                $id = base64_decode($post['id']);
                $res = $this->Tbl_helpdesk_tickets->remove($id);
                if ($res == true) {
                    echo 'success';
                } else {
                    echo 'failed';
                }
            }
        }
    }

    public function delete() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            if (is_array($post['id'])) {
                $arr_res = 1;
                foreach ($post['id'] AS $key => $val) {
                    $arr_res = $this->Tbl_helpdesk_tickets->delete($val);
                }
                if ($arr_res == true) {
                    echo 'success';
                } else {
                    echo 'failed';
                }
            } else {
                $id = base64_decode($post['id']);
                $res = $this->Tbl_helpdesk_tickets->delete($id);
                if ($res == true) {
                    echo 'success';
                } else {
                    echo 'failed';
                }
            }
        }
    }

    public function generate($type = null, $id = null) {
        switch ($type) {
            case 'excel':
                $this->gen_to_excel(trim($id));
                break;
        }
    }

    protected function gen_to_excel($id = null) {
        $this->load->model('Tbl_helpdesk_tickets');
        $this->load->library('oreno_sla');
        $ticket_activities = $this->oreno_sla->get($id);
        debug(get_detail_date($ticket_activities[0]['response']['response_time']));
        $spreadsheet = new Spreadsheet();
        $tbl_name = 'tbl_helpdesk_tickets a';
        $fields = 'a.*, c.name ticket_status, d.response_time_start, d.response_time_stop, d.transfer_time_start, d.transfer_time_stop, d.solving_time_start , d.solving_time_stop, d.is_open, e.name priority_name, f.name category_name, g.messages,h.first_name';
        $joins = '  LEFT JOIN tbl_helpdesk_ticket_transactions b ON b.ticket_id = a.id 
                    LEFT JOIN tbl_helpdesk_ticket_status c ON c.id = b.status_id 
                    LEFT JOIN tbl_helpdesk_activities d ON d.ticket_id = a.id 
                    LEFT JOIN tbl_helpdesk_ticket_priorities e ON e.id = b.priority_id 
                    LEFT JOIN tbl_helpdesk_ticket_categories f ON f.id = b.category_id
                    LEFT JOIN tbl_helpdesk_ticket_chats g ON g.ticket_id = a.id
                    LEFT JOIN tbl_users h ON h.id = d.created_by';
        $conditions = 'WHERE a.id LIKE "%' . $id . '%" ';
        $group = 'GROUP BY a.id';

        $ticket1 = $this->Tbl_helpdesk_tickets->query("SELECT {$fields} FROM {$tbl_name} {$joins} {$conditions} {$group}");

        $ticket2 = $this->Tbl_helpdesk_tickets->query("SELECT {$fields} FROM {$tbl_name} {$joins} {$conditions}");

        $title_file = 'Ticket_report_' . $ticket1[0]['code'];

        $spreadsheet->getProperties()->setCreator('PhpOffice')
                ->setLastModifiedBy('PhpOffice')
                ->setTitle('Office 2007 XLSX Test Document')
                ->setSubject('Office 2007 XLSX Test Document')
                ->setDescription('PhpOffice')
                ->setKeywords('PhpOffice')
                ->setCategory('PhpOffice');

        /*
         *
         * Sheet 1 start here
         */
        $sheet = $spreadsheet->getActiveSheet();
        // Rename worksheet
        $sheet->setTitle('Detail Ticket');
        // Header
        $sheet->mergeCells('B1:H1');
        $sheet->setCellValue('B1', 'Detail Ticket ' . $ticket1[0]['code']);
        $sheet->getRowDimension('1')->setRowHeight(30);
        $sheet->getStyle('B1:H1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        // Set Column Name
        $sheet->setCellValue('B4', 'No');
        $sheet->getColumnDimension('B')->setWidth(5);
        $sheet->setCellValue('C4', 'No Ticket');
        $sheet->getColumnDimension('C')->setWidth(30);
        $sheet->setCellValue('D4', 'Category');
        $sheet->getColumnDimension('D')->setWidth(20);
        $sheet->setCellValue('E4', 'Priority');
        $sheet->getColumnDimension('E')->setWidth(15);
        $sheet->setCellValue('F4', 'Issue');
        $sheet->getColumnDimension('F')->setWidth(80);
        $sheet->setCellValue('G4', 'Created Date');
        $sheet->getColumnDimension('G')->setWidth(50);
        $sheet->setCellValue('H4', 'Created by');
        $sheet->getColumnDimension('H')->setWidth(30);
        $sheet->getStyle('B4:H4')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        //Set Value  
        $no = 1;
        $sheet->setCellValue('B5', $no);
        $sheet->setCellValue('C5', $ticket1[0]['code']);
        $sheet->setCellValue('D5', $ticket1[0]['category_name']);
        $sheet->getRowDimension(4)->setRowHeight(-1);
        $sheet->setCellValue('E5', $ticket1[0]['priority_name']);
        $sheet->setCellValue('F5', $ticket1[0]['content']);
        $sheet->setCellValue('G5', idn_date($ticket1[0]['create_date']));
        $sheet->setCellValue('H5', $ticket1[0]['first_name']);
        $sheet->getStyle('B5:H5')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5')->getAlignment()->setWrapText(true)->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $no++;

        $sheet->getStyle('B1:H1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
        $styleArray = [
            'borders' => [
                'outline' => [
                    'borderStyle' =>
                    \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                ],
            ],
        ];
        $headerStyle = [
            'font' => [
                'bold' => true,
                'color' => ['argb' => '000000'],
                'size' => '22'
            ],
            'borders' => [
                'outline' => [
                    'borderStyle' =>
                    \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                ],
            ],
        ];
        $sheet->getStyle('B1:H1')->applyFromArray($headerStyle);
        $sheet->getStyle('B1:H5')->applyFromArray($styleArray);

        //set actual content here at sheet 1
        /*
         *
         * Sheet 1 end here
         */
        //create new sheet
        $spreadsheet->createSheet();
        /*
         *
         * Sheet 2 start here
         */
        // Add some data
        $spreadsheet->setActiveSheetIndex(1);
        $sheet = $spreadsheet->setActiveSheetIndex(1);
        $sheet->mergeCells('B1:M1');
        $sheet->setCellValue('B1', 'Report : Close Ticket ' . $ticket1[0]['code']);
        $sheet->getRowDimension('1')->setRowHeight(40);
        $sheet->getStyle('B1:M1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $no = 1;
        $row = 5;
        foreach ($ticket2 as $key => $value) {
            $sheet->setCellValue('B' . $row, $no);
            $sheet->setCellValue('C' . $row, $value['code']);
            $sheet->setCellValue('D' . $row, $value['category_name']);
            $sheet->setCellValue('E' . $row, $value['priority_name']);
            $sheet->setCellValue('F' . $row, $value['content']);
            $sheet->setCellValue('G' . $row, $value['messages']);
            $sheet->setCellValue('H' . $row, date('d/m/Y', strtotime($value['create_date'])));
            $sheet->setCellValue('I' . $row, date('H:i:s', strtotime($value['create_date'])));
            $sheet->setCellValue('J' . $row, date('d/m/Y', strtotime($value['response_time_start'])));
            $sheet->setCellValue('K' . $row, date('H:i:s', strtotime($value['response_time_start'])));
            $sheet->setCellValue('L' . $row, date('d/m/Y', strtotime($value['solving_time_stop'])));
            $sheet->setCellValue('M' . $row, date('H:i:s', strtotime($value['solving_time_stop'])));
            $sheet->getStyle('F' . $row)->getAlignment()->setWrapText(true)->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle('G' . $row)->getAlignment()->setWrapText(true)->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle('B:M')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

            $row++;
            $no++;
        }

        // Set Column Name
        $sheet->setCellValue('B4', 'No');
        $sheet->getColumnDimension('B')->setWidth(5);
        $sheet->setCellValue('C4', 'No Ticket');
        $sheet->getColumnDimension('C')->setWidth(30);
        $sheet->setCellValue('D4', 'Category');
        $sheet->getColumnDimension('D')->setWidth(20);
        $sheet->setCellValue('E4', 'Priority');
        $sheet->getColumnDimension('E')->setWidth(15);
        $sheet->setCellValue('F4', 'Issue');
        $sheet->getColumnDimension('F')->setWidth(80);
        $sheet->setCellValue('G4', 'Chat History');
        $sheet->getColumnDimension('G')->setWidth(50);
        $sheet->setCellValue('H4', 'Report Date');
        $sheet->getColumnDimension('H')->setWidth(20);
        $sheet->setCellValue('I4', 'Report Time');
        $sheet->getColumnDimension('I')->setWidth(20);
        $sheet->setCellValue('J4', 'Response Date');
        $sheet->getColumnDimension('J')->setWidth(20);
        $sheet->setCellValue('K4', 'Response Time');
        $sheet->getColumnDimension('K')->setWidth(20);
        $sheet->setCellValue('L4', 'Close Date');
        $sheet->getColumnDimension('L')->setWidth(20);
        $sheet->setCellValue('M4', 'Close Time');
        $sheet->getColumnDimension('M')->setWidth(20);
        $sheet->getStyle('B4:M4')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        //Set Value
        // $sheet->getRowDimension(4)->setRowHeight(-1);
        // $sheet->getStyle('B5:G5')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        // Rename worksheet
        // $this->load->library('oreno_sla');
        // $ticket_activities = $this->oreno_sla->get($id);
        // debug($act);
        $spreadsheet->getActiveSheet()->setTitle('Chat & History');

        $sheet->getStyle('B1:M1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('ccccc');
        $styleArray = [
            'borders' => [
                'outline' => [
                    'borderStyle' =>
                    \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                    'color' => ['argb' => 'cccccc'],
                ],
            ],
        ];
        $headerStyle = [
            'font' => [
                'bold' => true,
                'color' => ['argb' => 'ffffff'],
                'size' => '22'
            ],
            'borders' => [
                'outline' => [
                    'borderStyle' =>
                    \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                    'color' => ['argb' => 'cccccc'],
                ],
            ],
        ];
        $sheet->getStyle('B1:M1')->applyFromArray($headerStyle);
        $sheet->getStyle('B1:M5')->applyFromArray($styleArray);
        /*
         *
         * Sheet 2 end here
         */
        //create new sheet
        $spreadsheet->createSheet();

        /*
         *
         * Sheet 3 start here
         */
        // Add some data
        $spreadsheet->setActiveSheetIndex(2);
        $sheet = $spreadsheet->setActiveSheetIndex(2);
        $sheet->mergeCells('B1:E1');
        $sheet->setCellValue('B1', 'Report : Close Ticket ' . $ticket1[0]['code']);
        $sheet->getRowDimension('1')->setRowHeight(40);
        $sheet->getStyle('B1:E1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        // Set Column Name
        $sheet->setCellValue('B4', '#');
        $sheet->getColumnDimension('B')->setWidth(25);
        $sheet->setCellValue('C4', 'SLA Response');
        $sheet->getColumnDimension('C')->setWidth(40);
        $sheet->setCellValue('D4', 'SLA Transfer');
        $sheet->getColumnDimension('D')->setWidth(40);
        $sheet->setCellValue('E4', 'SLA Solving');
        $sheet->getColumnDimension('E')->setWidth(40);
        $sheet->getStyle('B4:E4')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        //Set Value
        $sheet->setCellValue('B5', 'Mulai');
        $sheet->setCellValue('B6', 'Selesai');
        $sheet->setCellValue('B7', 'Total Waktu');
        $sheet->getRowDimension(4)->setRowHeight(-1);
        $sheet->setCellValue('B8', 'Status Denda');
        $sheet->setCellValue('B9', 'Total Denda');
        $sheet->getStyle('B5:B9')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $spreadsheet->getActiveSheet()->setTitle('SLA Response Ticket');

        $sheet->getStyle('B1:E1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('ccccc');
        $styleArray = [
            'borders' => [
                'outline' => [
                    'borderStyle' =>
                    \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                    'color' => ['argb' => 'cccccc'],
                ],
            ],
        ];
        $headerStyle = [
            'font' => [
                'bold' => true,
                'color' => ['argb' => 'ffffff'],
                'size' => '22'
            ],
            'borders' => [
                'outline' => [
                    'borderStyle' =>
                    \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                    'color' => ['argb' => 'cccccc'],
                ],
            ],
        ];
        $sheet->getStyle('B1:E1')->applyFromArray($headerStyle);
        $sheet->getStyle('B1:E5')->applyFromArray($styleArray);
        /*
         *
         * Sheet 3 end here
         */
        //create new sheet
        $spreadsheet->createSheet();

        /*
         *
         * Sheet 4 start here
         */
        // Add some data
        $spreadsheet->setActiveSheetIndex(3)->setCellValue('A1', 'world!');
        // Rename worksheet
        $spreadsheet->getActiveSheet()->setTitle('SLA Solving Ticket');
        /*
         *
         * Sheet 4 end here
         */

        // Set active sheet index to the first sheet, so Excel opens
//this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);
        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type:
application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="' .
                $title_file . '.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
// always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        ob_clean();
        $writer->save('php://output');
        // window.open(doc.output('bloburl'), '_blank');
        exit;
    }

}
