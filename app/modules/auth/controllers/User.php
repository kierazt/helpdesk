<?php

class User extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        redirect(base_url('login'));
    }

    public function login($template_id = '') {
        $data = $this->setup_layout();
        $data['title_for_layout'] = 'welcome to login page Helpdesk Imigrasi';
        //load js
        $js_files = array(
            static_url('templates/metronics/assets/global/plugins/jquery-validation/js/jquery.validate.min.js'),
            static_url('templates/metronics/assets/global/plugins/jquery-validation/js/additional-methods.min.js'),
            static_url('templates/metronics/assets/global/plugins/select2/js/select2.full.min.js'),
            static_url('templates/metronics/assets/global/plugins/backstretch/jquery.backstretch.min.js')
        );
        $this->load_js($js_files);
        $this->parser->parse('layouts/pages/metronic_login.phtml', $data);
    }

    public function check_data() {
        $this->load->library('oreno_auth');
        $post = $this->input->post(NULL, TRUE);
        if (isset($post['login']) && !empty($post['login'])) {
            $auth = $this->oreno_auth->auth($post['login']);
            $result = json_decode($auth);
            echo return_call_back('message', array(
                'login' => $result->result->status,
                'group' => $result->result->group
                    ), 'json');
        } else {
            echo 'failed';
        }
        exit();
    }

    public function switch_lang() {
        $request_language = $this->input->get(NULL, TRUE);
        if (isset($request_language['bahasa']) && !empty($request_language['bahasa'])) {
            $session_language = $this->session->userdata('_lang');
            if ($request_language['bahasa'] != $session_language) {
                unset($_SESSION['_lang']);
                $_SESSION['_lang'] = $request_language['bahasa'];
                redirect($this->agent->referrer());
            }
        } else {
            $this->session->set_flashdata('error', $this->lang->line('failed_change_lang'));
            redirect($this->agent->referrer());
        }
    }

    public function logout() {
        $this->_logout();
        redirect(base_url('login'));
    }

}
