<?php

require_once DOCUMENT_ROOT . '/var/static/lib/packages/phpexcel/autoload.php';

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\IOFactory;

/**
 * Description of Ticket
 *
 * @author SuperUser
 */
class Ticket extends MY_Controller {

    //put your code here

    public function __construct() {
        parent::__construct();
        $this->load->model(array('Tbl_helpdesk_tickets'));
    }

    public function index() {
        redirect(base_url('helpdesk/report/ticket/category/'));
    }

    public function by_category() {
        $data['title_for_layout'] = 'Ticket reporting page by Category';
        $data['view-header-title'] = 'Ticket reporting page by Category';
        $data['content'] = 'ini kontent web';
        //load ajax var
        $var = array(
            array(
                'keyword' => 'export_file_name',
                'value' => 'export_ticket_report_' . date_now()
            )
        );
        $this->load_ajax_var($var);
        $css_files = array(
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/jquery.dataTables.min.css'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/buttons.dataTables.min.css'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/select.dataTables.min.css')
        );
        $this->load_css($css_files);
        $js_files = array(
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/jquery.dataTables.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/dataTables.buttons.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/buttons.flash.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/buttons.colVis.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/buttons.print.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/jszip.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/pdfmake.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/vfs_fonts.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/buttons.html5.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/dataTables.select.min.js')
        );
        $this->load_js($js_files);
        $this->load->model(array('Tbl_helpdesk_ticket_status', 'Tbl_helpdesk_ticket_priorities', 'Tbl_helpdesk_ticket_categories'));
        $data['status'] = $this->Tbl_helpdesk_ticket_status->find('list', array('conditions' => array('is_active' => 1)));
        $data['priority'] = $this->Tbl_helpdesk_ticket_priorities->find('list', array('conditions' => array('is_active' => 1)));
        $data['category'] = $this->Tbl_helpdesk_ticket_categories->find('list', array('conditions' => array('is_active' => 1, 'a.level' => 1)));

        $this->parser->parse('layouts/pages/metronic.phtml', $data);
    }

    public function by_ticket() {
        $data['title_for_layout'] = 'Ticket reporting page by Category';
        $data['view-header-title'] = 'Ticket reporting page by Category';
        $data['content'] = 'ini kontent web';
        //load ajax var
        $var = array(
            array(
                'keyword' => 'export_file_name',
                'value' => 'export_ticket_report_' . date_now()
            )
        );
        $this->load_ajax_var($var);
        $css_files = array(
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/jquery.dataTables.min.css'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/buttons.dataTables.min.css'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/select.dataTables.min.css')
        );
        $this->load_css($css_files);
        $js_files = array(
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/jquery.dataTables.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/dataTables.buttons.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/buttons.flash.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/buttons.colVis.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/buttons.print.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/jszip.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/pdfmake.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/vfs_fonts.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/buttons.html5.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/dataTables.select.min.js')
        );
        $this->load_js($js_files);
        $this->parser->parse('layouts/pages/metronic.phtml', $data);
    }

    public function by_date() {
        $data['title_for_layout'] = 'Ticket reporting page by Date';
        $data['view-header-title'] = 'Ticket reporting page by Date';
        $data['content'] = 'ini kontent web';
        //load ajax var
        $var = array(
            array(
                'keyword' => 'export_file_name',
                'value' => 'export_ticket_report_' . date_now()
            )
        );
        $this->load_ajax_var($var);
        $css_files = array(
            'https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css',
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/jquery.dataTables.min.css'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/buttons.dataTables.min.css'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/select.dataTables.min.css')
        );
        $this->load_css($css_files);
        $js_files = array(
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/jquery.dataTables.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/dataTables.buttons.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/buttons.flash.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/buttons.colVis.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/buttons.print.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/jszip.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/pdfmake.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/vfs_fonts.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/buttons.html5.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/dataTables.select.min.js')
        );
        $this->load_js($js_files);
        $this->load->model(array('Tbl_helpdesk_ticket_status', 'Tbl_helpdesk_ticket_priorities', 'Tbl_helpdesk_ticket_categories'));
        $data['status'] = $this->Tbl_helpdesk_ticket_status->find('list', array('conditions' => array('is_active' => 1)));
        $data['priority'] = $this->Tbl_helpdesk_ticket_priorities->find('list', array('conditions' => array('is_active' => 1)));
        $data['category'] = $this->Tbl_helpdesk_ticket_categories->find('list', array('conditions' => array('is_active' => 1)));

        $this->parser->parse('layouts/pages/metronic.phtml', $data);
    }

    public function get_list() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $this->load->library('pagination');
            //init config for datatables
            $draw = $post['draw'];
            $start = $post['start'];
            $length = $post['length'];
            $tbl_name = 'tbl_helpdesk_tickets a';
            $fields = 'a.*, c.name ticket_status, d.response_time_start, d.response_time_stop, d.transfer_time_start, d.transfer_time_stop, d.solving_time_start, d.solving_time_stop, d.is_open, e.name priority_name, f.name category_name';
            $conditions = '';
            $param3 = false;
            if (isset($post['param1']) && !empty($post['param1'])) {
                $arr_c = '';
                $key = array_keys($post['param1']);
                for ($i = 0; $i < count($key); $i++) {
                    if (!empty($arr_c) && $post['param1'][$key[$i]] != 0)
                        $arr_c .= ' AND ';
                    switch ($key[$i]) {
                        case 'ticket_status':
                            $prefix = 'c.id';
                            break;
                        case 'ticket_priority':
                            $prefix = 'e.id';
                            break;
                        case 'ticket_category':
                            $prefix = 'f.id';
                            break;
                    }
                    if ($post['param1'][$key[$i]] != 0) {
                        $arr_c .= $prefix . ' = ' . $post['param1'][$key[$i]];
                    }
                }
                $conditions = ' WHERE ' . $arr_c; //' WHERE c.id = ' . $post['param1']['ticket_status'] . ' OR d.id = ' . $post['param1']['ticket_priority'] . ' OR f.id = ' . $post['param1']['ticket_category'];
            } elseif (isset($post['param2']) && !empty($post['param2'])) {
                if ($post['param2']['from_date'] == $post['param2']['to_date']) {
                    $conditions = " WHERE a.create_date >= '" . date('Y-m-d H:i:s', strtotime($post['param2']['from_date'])) . "' AND a.create_date < '" . date('Y-m-d H:i:s', strtotime('+1day', strtotime($post['param2']['to_date']))) . "'";
                } else {
                    $conditions = " WHERE a.create_date >= '" . date('Y-m-d H:i:s', strtotime($post['param2']['from_date'])) . "' AND a.create_date <= '" . date('Y-m-d H:i:s', strtotime($post['param2']['to_date'])) . "'";
                }
            } elseif (isset($post['param3']) && !empty($post['param3'])) {
                $param3 = true;
                $conditions = 'WHERE a.code LIKE "%' . $post['param3']['code'] . '%" OR a.code LIKE "%' . $post['param3']['code'] . '" OR a.code LIKE "' . $post['param3']['code'] . '%"';
            }
            $joins = ' LEFT JOIN tbl_helpdesk_ticket_transactions b ON b.ticket_id = a.id LEFT JOIN tbl_helpdesk_ticket_status c ON c.id = b.status_id LEFT JOIN tbl_helpdesk_activities d ON d.ticket_id = a.id LEFT JOIN tbl_helpdesk_ticket_priorities e ON e.id = b.priority_id LEFT JOIN tbl_helpdesk_ticket_categories f ON f.id = b.category_id';
            $res = $this->Tbl_helpdesk_tickets->query("SELECT {$fields} FROM {$tbl_name} {$joins} {$conditions} LIMIT {$start}, {$length}"); // ORDER BY a.create_date DESC GROUP BY a.id  ORDER BY a.create_date DESC GROUP BY a.id 
            $cond_count = $this->Tbl_helpdesk_tickets->query("SELECT COUNT(*) total FROM {$tbl_name} {$joins} {$conditions}");
            $total_rows = $cond_count[0]['total'];
            $config = array(
                'base_url' => base_url('vendor/ticket/get_list/'),
                'total_rows' => $total_rows,
                'per_page' => $length,
            );
            $this->pagination->initialize($config);
            $arr = array();
            if (isset($res) && !empty($res)) {
                $i = $start + 1;
                foreach ($res as $d) {
                    $status = '';
                    if ($d['is_active'] == 1) {
                        $status = 'checked';
                    }
                    $action_status = '<div class="form-group">
                        <div class="col-md-9" style="height:30px">
                            <input type="checkbox" class="make-switch" data-size="small" data-value="' . $d['is_active'] . '" data-id="' . $d['id'] . '" name="status" ' . $status . '/>
                        </div>
                    </div>';
                    $data['num'] = $i;
                    $data['code'] = $d['code']; //optional	
                    $data['content'] = substr($d['content'], 0, 80); //optional	
                    $data['status'] = $d['ticket_status']; //optional		
                    $data['priority'] = $d['priority_name']; //optional		
                    $data['category'] = $d['category_name']; //optional		
                    $data['create'] = idn_date(strtotime($d['create_date'])); //optional
                    $data['active'] = $action_status; //optional
                    $data['action'] = '';
                    if ($param3 == true) {
                        $data['action'] = '<a href="' . base_url('helpdesk/report/ticket/generate/excel/' . $d['id']) . '" title="Download file to excel"><i class="fa fa-file-excel-o"></i></a>';
                    }
                    $arr[] = $data;
                    $i++;
                }
            }
            $output = array(
                'draw' => $draw,
                'recordsTotal' => $total_rows,
                'recordsFiltered' => $total_rows,
                'data' => $arr,
            );
            //output to json format
            echo json_encode($output);
        } else {
            echo json_encode(array());
        }
    }

    public function get_data() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $res = $this->Tbl_helpdesk_tickets->find('first', array(
                'conditions' => array('id' => base64_decode($post['id']))
            ));
            if (isset($res) && !empty($res)) {
                echo json_encode($res);
            } else {
                echo null;
            }
        }
    }

    public function insert() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $status = 0;
            if ($post['active'] == 'true') {
                $status = 1;
            }
            $arr_insert = array(
                'name' => $post['name'],
                'description' => $post['description'],
                'is_active' => $status,
                'created_by' => (int) base64_decode($this->auth_config->user_id),
                'create_date' => date_now()
            );
            $result = $this->Tbl_helpdesk_tickets->insert($arr_insert);
            if ($result == true) {
                echo 'success';
            } else {
                echo 'failed';
            }
        } else {
            echo 'failed';
        }
    }

    public function update() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $status = 0;
            if ($post['active'] == "true") {
                $status = 1;
            }
            $arr = array(
                'name' => $post['name'],
                'description' => $post['description'],
                'is_active' => $status,
            );
            $res = $this->Tbl_helpdesk_tickets->update($arr, base64_decode($post['id']));
            if ($res == true) {
                echo 'success';
            } else {
                echo 'failed';
            }
        } else {
            echo 'failed';
        }
    }

    public function update_status() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $id = base64_decode($post['id']);
            $status = 0;
            if ($post['active'] == "true") {
                $status = 1;
            }
            $arr = array(
                'is_active' => $status
            );
            $res = $this->Tbl_helpdesk_tickets->update($arr, $id);
            if ($res == true) {
                echo 'success';
            } else {
                echo 'failed';
            }
        }
    }

    public function remove() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            if (is_array($post['id'])) {
                $arr_res = 1;
                foreach ($post['id'] AS $key => $val) {
                    $arr_res = $this->Tbl_helpdesk_tickets->remove($val);
                }
                if ($arr_res == true) {
                    echo 'success';
                } else {
                    echo 'failed';
                }
            } else {
                $id = base64_decode($post['id']);
                $res = $this->Tbl_helpdesk_tickets->remove($id);
                if ($res == true) {
                    echo 'success';
                } else {
                    echo 'failed';
                }
            }
        }
    }

    public function delete() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            if (is_array($post['id'])) {
                $arr_res = 1;
                foreach ($post['id'] AS $key => $val) {
                    $arr_res = $this->Tbl_helpdesk_tickets->delete($val);
                }
                if ($arr_res == true) {
                    echo 'success';
                } else {
                    echo 'failed';
                }
            } else {
                $id = base64_decode($post['id']);
                $res = $this->Tbl_helpdesk_tickets->delete($id);
                if ($res == true) {
                    echo 'success';
                } else {
                    echo 'failed';
                }
            }
        }
    }

    public function generate() {
        $spreadsheet = new Spreadsheet();

        $sheet = $spreadsheet->getActiveSheet(0);
        $sheet->setCellValue('A1', 'Hello World !');

        $spreadsheet->getActiveSheet()->setTitle('Report Excel ' . date('d-m-Y H'));
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Report Excel.xlsx"');
        header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit;
    }

}
