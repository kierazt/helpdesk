<div class="row">
    <div class="col-md-12">
        <!-- Begin: life time stats -->
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject font-dark sbold uppercase">{view-header-title}</span>
                </div>
            </div>
            <div class="portlet-body">
                <form method="POST" id="add_edit">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-gift"></i>Ticket Detail 
                                    </div>
                                    <div class="tools">
                                        <a href="javascript:;" class="expand"> </a>
                                    </div>
                                </div>
                                <div class="portlet-body display-hide">
                                    <?php $this->load->view('includes/tools/detail_ticket_portlet.html.php'); ?>
                                </div>
                            </div>
                        </div>

                        <?php if (isset($files) && !empty($files)): ?>
                            <div class="col-md-12">
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-gift"></i>Ticket Attachment 
                                        </div>
                                        <div class="tools">
                                            <a href="javascript:;" class="expand"> </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body display-hide" style="overflow-y:auto">
                                        <?php foreach ($files AS $key => $value): ?>
                                            <div class="col-md-2" style="margin-bottom:2px">
                                                <div class="dashboard-stat blue">
                                                    <div class="visual">
                                                        <i class="fa fa-comments"></i>
                                                    </div>
                                                    <div class="details">
                                                        <div class="desc">
                                                            <small style="font-size:12px" title="<?php echo $value['path']; ?>"><?php echo $value['code']; ?></small>
                                                        </div>
                                                    </div>
                                                    <a class="more f_attachment" data-toggle="modal" href="#file_attach_mdl" data-path="<?php echo $value['path']; ?>"  data-id="<?php echo $value['id']; ?>" data-code="<?php echo $value['code']; ?>" href="javascript:;">
                                                        View more <i class="m-icon-swapright m-icon-white"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                        <div class="col-md-12" id="chatbox">
                            <div class="portlet light portlet-fit bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-microphone font-green"></i>
                                        <span class="caption-subject bold font-green uppercase"> Tracking Ticket [<?php echo isset($code) ? $code : ''; ?>]</span>
                                        <span class="caption-helper"><?php echo isset($ticket['ticket_status']) ? $ticket['ticket_status'] : ''; ?></span>
                                    </div>
                                    <div class="tools">
                                        <a href="javascript:;" class="collapse"> </a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="timeline" style="height:50%; overflow:auto">
                                        <!-- TIMELINE ITEM -->
                                        <div class="timeline"></div>

                                        <!-- END TIMELINE ITEM -->									
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <textarea class="form-control" rows="3" name="message" placeholder="Type your message and enter to send a message"></textarea>
                            </div>
                            <input type="text" name="ticket_id" id="ticket_id" value="<?php echo isset($ticket['id']) ? $ticket['id'] : ''; ?>" hidden/>
                            <button type="submit" class="btn green" id="sbmt_message">Submit</button>
                            <a class="btn green" id="re_open">Re-Open</a>
                            <a id="mark_as_solve" class="btn red-mint" >Mark as Solve</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
</div>
<div id="file_attach_mdl" class="modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="scroller" style="height:300px" data-always-visible="1" data-rail-visible1="1">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tile image selected">
                                <div class="tile-body">
                                    <span class="file"></span>
                                </div>
                                <div class="tile-object">
                                    <div id="media"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.modal -->
<div class="modal fade bs-modal-lg" id="modal_solve" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Are your sure to mark this ticket as 'Solve/Close' ?</h4>
            </div>
            <div class="modal-body"> Modal body goes here </div>
            <div class="modal-footer">
                <button type="button" class="btn green">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>