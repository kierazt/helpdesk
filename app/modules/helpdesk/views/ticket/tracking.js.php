<script>
    var fnAutoLoadMessage = function () {
        $.ajaxSetup({cache: false}); // This part addresses an IE bug.  without it, IE will only load the first number and will never refresh
        App.startPageLoading();
        var uri = base_url + 'ticket/get_content/';
        var formdata = {
            ticket_id: Base64.encode(ticket_id)
        };
        setInterval(function () {
            $.ajax({
                url: uri,
                type: "post",
                data: formdata,
                success: function (response) {
                    App.stopPageLoading();
                    $('.timeline').html(response);
                    return false;
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    App.stopPageLoading();
                    console.log(textStatus, errorThrown);
                    return false;
                }
            });
            App.stopPageLoading();
        }, 5000);
    };

    var fnLoadDetailTicket = function () {
        var formdata = {
            id: Base64.encode(ticket_id)
        };
        $.ajax({
            url: base_url + 'ticket/get_ticket_detail/',
            type: "post",
            data: formdata,
            success: function (response) {
                App.stopPageLoading();
                var row = JSON.parse(response);
                $('input[name="create_date"]').val(row.create_date);
                $('input[name="code"]').val(row.code);
                $('input[name="ticket_status"]').val(row.ticket_status);
                $('input[name="vendor_code"]').val(row.vendor_code);
                $('input[name="vendor_name"]').val(row.vendor_name);
                $('textarea[name="content"]').val(row.content);
                $('textarea[name="description"]').val(row.description);
                return false;
            },
            error: function (jqXHR, textStatus, errorThrown) {
                App.stopPageLoading();
                console.log(textStatus, errorThrown);
                return false;
            }
        });
    };
    var index = function () {
        return {
            //main function to initiate the module
            init: function () {
                toastr.success('ticket view js ready!!!');
                fnAutoLoadMessage();
                fnLoadDetailTicket();
                if (ticket_status) {
                    fnTicketAuth(ticket_status);
                }
                $('.f_attachment').on('click', function () {
                    var id = $(this).data('id');
                    var path = static_url + 'tickets/' + $(this).data('path');
                    $('#media').html('<img style="width:100%; height:100%; margin:10px" src="' + path + '" />');
                });
                $('#sbmt_message').on('click', function () {
                    var uri = base_url + 'ticket/insert_message';
                    var formdata = {
                        ticket_id: Base64.encode(ticket_id),
                        ticket_code: code,
                        message: $('textarea[name="message"]').val()
                    };
                    $.ajax({
                        url: uri,
                        type: "post",
                        data: formdata,
                        success: function (response) {
                            App.stopPageLoading();
                            fnAutoLoadMessage();
                            $('textarea[name="message"]').val('');
                            return false;
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            App.stopPageLoading();
                            console.log(textStatus, errorThrown);
                            return false;
                        }
                    });
                });

                $('#mark_as_solve').on('click', function () {
                    var uri = base_url + 'ticket/close_status';
                    var formdata = {
                        ticket_id: Base64.encode($('input[name="ticket_id"]').val())
                    };
                    $.ajax({
                        url: uri,
                        type: "post",
                        data: formdata,
                        success: function (response) {
                            App.stopPageLoading();
                            fnCloseModal();
                            return false;
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            App.stopPageLoading();
                            console.log(textStatus, errorThrown);
                            return false;
                        }
                    });
                });
            }
        };

    }();

    jQuery(document).ready(function () {
        index.init();
    });

</script>
