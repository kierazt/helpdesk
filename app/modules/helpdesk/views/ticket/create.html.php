<div class="row">
    <div class="col-md-12 ">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                    <i class="icon-layers font-red-sunglo"></i>
                    <span class="caption-subject bold uppercase"><?php echo $this->lang->line('global_ticket_create'); ?></span>
                </div>
                <div class="portlet-body form">
                    <form class="issue_ticket_form" role="form" style="padding-top:40px">
                        <div class="col-md-12">
                            <div class="form-body">
                                <div class="form-group form-md-line-input">
                                    <div class="form-control" id="code"></div>
                                    <label for="form_control_1"><?php echo $this->lang->line('global_ticket_number'); ?></label>
                                </div>
                                <div class="form-group col-md-4 form-md-line-input form-md-floating-label has-info" id="main_category">
                                    <select name="category_1" class="form-control edited first_ctg" id="first_ctg">
                                        <option>-- select one --</option>
                                        <?php if (isset($category) && !empty($category)) : ?>
                                            <?php foreach ($category AS $key => $value): ?>
                                                <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>														
                                            <?php endforeach; ?>
                                        <?php endif; ?>	
                                    </select>
                                    <label for="form_control_1"><?php echo $this->lang->line('global_category'); ?></label>
                                </div>
                                <div class="form-group col-md-4 form-md-line-input form-md-floating-label has-info" id="main_category">
                                    <select name="category_2" class="form-control edited second_ctg" id="second_ctg">
                                        <option>-- select category first --</option>
                                    </select>
                                    <label for="form_control_1"><?php echo $this->lang->line('global_problem_subject'); ?></label>
                                </div>
                                <div class="form-group col-md-4 form-md-radios">
                                    <label><?php echo $this->lang->line('global_priority'); ?></label>
                                    <div class="md-radio-inline">
                                        <?php if (isset($priority) && !empty($priority)) : ?>
                                            <?php $id = 1; ?>
                                            <?php foreach ($priority AS $key => $value): ?>
                                                <div class="md-radio <?php echo $value['style']; ?>">
                                                    <input type="radio" id="priority<?php echo $id; ?>" name="priority" class="md-radiobtn" value="<?php echo $value['id']; ?>"<?php echo ($value['checked'] == 1) ? ' checked=""' : ''; ?>>
                                                    <label for="priority<?php echo $id; ?>">
                                                        <span></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span> <?php echo $value['name']; ?> 
                                                    </label>
                                                </div>				
                                                <?php $id++; ?>												
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group form-md-line-input">
                                <textarea name="issue" class="form-control" rows="3" placeholder="<?php echo $this->lang->line('global_issue_msg'); ?>"></textarea>
                                <label for="form_control_1"><?php echo $this->lang->line('global_issue'); ?></label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <!-- drop area -->
                            <div id="droparea" style="overflow: auto">
                                <div class="dropareainner">
                                    <p class="dropfiletext">Drop files here</p>
                                    <p id="err">Wait there! You must ENABLE Javascript to have this works!</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">	
                            <div class="form-actions noborder">
                                <button type="button" class="btn blue" id="issue_ticket" ><?php echo $this->lang->line('global_submit'); ?></button>
                                <button type="button" class="btn default"><?php echo $this->lang->line('global_cancel'); ?></button>
                            </div>
                        </div>						
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>		