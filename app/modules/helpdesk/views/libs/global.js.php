<script>
    var fnLoadingImg = function (gif) {
        return '<img class="page-loading" src="' + static_url + 'images/' + gif + '"></img>';
    };

    var fnClearBind = function (e) {
        if (sessionStorage.token != "abide") {
            // call api
        }
    };

    var fnBeforeLoadPage = function (txt) {
        console.log('load');
        $(window).bind('beforeunload', function () {
            if (txt) {
                return txt;
            } else {
                return 'are you sure you want to leave?';
            }
        });
    };

    var fnToStr = function (value, key, to) {
        if (dev_status) {
            switch (key) {
                case 'success':
                    toastr.success(value, key, {timeOut: to});
                    break;
                case 'warning':
                    toastr.warning(value, key, {timeOut: to});
                    break;
                case 'info':
                    toastr.info(value, key, {timeOut: to});
                    break;
                case 'error':
                    toastr.error(value, key, {timeOut: to});
                    break;
            }
        }
    };

    var fnCloseModal = function () {
        App.startPageLoading();
        $(".modal").hide();
        $('.modal').modal('hide');
        fnResetBtn();
        $("form")[0].reset();
        //fnRefreshDataTable();
        App.stopPageLoading();
    };

    var fnResetBtn = function () {
        App.startPageLoading();
        $("#opt_delete").attr("disabled", true);
        $("#opt_delete").addClass("disabled");

        $("#opt_remove").attr("disabled", true);
        $("#opt_remove").addClass("disabled");

        $("#opt_add").attr("disabled", false);
        $("#opt_add").removeClass("disabled");

        $("#opt_edit").attr("disabled", true);
        $("#opt_edit").addClass("disabled");
        App.stopPageLoading();
    };

    var fnCloseBootbox = function () {
        App.startPageLoading();
        $(".bootbox").hide();
        $(".modal-backdrop").hide();
        fnRefreshDataTable();
    };

    var fnRefreshDataTable = function () {
        App.startPageLoading();
        $(".table").DataTable().ajax.reload();
        $('input[id="select_all"]').prop('checked', false);
        App.stopPageLoading();
    };

    var fnActionId = function (url_post, id, options) {
        $.ajax({
            url: url_post,
            method: "POST", //First change type to method here
            data: formdata,
            success: function (response) {
                fnToStr(options + ' is successfully!', 'success');
                fnRefreshDataTable();
            },
            error: function () {
                fnToStr(options + ' is failed, please try again or call superuser to help!', 'error');
                fnRefreshDataTable();
            }
        });
        return false;
    };

    var fnCheckResponseBtn = function (id) {
        var return_val = false;
        var url_post = base_url + 'vendor/ticket/check_status_ticket';
        var formdata = {
            id: id
        };
        $.ajax({
            url: url_post,
            method: "POST", //First change type to method here
            data: formdata,
            success: function (response) {
                if (response == 'true') {
                    return true;
                } else {
                    return false;
                }
            }
        });
    };

    var fnGetTimtikUser = function () {
        var url_post = base_url + 'vendor/user/get_user/1';
        $.ajax({
            url: url_post,
            method: "POST",
            success: function (response) {
                $('.timtik').html(response);
            }
        });
        return false;
    };

    var fnGetVendorUser = function () {
        var url_post = base_url + 'vendor/user/get_user/2';
        $.ajax({
            url: url_post,
            method: "POST",
            success: function (response) {
                $('.vendor').html(response);
            }
        });
        return false;
    };

    var fnTicketAuth = function (status) {
        switch (status) {
            case 'request-to-close':
                $('#sbmt_message').attr('disabled', 'disabled');
                $('#req_to_close').attr('disabled', 'disabled');
                $('#req_to_close').attr('title', 'This ticket already request to be close...');
                break;
            case 'close':
                $('#re_open').show();
                $('#sbmt_message').attr('disabled', 'disabled');
                $('#req_to_close').hide();
                $('.caption-helper').css('color:red');
                break;
            case 'progress':
                $('#re_open').hide();
                $('#mark_as_solve').attr('disabled', 'disabled');
                break;
            case 'open':
                $('#re_open').hide();
                break;
        }
    };

    var GlobalAjax = function () {
        return {
            //main function to initiate the module
            init: function () {

                fnToStr('Global js ready!!!', 'success', 1100);

                $('span#Open').html(open_total);
                $('span#Progress').html(progress_total);
                $('span#Close').html(close_total);
                $('span#CloseRequest').html(close_request_total);

                $('button[type="button"]').on('click', function () {
                    var dismiss = $(this).attr('data-dismiss');
                    App.startPageLoading();
                    switch (dismiss) {
                        case 'modal':
                            $('.modal').modal('hide');
                            fnRefreshDataTable();
                            fnResetBtn();
                            break;
                    }
                    App.stopPageLoading();
                });

                $('table#datatable_ajax').on('click', '#select_all', function () {
                    var is_checked = $(this).is(':checked');
                    if (is_checked == true) {
                        $('input[type="checkbox"]').prop('checked', true);
                    } else {
                        $('input[type="checkbox"]').prop('checked', false);
                        $('input[id="select_all"]').prop('checked', false);
                    }
                });

                $('table#datatable_ajax').on('click', '.md-check', function () {
                    var id = $(this).attr('data-id');
                    var is_checked = $('input.select_tr').is(':checked');
                    if (is_checked == true) {
                        var count = $('input.select_tr').filter(':checked').length;
                        if (id) {
                            $('input[name="id"]').val(id);
                        }
                        if (count > 1)
                        {
                            $("#opt_delete").attr("disabled", false);
                            $("#opt_delete").removeClass("disabled");

                            $("#opt_remove").attr("disabled", false);
                            $("#opt_remove").removeClass("disabled");

                            $("#opt_add").attr("disabled", true);
                            $("#opt_add").addClass("disabled");

                            $("#opt_edit").attr("disabled", true);
                            $("#opt_edit").addClass("disabled");
                        } else {
                            $("#opt_delete").attr("disabled", false);
                            $("#opt_delete").removeClass("disabled");

                            $("#opt_remove").attr("disabled", false);
                            $("#opt_remove").removeClass("disabled");

                            $("#opt_add").attr("disabled", true);
                            $("#opt_add").addClass("disabled");

                            $("#opt_edit").attr("disabled", false);
                            $("#opt_edit").removeClass("disabled");
                        }
                    } else {
                        $("#opt_delete").attr("disabled", true);
                        $("#opt_delete").addClass("disabled");

                        $("#opt_remove").attr("disabled", true);
                        $("#opt_remove").addClass("disabled");

                        $("#opt_add").attr("disabled", false);
                        $("#opt_add").removeClass("disabled");

                        $("#opt_edit").attr("disabled", true);
                        $("#opt_edit").addClass("disabled");
                    }
                });

                $('table.table').on('click', 'td a.btn', function () {
                    var id = $(this).attr('data-id');
                    var href = $(this).attr('href');
                    var formdata = {
                        id: (id)
                    };
                    $.ajax({
                        url: base_url + 'helpdesk/ticket/get_ticket_detail/',
                        method: "POST", //First change type to method here
                        data: formdata,
                        success: function (data) {
                            var row = JSON.parse(data);
                            var status_ = false;
                            if (row.is_active == 1) {
                                status_ = true;
                            }
                            if (href == '#detail') {
                                //assign into detail view
                                $('input[name="create_date"]').val(row.create_date);
                                $('input[name="code"]').val(row.code);
                                $('input[name="ticket_status"]').val(row.ticket_status);
                                $('input[name="category_name"]').val(row.category_name);
                                $('input[name="job_category_name"]').val(row.job_category_name);
                                $('textarea[name="content"]').val(row.content);
                                $('textarea[name="description"]').val(row.description);
                            }
                        },
                        error: function (data) {
                            toastr.success('Failed response this ticket!');
                            return false;
                        }
                    });
                });
            }
        };
    }();

    jQuery(document).ready(function () {
        GlobalAjax.init();
    });
</script>
