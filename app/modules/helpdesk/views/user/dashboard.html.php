<style>
    #history_dttable_length{
        display:none;
    }

    #history_dttable_filter{
        display:none;
    }

    #history_dttable_info{
        display:none;
    }

    #history_dttable_paginate{
        display:none;
    }
</style>
<div class="row">
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat blue">
            <div class="visual">
                <i class="fa fa-comments"></i>
            </div>
            <div class="details">
                <div class="desc"> <?php echo $this->lang->line('global_create_ticket'); ?> </div>
            </div>
            <a class="more" href="<?php echo base_url('ticket/create'); ?>"> <?php echo $this->lang->line('global_open_form'); ?>
                <i class="m-icon-swapright m-icon-white"></i>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat blue">
            <div class="visual">
                <i class="fa fa-comments"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup"><?php echo isset($_ajax_var_ticket->open) ? $_ajax_var_ticket->open : 0; ?></span>
                </div>
                <div class="desc"> Open <?php echo $this->lang->line('global_ticket'); ?> </div>
            </div>
            <a class="more" href="<?php echo base_url('ticket/view/open'); ?>"> <?php echo $this->lang->line('global_view_more'); ?>
                <i class="m-icon-swapright m-icon-white"></i>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat red">
            <div class="visual">
                <i class="fa fa-bar-chart-o"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup"><?php echo isset($_ajax_var_ticket->progress) ? $_ajax_var_ticket->progress : 0; ?></span></div>
                <div class="desc"> Progress <?php echo $this->lang->line('global_ticket'); ?> </div>
            </div>
            <a class="more" href="<?php echo base_url('ticket/view/progress'); ?>"> <?php echo $this->lang->line('global_view_more'); ?>
                <i class="m-icon-swapright m-icon-white"></i>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat purple">
            <div class="visual">
                <i class="fa fa-globe"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup"><?php echo isset($_ajax_var_ticket->close) ? $_ajax_var_ticket->close : 0; ?></span></div>
                <div class="desc"> Close <?php echo $this->lang->line('global_ticket'); ?> </div>
            </div>
            <a class="more" href="<?php echo base_url('ticket/view/close'); ?>"> <?php echo $this->lang->line('global_view_more'); ?>
                <i class="m-icon-swapright m-icon-white"></i>
            </a>
        </div>
    </div>


    <div class="col-md-12 col-sm-12">
        <div class="portlet light tasks-widget bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i><?php echo $this->lang->line('global_ticket'); ?>
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"> </a>
                </div>
            </div>

            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover table-checkable" id="ticket_dttable">
                    <thead>
                        <tr role="row" class="heading">
                            <th width="2%">
                                <div class="form-group form-md-checkboxes">
                                    <div class="md-checkbox-list">
                                        <div class="md-checkbox">
                                            <input type="checkbox" id="select_all" name="select_all" class="md-check">
                                            <label for="select_all">
                                                <span></span>
                                                <span class="check" style="left:20px;"></span>
                                                <span class="box" style="left:14px;"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </th>
                            <th width="5%"> # </th>
                            <th width="15%"><?php echo $this->lang->line('global_no_ticket'); ?></th>
                            <th width="15%"><?php echo $this->lang->line('global_issue'); ?></th>
                            <th width="15%"><?php echo $this->lang->line('global_category'); ?></th>
                            <th width="15%"><?php echo $this->lang->line('global_job_category'); ?></th>
                            <th width="15%"><?php echo $this->lang->line('global_create_date'); ?></th>
                            <th width="15%"><?php echo $this->lang->line('global_status'); ?></th>
                            <th width="15%"><?php echo $this->lang->line('global_action'); ?></th>
                        </tr>							
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="col-md-5 col-sm-5">
        <div class="portlet light tasks-widget bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i><?php echo $this->lang->line('global_recent_activities'); ?>
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"> </a>
                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover table-checkable" id="history_dttable">
                    <thead>
                        <tr role="row" class="heading">
                            <th></th><th></th>
                        </tr>							
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('includes/tools/detail_ticket.html.php'); ?>