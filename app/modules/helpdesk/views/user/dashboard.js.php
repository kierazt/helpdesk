<script>
    var Ajax = function () {
        return {
            //main function to initiate the module
            init: function () {
                toastr.success('Dashboard js ready!!!');
                var table = $('#ticket_dttable').DataTable({
                    "lengthMenu": [[10, 25, 50], [10, 25, 50]],
                    "sPaginationType": "bootstrap",
                    "paging": true,
                    "pagingType": "full_numbers",
                    "ordering": false,
                    "serverSide": true,
                    "ajax": {
                        url: base_url + 'ticket/get_list',
                        type: 'POST'
                    },
                    "columns": [
                        {"data": "rowcheck"},
                        {"data": "num"},
                        {"data": "code"},
                        {"data": "content"},
                        {"data": "category_name"},
                        {"data": "job_category_name"},
                        {"data": "create"},
                        {"data": "status"},
                        {"data": "action"}
                    ],
                    "drawCallback": function (master) {
                        $('.make-switch').bootstrapSwitch();
                    }
                });
                $('table#ticket_dttable').on('click', 'td a.btn', function () {
                    var id = $(this).attr('data-id');
                    var href = $(this).attr('href');

                    var formdata = {
                        id: Base64.encode(id)
                    };
                    $.ajax({
                        url: base_url + 'helpdesk/ticket/get_ticket_detail/',
                        method: "POST", //First change type to method here
                        data: formdata,
                        success: function (response) {
                            var row = JSON.parse(response);
                            var status_ = false;
                            if (row.is_active == 1) {
                                status_ = true;
                            }
                            if (href == '#detail') {
                                //assign into detail view
                                $('input[name="create_date"]').val(row.create_date);
                                $('input[name="code"]').val(row.code);
                                $('input[name="ticket_status"]').val(row.ticket_status);
                                $('input[name="category_name"]').val(row.category_name);
                                $('input[name="job_category_name"]').val(row.job_category_name);
                                $('textarea[name="content"]').val(row.content);
                                $('textarea[name="description"]').val(row.description);
                            }
                            return false;
                        },
                        error: function () {
                            toastr.error('Failed ' + response);
                            alert('fail');
                            return false;
                        }
                    });
                });
            }
        };

    }();

    jQuery(document).ready(function () {
        Ajax.init();
    });

</script>
