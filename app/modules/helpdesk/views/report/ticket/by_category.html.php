<div class="row">
    <div class="col-md-12 ">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject font-dark sbold uppercase">Horizontal Form</span>
                </div>
            </div>
            <div class="portlet-body form">
                <form class="form-horizontal" role="form" id="report_btn_table" autocomplete="off">
                    <div class="col-md-6">
                        <div class="form-body">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Ticket Status</label>
                                <div class="col-md-9">
                                    <select class="form-control" name="ticket_status" id="ticket_status">
                                        <option value="0">-- select all --</option>
                                        <?php if (isset($status) && !empty($status)): ?>
                                            <?php foreach ($status AS $key => $val): ?>
                                                <option value="<?php echo $val['id'] ?>"><?php echo $val['name'] ?></option>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Ticket priority</label>
                                <div class="col-md-9">
                                    <select class="form-control" name="ticket_priority" id="ticket_priority">
                                        <option value="0">-- select all --</option>
                                        <?php if (isset($priority) && !empty($priority)): ?>
                                            <?php foreach ($priority AS $key => $val): ?>
                                                <option value="<?php echo $val['id'] ?>"><?php echo $val['name'] ?></option>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Ticket category</label>
                                <div class="col-md-9">
                                    <select class="form-control" name="ticket_category" id="ticket_category">
                                        <option value="0">-- select all --</option>
                                        <?php if (isset($category) && !empty($category)): ?>
                                            <?php foreach ($category AS $key => $val): ?>
                                                <option value="<?php echo $val['id'] ?>"><?php echo $val['name'] ?></option>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">Submit</button>
                                <button type="button" class="btn default">Cancel</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <!-- Begin: life time stats -->
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject font-dark sbold uppercase">{view-header-title}</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-container tbl_result">
                    <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax" hidden>
                        <thead>
                            <tr>
                                <th width="5%"> # </th>
                                <th width="12%"> Code </th>
                                <th width="12%"> Content </th>
                                <th width="12%"> Status </th>
                                <th width="12%"> Priority </th>
                                <th width="12%"> Category </th>
                                <th width="12%"> Create Date </th>
                                <th width="12%"> Active </th>
                            </tr>								
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
</div>