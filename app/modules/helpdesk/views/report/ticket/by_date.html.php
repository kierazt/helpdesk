<div class="row">
    <div class="col-md-12 ">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject font-dark sbold uppercase">Horizontal Form</span>
                </div>
            </div>
            <div class="portlet-body form">
                <form class="form-horizontal" role="form" id="report_btn_table">
                    <div class="col-md-6">
                        <div class="form-body">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Start Date</label>
                                <div class="col-md-9">
                                    <input class="form-control" name="from" id="from">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">End Date</label>
                                <div class="col-md-9">
                                    <input class="form-control" name="to" id="to">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">Submit</button>
                                <button type="button" class="btn default">Cancel</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <!-- Begin: life time stats -->
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject font-dark sbold uppercase">{view-header-title}</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-container tbl_result">
                    <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax" hidden>
                        <thead>
                            <tr>
                                <th width="5%"> # </th>
                                <th width="15%"> Code </th>
                                <th width="15%"> Content </th>
                                <th width="15%"> Status </th>
                                <th width="15%"> Priority </th>
                                <th width="15%"> Category </th>
                                <th width="200"> Description </th>
                                <th width="15%"> Active </th>
                            </tr>								
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
</div>