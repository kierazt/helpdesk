<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Tracking
 *
 * @author asus
 */
class Tracking extends MY_Controller {

    public function __construct() {
        parent::__construct();
        //$this->load->model(array(''));
    }

    public function index() {
        redirect(base_url('vendor/tracking/view/'));
    }

    public function view($code = null) {
        if ($code == null)
            redirect(base_url('ticket/view/open?redirect=true&msg=Please select only registered ticket code'));
        if (strlen($code) != 28)
            redirect(base_url('ticket/view/open?redirect=true&msg=Please select only registered ticket code'));

        $data['title_for_layout'] = 'Ticket tracking';
        $this->load->model(array('Tbl_helpdesk_tickets', 'Tbl_helpdesk_ticket_chats','Tbl_helpdesk_ticket_files'));
        $data['code'] = base64_decode($code);
        $data['view-header-title'] = 'Track your ticket';

        try {
            $ticket = $this->Tbl_helpdesk_tickets->find('first', array(
                'fields' => array('a.*', 'c.name ticket_status'),
                'conditions' => array('code' => $data['code']),
                'joins' => array(
                    array(
                        'table' => 'tbl_helpdesk_ticket_transactions b',
                        'conditions' => 'b.ticket_id = a.id',
                        'type' => 'left'
                    ),
                    array(
                        'table' => 'tbl_helpdesk_ticket_status c',
                        'conditions' => 'c.id = b.status_id',
                        'type' => 'left'
                    )
                )
                    )
            );
        } catch (exception $e) {
            $ticket = null;
        }

        if ($ticket == null || $ticket == '')
            redirect(base_url('ticket/view/open?redirect=true&msg=Ticket with code ' . $data['code'] . ' cannot found at db'));

        $data['chats'] = $this->Tbl_helpdesk_ticket_chats->find('all', array('conditions' => array('a.ticket_id' => $ticket['id'])));
        $var = array(
            array(
                'keyword' => 'code',
                'value' => $data['code']
            ),
            array(
                'keyword' => 'ticket_id',
                'value' => $ticket['id']
            ),
            array(
                'keyword' => 'ticket_status',
                'value' => $ticket['ticket_status']
            )
        );
        $this->load_ajax_var($var);
        $js_files = array(
            "http://ajax.microsoft.com/ajax/jquery.templates/beta1/jquery.tmpl.min.js",
            static_url('lib/single/modernizr.custom.js'),
            static_url('lib/packages/dropzone/dist/dropzone.js'),
            static_url('templates/metronics/assets/global/plugins/jquery-validation/js/jquery.validate.min.js'),
            static_url('templates/metronics/assets/global/plugins/jquery-validation/js/additional-methods.min.js'),
            static_url('templates/metronics/assets/global/plugins/select2/js/select2.full.min.js'),
            static_url('templates/metronics/assets/global/plugins/backstretch/jquery.backstretch.min.js'),
            static_url('templates/metronics/assets/global/scripts/datatable.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/datatables.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js'),
      
        );
        $this->load_js($js_files);
        $data['ticket'] = $ticket;
        $data['files'] = $this->Tbl_helpdesk_ticket_files->find('all', array('conditions' => array('a.code' => $data['code'])));
        $this->parser->parse('layouts/pages/metronic.phtml', $data);
    }

    public function get_content($ticket_id = null) {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $ticket_id = base64_decode($post['ticket_id']);
            $this->load->model(array('Tbl_helpdesk_ticket_chats', 'Tbl_users'));
            $res = $this->Tbl_helpdesk_ticket_chats->find('all', array('conditions' => array('ticket_id' => $ticket_id), 'order' => array('key' => 'create_date', 'type' => 'DESC')));
            if (isset($res) && !empty($res)) {
                $arr_res = '';
                foreach ($res AS $key => $value) {
                    $user = $this->Tbl_users->find('first', array(
                        'fields' => array('a.*', 'b.img'),
                        'conditions' => array('a.id' => $value['created_by']),
                        'joins' => array(
                            array(
                                'table' => 'tbl_user_profiles b',
                                'conditions' => 'b.user_id = a.id',
                                'type' => 'left'
                            )
                        )
                            )
                    );
                    if ($value['is_vendor'] == 1) {
                        $by = 'Vendor[' . $user['email'] . ']';
                    } else {
                        $by = 'User[' . $user['email'] . ']';
                    }
                    $arr_res .= '<div class="timeline-item">
                        <div class="timeline-badge">
                            <img class="timeline-badge-userpic" src="' . static_url($user['img']) . '"> </div>
                        <div class="timeline-body">
                            <div class="timeline-body-arrow"> </div>
                            <div class="timeline-body-head">
                                <div class="timeline-body-head-caption">
                                    <a href="javascript:;" class="timeline-body-title font-blue-madison">' . $by . '</a>
                                    <span class="timeline-body-time font-grey-cascade">' . idn_date($value['create_date']) . '</span>
                                </div>
                            </div>
                            <div class="timeline-body-content">
                                <span class="font-grey-cascade">' . $value['messages'] . '</span>
                            </div>
                        </div>
                    </div>';
                }
                echo $arr_res;
            }
        }
    }

    public function insert_message() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $this->load->model(array('Tbl_helpdesk_ticket_chats'));
            $ticket_id = base64_decode($post['ticket_id']);
            $arr_insert = array(
                'messages' => $post['message'],
                'ticket_id' => $ticket_id,
                'ticket_code' => $post['ticket_code'],
                'is_vendor' => 1,
                'is_active' => 1,
                'created_by' => (int) base64_decode($this->auth_config->user_id),
                'create_date' => date_now()
            );
            $res = $this->Tbl_helpdesk_ticket_chats->insert($arr_insert);
            if ($res == true) {
                echo 'success';
            } else {
                echo 'failed';
            }
        }
    }

    public function get_ticket_detail() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $this->load->model('Tbl_helpdesk_tickets');
            $id = base64_decode($post['ticket_id']);
            $res = $this->Tbl_helpdesk_tickets->find('first', array(
                'fields' => array('a.*', 'c.name ticket_status', 'e.name category_name', 'f.name job_category_name'),
                'conditions' => array('a.id' => $id),
                'order' => array('key' => 'a.create_date', 'type' => 'ASC'),
                'joins' => array(
                    array(
                        'table' => 'tbl_helpdesk_ticket_transactions b',
                        'conditions' => 'b.ticket_id = a.id',
                        'type' => 'left'
                    ),
                    array(
                        'table' => 'tbl_helpdesk_ticket_status c',
                        'conditions' => 'c.id = b.status_id',
                        'type' => 'left'
                    ),
                    array(
                        'table' => 'tbl_helpdesk_vendor_users d',
                        'conditions' => 'd.category_id = b.category_id',
                        'type' => 'left'
                    ),
                    array(
                        'table' => 'tbl_helpdesk_ticket_categories e',
                        'conditions' => 'e.id = b.category_id',
                        'type' => 'left'
                    ),
                    array(
                        'table' => 'tbl_helpdesk_ticket_categories f',
                        'conditions' => 'f.id = b.job_id',
                        'type' => 'left'
                    )
                )
            ));

            if (isset($res) && !empty($res)) {
                if ($res['create_date']) {
                    $res['create_date'] = idn_date(strtotime($res['create_date']));
                }
                echo json_encode($res);
            } else {
                echo null;
            }
        }
    }

    public function get_job_desc() {
        if (isset($post) && !empty($post)) {
            $this->load->model('Tbl_helpdesk_ticket_categories');
            $res = $this->Tbl_helpdesk_ticket_categories->find('all', array('conditions' => array('parent_id' => $post['job_id'])));
            if (isset($res) && !empty($res)) {
                $rr = '';
                foreach ($res AS $key => $val) {
                    $rr .= '
                        <div class="md-checkbox">
                            <input type="checkbox" data-id="' . $val['id'] . '" id="checkbox' . $val['id'] . '" class="md-check msg_job_list" name="msg_job_list[]">
                            <label for="checkbox' . $val['id'] . '">
                                <span class="inc"></span>
                                <span class="check"></span>
                                <span class="box"></span> ' . $val['name'] . ' 
                            </label>
                        </div>';
                }
                echo $rr;
            } else {
                echo 'data not found...';
            }
        }
    }

    public function close_ticket_request() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $this->load->model(array('Tbl_helpdesk_ticket_requests', 'Tbl_helpdesk_ticket_categories', 'Tbl_helpdesk_activities', 'Tbl_helpdesk_activities', 'Tbl_helpdesk_ticket_transactions'));
            $job_lis = '';
            $rr = '';
            if (isset($post['msg_job_list']) && !empty($post['msg_job_list'])) {
                foreach ($post['msg_job_list'] AS $key => $val) {
                    if ($val == 'on') {
                        $txt = $this->Tbl_helpdesk_ticket_categories->find('first', array('conditions' => array('id' => $key)));
                        if (!empty($rr))
                            $rr .= ', ';
                        $rr .= $txt['name'];
                    }
                }
            }
            $arr = array(
                'ticket_id' => $post['ticket_id'],
                'job_list' => $rr,
                'message' => $post['message'],
                'is_active' => 1,
                'created_by' => (int) base64_decode($this->auth_config->user_id),
                'create_date' => date_now()
            );
            $res = $this->Tbl_helpdesk_ticket_requests->insert($arr);
            if ($res == true) {
                $this->Tbl_helpdesk_ticket_transactions->query("UPDATE `tbl_helpdesk_ticket_transactions` SET `status_id` = '4' WHERE `tbl_helpdesk_ticket_transactions`.`ticket_id` = {$post['ticket_id']};", 'insert'); //($arr_transc, 'ticket_id', $post['ticket_id']);
                $this->Tbl_helpdesk_activities->query("UPDATE `tbl_helpdesk_activities` SET `solving_time_stop` = '" . date_now() . "' WHERE `tbl_helpdesk_activities`.`id` = {$post['ticket_id']};", 'insert');

                echo 'success';
            }
        }
        echo 'failed';
    }

}
