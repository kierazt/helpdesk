<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Ticket
 *
 * @author asus
 */
class Ticket extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(array('Tbl_helpdesk_tickets'));
    }

    public function index() {
        redirect(base_url('vendor/ticket/view/'));
    }

    public function view($keyword = 'open') {
        $data['title_for_layout'] = 'welcome';
        $data['view-header-title'] = 'View Open Ticket List';
        $data['content'] = 'ini kontent web';
        $var = array(
            array(
                'keyword' => 'key',
                'value' => $keyword
            ),
            array(
                'keyword' => 'office_id',
                'value' => isset($this->auth_config->office_id) ? $this->auth_config->office_id : ''
            )
        );
        $this->load_ajax_var($var);
        $js_files = array(
            static_url('templates/metronics/assets/global/scripts/datatable.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/datatables.min.js'),
            static_url('templates/metronics/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js'),
        );
        $this->load_js($js_files);
        $this->parser->parse('layouts/pages/metronic.phtml', $data);
    }

    public function get_list($key = null) {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $this->load->model('Tbl_helpdesk_imigration_branchs');
            $this->load->library('pagination');
            //init config for datatables
            $draw = $post['draw'];
            $start = $post['start'];
            $length = $post['length'];
            $search = trim($post['search']['value']);

            $cond_count = array();
            $cond['table'] = $cond_count['table'] = 'tbl_helpdesk_tickets';
            if (isset($search) && !empty($search)) {
                $cond['like'] = $cond_count['like'] = array('a.name', $search);
            }
            if (isset($key) && !empty($key)) {
                if ($key == 'request-to-close') {
                    $key = 'close_request';
                }
                $cond['conditions'] = $cond_count['conditions'] = array('c.name' => $key);
            }

            //$cond['conditions'] = $cond_count['conditions'] = array('a.created_by' => (int) base64_decode($this->auth_config->user_id));
            $cond['fields'] = array('a.*', 'b.*', 'c.name ticket_status', 'd.response_time_start', 'd.response_time_stop', 'd.transfer_time_start', 'd.transfer_time_stop', 'd.solving_time_start', 'd.solving_time_stop', 'd.is_open', 'e.name category_name', 'f.name job_category_name', 'g.user_id response_by');
            $cond['order'] = array('key' => 'a.create_date', 'type' => 'DESC');
            $cond['limit'] = array('perpage' => $length, 'offset' => $start);
            $cond['group'] = array('a.id');
            $cond['joins'] = $cond_count['joins'] = array(
                array(
                    'table' => 'tbl_helpdesk_ticket_transactions b',
                    'conditions' => 'b.ticket_id = a.id',
                    'type' => 'left'
                ),
                array(
                    'table' => 'tbl_helpdesk_ticket_status c',
                    'conditions' => 'c.id = b.status_id',
                    'type' => 'left'
                ),
                array(
                    'table' => 'tbl_helpdesk_activities d',
                    'conditions' => 'd.ticket_id = a.id',
                    'type' => 'left'
                ),
                array(
                    'table' => 'tbl_helpdesk_ticket_categories e',
                    'conditions' => 'e.id = b.category_id',
                    'type' => 'left'
                ),
                array(
                    'table' => 'tbl_helpdesk_ticket_categories f',
                    'conditions' => 'f.id = b.job_id',
                    'type' => 'left'
                ),
                array(
                    'table' => 'tbl_helpdesk_ticket_handlers g',
                    'conditions' => 'g.ticket_id = a.id',
                    'type' => 'left'
                )
            );
            $total_rows = $this->Tbl_helpdesk_tickets->find('count', $cond_count);
            $config = array(
                'base_url' => base_url('vendor/ticket/get_list/' . $key),
                'total_rows' => $total_rows,
                'per_page' => $length,
            );
            $this->pagination->initialize($config);
            $res = $this->Tbl_helpdesk_tickets->find('all', $cond);
            $arr = array();
            if (isset($res) && !empty($res)) {
                $i = $start + 1;
                foreach ($res as $d) {
                    $status = '';
                    if ($d['is_active'] == 1) {
                        $status = 'checked';
                    }
                    $action_status = '<div class="form-group">
                        <div class="col-md-9" style="height:30px">
                            <input type="checkbox" class="make-switch" data-size="small" data-value="' . $d['is_active'] . '" data-id="' . $d['id'] . '" name="status" ' . $status . '/>
                        </div>
                    </div>';
                    $branch_code = $this->Tbl_helpdesk_imigration_branchs->get_code($d['branch_id']);
                    if ($this->auth_config->group_id == 1) {
                        $branch_code = 'Tim TIK';
                    }
                    $data['rowcheck'] = '
                    <div class="form-group form-md-checkboxes">
                        <div class="md-checkbox-list">
                            <div class="md-checkbox">
                                <input type="checkbox" id="select_tr' . $d['id'] . '" class="md-check select_tr" name="select_tr[' . $d['id'] . ']" data-id="' . $d['id'] . '" />
                                <label for="select_tr' . $d['id'] . '">
                                    <span></span>
                                    <span class="check" style="left:20px;"></span>
                                    <span class="box" style="left:14px;"></span>
                                </label>
                            </div>
                        </div>
                    </div>';
                    $data['num'] = $i;
                    $data['code'] = $d['code']; //optional
                    $data['branch_code'] = $branch_code; //optional 
                    $data['content'] = substr($d['content'], 0, 80); //optional 
                    $data['category_name'] = $d['category_name']; //optional
                    $data['job_category_name'] = $d['job_category_name']; //optional
                    $data['create'] = idn_date($d['create_date']); //optional 
                    $data['status'] = $this->get_btn_ticket_status($d['ticket_status']); //optional   
                    $data['description'] = $d['description']; //optional

                    $dsbl = $dsbl2 = '';
                    if ($d['is_open'] == 1) {
                        $dsbl = ' disabled=""';
                    }

                    if ($d['ticket_status'] == 'open') {
                        $action = '
                            <a class="btn red btn-outline bold" data-toggle="modal" href="#detail" title="Detail" data-id="' . base64_encode($d['id']) . '"> <i class="fa fa-search-plus"></i> </a>                 
                            <a class="btn red btn-outline bold" data-toggle="modal" href="#response" title="Response" data-id="' . base64_encode($d['id']) . '"' . $dsbl . '> <i class="fa fa-pencil-square-o"></i> </a>
                        ';
                    } elseif ($d['ticket_status'] == 'progress') {
                        if ((int) base64_decode($this->auth_config->user_id) == $d['response_by']) {
                            $action = '
                                <a class="btn red btn-outline sbold" data-toggle="modal" href="#detail" title="Detail" data-id="' . base64_encode($d['id']) . '"> <i class="fa fa-search-plus"></i> </a>                    
                                <a class="btn red btn-outline sbold" data-toggle="modal" href="#transfer" title="Transfer" data-id="' . base64_encode($d['id']) . '"><i class="fa fa-mail-forward"></i></a>
                                <a id="' . $d['id'] . '" class="btn red btn-outline sbold" data-toggle="modal" href="' . base_url('vendor/tracking/view/' . base64_encode($d['code'])) . '" title="Tracking" data-id="' . $d['id'] . '"> <i class="fa fa-tripadvisor"></i> </a>
                            ';
                        } else {
                            $action = '
                                <a class="btn red btn-outline sbold" data-toggle="modal" href="#detail" title="Detail" data-id="' . base64_encode($d['id']) . '"> <i class="fa fa-search-plus"></i> </a>                    
                            ';
                        }
                    } elseif ($d['ticket_status'] == 'close_request') {
                        $action = '<a id="' . $d['id'] . '" class="btn red btn-outline sbold" data-toggle="modal" href="' . base_url('vendor/tracking/view/' . base64_encode($d['code'])) . '" title="Tracking" data-id="' . $d['id'] . '"> <i class="fa fa-tripadvisor"></i> </a>
                            ';
                    } elseif ($d['ticket_status'] == 'close') {
                        $action ='';
                    }
                    $data['action'] = $action;
                    $arr[] = $data;
                    $i++;
                }
            }
            $output = array(
                'draw' => $draw,
                'recordsTotal' => $total_rows,
                'recordsFiltered' => $total_rows,
                'data' => $arr,
            );
            //output to json format
            echo json_encode($output);
        } else {
            echo json_encode(array());
        }
    }

    public function get_data($id = null) {
        $res = $this->Tbl_helpdesk_tickets->find('first', array(
            'conditions' => array('id' => $id)
        ));
        if (isset($res) && !empty($res)) {
            echo json_encode($res);
        } else {
            echo null;
        }
    }

    public function insert() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $sess = $this->_session_auth($this->config->session_name);
            $arr_insert = array(
                'code' => $post['code'],
                'content' => $post['issue'],
                'description' => '-',
                'is_active' => 1,
                'created_by' => (int) base64_decode($this->auth_config->user_id),
                'create_date' => date_now()
            );
            $ticket_id = $this->Tbl_helpdesk_tickets->insert_return_id($arr_insert);
            if ($ticket_id) {
                $this->load->model(array('Tbl_helpdesk_ticket_transactions', 'Tbl_helpdesk_ticket_rules', 'Tbl_helpdesk_activities'));
                $rule_id = $this->Tbl_helpdesk_ticket_rules->find('first', array('conditions' => array('id' => (int) $post['priority'])));
                $res = false;
                $arr_trans = array(
                    'ticket_id' => (int) $ticket_id,
                    'category_id' => (int) $post['category'],
                    'job_id' => (int) $post['job'],
                    'status_id' => 1,
                    'branch_id' => (int) $sess['office_id'],
                    'priority_id' => (int) $post['priority'],
                    'rule_id' => $rule_id['id'],
                    'is_active' => 1,
                    'created_by' => (int) base64_decode($this->auth_config->user_id),
                    'create_date' => date_now()
                );
                $res = $this->Tbl_helpdesk_ticket_transactions->insert($arr_trans);
                $arr_activity = array(
                    'ticket_id' => $ticket_id,
                    'response_time_start' => date_now(),
                    'response_time_stop' => '0000-00-00 00:00:00',
                    'transfer_time_start' => '0000-00-00 00:00:00',
                    'transfer_time_stop' => '0000-00-00 00:00:00',
                    'solving_time_start' => '0000-00-00 00:00:00',
                    'solving_time_stop' => '0000-00-00 00:00:00',
                    'is_open' => 0,
                    'created_by' => (int) base64_decode($this->auth_config->user_id),
                    'create_date' => date_now()
                );
                $res = $this->Tbl_helpdesk_activities->insert($arr_activity);
                if ($res == true) {
                    echo 'success';
                }
            }
        }
    }

    public function update() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $status = 0;
            if ($post['active'] == "true") {
                $status = 1;
            }
            $arr = array(
                'name' => $post['name'],
                'description' => $post['description'],
                'is_active' => $status,
            );
            $res = $this->Tbl_helpdesk_tickets->update($arr, base64_decode($post['id']));
            if ($res == true) {
                echo 'success';
            } else {
                echo 'failed';
            }
        } else {
            echo 'failed';
        }
    }

    public function update_status() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $id = base64_decode($post['id']);
            $status = 0;
            if ($post['active'] == "true") {
                $status = 1;
            }
            $arr = array(
                'is_active' => $status
            );
            $res = $this->Tbl_helpdesk_tickets->update($arr, $id);
            if ($res == true) {
                echo 'success';
            } else {
                echo 'failed';
            }
        }
    }

    public function remove() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $id = base64_decode($post['id']);
            $res = $this->Tbl_helpdesk_tickets->remove($id);
            if ($res == true) {
                echo 'success';
            } else {
                echo 'failed';
            }
        }
    }

    public function delete() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $id = base64_decode($post['is']);
            $res = $this->Tbl_helpdesk_tickets->delete($id);
            if ($res == true) {
                echo 'success';
            } else {
                echo 'failed';
            }
        }
    }

    public function tracking($id = null) {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $this->load->model(array('Tbl_helpdesk_ticket_chats', 'Tbl_helpdesk_ticket_transactions'));
            $ticket_id = base64_decode($post['ticket_id']);
            $arr_insert = array(
                'messages' => $post['message'],
                'ticket_id' => $ticket_id,
                'ticket_code' => $post['ticket_code'],
                'is_vendor' => 1,
                'is_active' => 1,
                'created_by' => (int) base64_decode($this->auth_config->user_id),
                'create_date' => date_now()
            );
            $res = $this->Tbl_helpdesk_ticket_chats->insert($arr_insert);
            if ($res == true) {
                $arr_ticket = array(
                    'status_id' => 2
                );
                $this->Tbl_helpdesk_ticket_transactions->update_by($arr_ticket, $ticket_id, 'ticket_id');
                echo 'success';
            } else {
                echo 'failed';
            }
        }
    }

    public function set_open() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $this->load->model('Tbl_helpdesk_activities');
            $res = $this->Tbl_helpdesk_activities->update(array('open_time' => date_now(), 'is_open' => 1), base64_decode($post['id']));
            if (isset($res) && !empty($res)) {
                echo 'success';
            } else {
                echo 'failed';
            }
        }
    }

    public function check_ticket_timeout() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $this->load->model('Tbl_helpdesk_activities');
            $ticket = $this->Tbl_helpdesk_tickets->find('first', array('conditions' => array('code' => base64_decode($post['ticket_code']))));
            $res = $this->Tbl_helpdesk_activities->find('first', array('conditions' => array('id' => $ticket['id'])));
            $max_ticket = 5;
            $sum_ticket = (int) (fn_date_diff_ticket($res['open_time'], date_now()));
            if (($res['is_open'] == 1) && ($sum_ticket <= $max_ticket)) {
                echo 'true';
            } else {
                $res = $this->Tbl_helpdesk_activities->update_by(array('open_time' => '0000-00-00 00:00:00', 'is_open' => 0), $ticket['id'], 'ticket_id');
                echo 'false';
            }
        }
    }

    public function set_close() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $this->load->model('Tbl_helpdesk_activities');
            $res = $this->Tbl_helpdesk_activities->update(array('is_open' => 0), base64_decode($post['id']));
            if (isset($res) && !empty($res)) {
                echo 'success';
            } else {

                echo 'failed';
            }
        }
    }

    public function check_status_ticket() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $this->load->model('Tbl_helpdesk_activities');
            $res = $this->Tbl_helpdesk_activities->find('first', array('conditions' => array('id' => base64_decode($post['id']))));
            if ($res['is_open'] == 0) {
                echo 'true';
            } else {
                echo 'false';
            }
        }
    }

    public function get_ticket_detail() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            $this->load->model('Tbl_helpdesk_tickets');
            $id = base64_decode($post['id']);
            $res = $this->Tbl_helpdesk_tickets->find('first', array(
                'fields' => array('a.*', 'c.name ticket_status', 'e.name category_name', 'f.name job_category_name'),
                'conditions' => array('a.id' => $id),
                'order' => array('key' => 'a.create_date', 'type' => 'ASC'),
                'joins' => array(
                    array(
                        'table' => 'tbl_helpdesk_ticket_transactions b',
                        'conditions' => 'b.ticket_id = a.id',
                        'type' => 'left'
                    ),
                    array(
                        'table' => 'tbl_helpdesk_ticket_status c',
                        'conditions' => 'c.id = b.status_id',
                        'type' => 'left'
                    ),
                    array(
                        'table' => 'tbl_helpdesk_vendor_users d',
                        'conditions' => 'd.category_id = b.category_id',
                        'type' => 'left'
                    ),
                    array(
                        'table' => 'tbl_helpdesk_ticket_categories e',
                        'conditions' => 'e.id = b.category_id',
                        'type' => 'left'
                    ),
                    array(
                        'table' => 'tbl_helpdesk_ticket_categories f',
                        'conditions' => 'f.id = b.job_id',
                        'type' => 'left'
                    )
                )
            ));

            if (isset($res) && !empty($res)) {
                if ($res['create_date']) {
                    $res['create_date'] = idn_date(strtotime($res['create_date']));
                }
                echo json_encode($res);
            } else {
                echo null;
            }
        }
    }

    public function response_ticket() {
        $post = $this->input->post(NULL, TRUE);
        if (isset($post) && !empty($post)) {
            //set ticket status into progress
            $this->load->model(array('Tbl_helpdesk_ticket_chats', 'Tbl_helpdesk_ticket_handlers', 'Tbl_helpdesk_ticket_transactions', 'Tbl_helpdesk_activities'));
            $ticket_id = base64_decode($post['ticket_id']);
            $arr_insert = array(
                'messages' => $post['message'],
                'ticket_id' => $ticket_id,
                'ticket_code' => $post['ticket_code'],
                'is_vendor' => 1,
                'is_active' => 1,
                'created_by' => (int) base64_decode($this->auth_config->user_id),
                'create_date' => date_now()
            );
            $res = $this->Tbl_helpdesk_ticket_chats->insert($arr_insert);
            if ($res == true) {
                $arr_ticket = array(
                    'status_id' => 2
                );
                $this->Tbl_helpdesk_ticket_transactions->update_by($arr_ticket, $ticket_id, 'ticket_id');
                $arr_activity = array(
                    'response_time_stop' => date_now(),
                    'solving_time_start' => date_now(),
                    'is_open' => 0,
                    'created_by' => (int) base64_decode($this->auth_config->user_id),
                    'create_date' => date_now()
                );
                $res = $this->Tbl_helpdesk_activities->update_by($arr_activity, $ticket_id, 'ticket_id');
                if ($res == true) {
                    $arr_handler_ticket = array(
                        'ticket_id' => $ticket_id,
                        'user_id' => (int) base64_decode($this->auth_config->user_id),
                        'group_id' => (int) ($this->auth_config->group_id),
                        'ticket_status' => 2,
                        'is_active' => 1,
                        'created_by' => (int) base64_decode($this->auth_config->user_id),
                        'create_date' => date_now()
                    );
                    $this->Tbl_helpdesk_ticket_handlers->insert($arr_handler_ticket);
                    echo 'success';
                }
            } else {
                echo 'failed';
            }
        }
    }

}
