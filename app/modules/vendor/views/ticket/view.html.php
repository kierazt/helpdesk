<div class="row">
    <div class="col-md-12">
        <!-- Begin: life time stats -->
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="glyphicon glyphicon-list-alt"></i>
                    <span class="caption-subject font-dark bold uppercase"><?php echo $this->lang->line('global_ticket_list'); ?></span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-container">
                    <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax">
                        <thead>
                            <tr role="row" class="heading">
                                <th width="5%"> # </th>
                                <th width="15%"> <?php echo $this->lang->line('global_no_ticket'); ?> </th>
                                <th width="15%"> <?php echo $this->lang->line('global_issue'); ?> </th>
                                <th width="15%"> Category </th>
                                <th width="15%"> Job Category </th>
                                <th width="15%"> Date </th>
                                <th width="15%"> <?php echo $this->lang->line('global_status'); ?> </th>
                                <th width="15%"> <?php echo $this->lang->line('global_action'); ?></th>
                            </tr>							
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
</div>
<?php $this->load->view('includes/tools/detail_ticket.html.php'); ?>
<?php $this->load->view('includes/tools/response_ticket.html.php'); ?>
<?php $this->load->view('includes/tools/transfer_ticket.html.php'); ?>