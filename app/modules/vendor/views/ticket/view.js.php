<script>
    var index = function () {
        return {
            //main function to initiate the module
            init: function () {
                toastr.success('ticket view js ready!!!');
                $('table#datatable_ajax').on('click', 'td a.btn', function () {
                    var id = $(this).attr('data-id');
                    $.post(base_url + 'vendor/user/get_ticket_detail/' + id, function (data) {
                        var row = JSON.parse(data);
                        var status_ = false;
                        if (row.is_active == 1) {
                            status_ = true;
                        }
                        $('input[name="ticket_id"]').val(row.id);
                        $('input[name="code"]').val(row.code);
                        $('input[name="create_date"]').val(row.create_date);
                        $('input[name="category_name"]').val(row.category_name);
                        $('input[name="job_category_name"]').val(row.job_category_name);
                        $('textarea[name="content"]').val(row.content);
                        $('textarea[name="description"]').val(row.description);
                        $('input[name="ticket_status"]').val(row.ticket_status);
                    });
                });
                $('input[type="checkbox"][name="insert_by"]').on('click', function () {
                    var id = $(this).prop("checked");
                    console.log(id);
                    if (id == true) {
                        $.post(base_url + 'vendor/user/get_issue_suggest/' + id, function (data) {
                            $('#issue_template').html(data);
                            $('#insert_from_temp').fadeIn();
                        });
                    } else {
                        $('#insert_from_temp').fadeOut();
                    }
                });
                $('#issue_template').on('change', function () {
                    var value = $(this).val();
                    $('textarea[name="message"]').val(value);
                });
                $('input[type="checkbox"][name="agree"]').on('click', function () {
                    var id = $(this).prop("checked");
                    if (id == true) {
                        $('#sbmt_form').fadeIn();
                    } else {
                        $('#sbmt_form').fadeOut();
                    }
                });
                var table = $('#datatable_ajax').DataTable({
                    "lengthMenu": [[10, 25, 50], [10, 25, 50]],
                    "sPaginationType": "bootstrap",
                    "paging": true,
                    "pagingType": "full_numbers",
                    "ordering": false,
                    "serverSide": true,
                    "ajax": {
                        url: base_url + 'vendor/ticket/get_list/' + key,
                        type: 'POST'
                    },
                    "columns": [
                        {"data": "num"},
                        {"data": "code"},
                        {"data": "content"},
                        {"data": "category_name"},
                        {"data": "job_category_name"},
                        {"data": "create"},
                        {"data": "status"},
                        {"data": "action"}
                    ],
                    "drawCallback": function (master) {
                        $('.make-switch').bootstrapSwitch();
                    }
                });
            }
        };

    }();

    jQuery(document).ready(function () {
        index.init();
    });

</script>
