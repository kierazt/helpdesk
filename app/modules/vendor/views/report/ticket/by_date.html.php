<div class="row">
    <div class="col-md-12 ">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="glyphicon glyphicon-filter"></i>
                    <span class="caption-subject font-dark bold uppercase"><?php echo $this->lang->line('global_report_filter');?></span>
                </div>
            </div>
            <div class="portlet-body form">
                <form class="form-horizontal" role="form" id="report_btn_table">
                    <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3" style="width: 15%"><?php echo $this->lang->line('global_start_date'); ?></label>
                        <div class="col-sm-3">
                            <div class="input-group date from_datetime">
                               <input class="form-control" type="text" name="from" id="from"/>
                                <span class="input-group-btn">
                                    <button class="btn default date-set" type="button">
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3" style="width: 15%"><?php echo $this->lang->line('global_end_date'); ?></label>
                        <div class="col-sm-3">
                            <div class="input-group date to_datetime">
                                <input class="form-control" type="text" name="to" id="to" /> 
                                <span class="input-group-btn">
                                    <button class="btn default date-set" type="button">
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                  </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green"><?php echo $this->lang->line('global_submit');?></button>
                                <button type="button" class="btn default"><?php echo $this->lang->line('global_cancel');?></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <!-- Begin: life time stats -->
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="glyphicon glyphicon-file"></i>
                    <span class="caption-subject font-dark bold uppercase"><?php echo $this->lang->line('global_report_date_title');?></span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-container tbl_result">
                    <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax" hidden>
                        <thead>
                            <tr>
                                <th width="5%"> # </th>
                                <th width="15%"><?php echo $this->lang->line('global_no_ticket');?></th>
                                <th width="15%"><?php echo $this->lang->line('global_content');?></th>
                                <th width="15%"><?php echo $this->lang->line('global_status');?></th>
                                <th width="15%"><?php echo $this->lang->line('global_priority');?></th>
                                <th width="15%"><?php echo $this->lang->line('global_category');?></th>
                                <th width="200"><?php echo $this->lang->line('global_create_date');?></th>
                                <th width="15%"><?php echo $this->lang->line('global_active');?></th>
                            </tr>								
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
</div>