<script>
    var Ajax = function () {
        return {
            //main function to initiate the module
            init: function () {
                toastr.success('Dashboard js ready!!!');

                $('input[type="checkbox"][name="insert_by"]').on('click', function () {
                    var id = $(this).prop("checked");
                    if (id == true) {
                        $.post(base_url + 'vendor/user/get_issue_suggest/' + id, function (data) {
                            $('#issue_template').html(data);
                            $('#insert_from_temp').fadeIn();
                        });
                    } else {
                        $('#insert_from_temp').fadeOut();
                    }
                });
                $('#issue_template').on('change', function () {
                    var value = $(this).val();
                    $('textarea[name="message"]').val(value);
                    $('#issue_template').prop('selectedIndex', 0);
                });
                $('input[type="checkbox"][name="agree"]').on('click', function () {
                    var id = $(this).prop("checked");
                    if (id == true) {
                        $('#sbmt_form').fadeIn();
                    } else {
                        $('#sbmt_form').fadeOut();
                    }
                });
                var table = $('#ticket_dttable').DataTable({
                    "lengthMenu": [[10, 25, 50], [10, 25, 50]],
                    "sPaginationType": "bootstrap",
                    "paging": true,
                    "pagingType": "full_numbers",
                    "ordering": false,
                    "serverSide": true,
                    "ajax": {
                        url: base_url + 'vendor/ticket/get_list/',
                        type: 'POST'
                    },
                    "columns": [
                        {"data": "num"},
                        {"data": "code"},
                        {"data": "content"},
                        {"data": "category_name"},
                        {"data": "job_category_name"},
                        {"data": "status"},
                        {"data": "create"},
                        {"data": "action"}
                    ],
                    "drawCallback": function (master) {
                        $('.make-switch').bootstrapSwitch();
                    }
                });
                var table2 = $('#history_dttable').DataTable({
                    "lengthMenu": [[10, 25, 50], [10, 25, 50]],
                    "sPaginationType": "bootstrap",
                    "paging": true,
                    "pageLength": 10,
                    "autoWidth": false,
                    "pagingType": "full_numbers",
                    "ordering": false,
                    "processing": true,
                    "language": {
                        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
                    },
                    "serverSide": true,
                    "ajax": {
                        url: base_url + 'vendor/user/get_history/',
                        type: 'POST'
                    },
                    "columns": [
                        {"data": "activities", "width": "80%"},
                        {"data": "date", "width": "20%"}
                    ]
                });
                
                $('#sbmt_form').on('click', function () {
                    App.startPageLoading();
                    var uri = base_url + 'vendor/ticket/tracking';
                    var ticket_code = $('input[name="ticket_code"]').val();
                    var formdata = {
                        ticket_id: Base64.encode($('input[name="ticket_id"]').val()),
                        ticket_code: ticket_code,
                        message: $('textarea[name="message"]').val()
                    };
                    $.ajax({
                        url: uri,
                        method: "POST", //First change type to method here
                        data: formdata,
                        success: function (response) {
                            App.stopPageLoading();
                            toastr.success('Successfully add new ticket data ');
                            window.location.href = base_url + 'vendor/tracking/view/' + Base64.encode(ticket_code);
                            return false;
                        },
                        error: function () {
                            App.stopPageLoading();
                            toastr.success('Failed add new ticket data ');
                            return false;
                        }

                    });
                    return false;
                });
            }
        };

    }();

    jQuery(document).ready(function () {
        Ajax.init();
    });

</script>