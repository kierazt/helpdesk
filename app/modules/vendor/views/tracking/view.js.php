<script>
    var fnAutoLoadMessage = function () {
        $.ajaxSetup({cache: false}); // This part addresses an IE bug.  without it, IE will only load the first number and will never refresh

        App.startPageLoading();
        var uri = base_url + 'vendor/tracking/get_content/';
        var formdata = {
            ticket_id: Base64.encode(ticket_id)
        };
        setInterval(function () {
            $.ajax({
                url: uri,
                type: "post",
                data: formdata,
                success: function (response) {
                    App.stopPageLoading();
                    $('.timeline').html(response);
                    return false;
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    App.stopPageLoading();
                    console.log(textStatus, errorThrown);
                    return false;
                }
            });
            App.stopPageLoading();
        }, 5000);
    };

    var fnLoadDetailTicket = function () {
        var uri = base_url + 'vendor/tracking/get_ticket_detail/';
        var formdata = {
            ticket_id: Base64.encode(ticket_id)
        };
        $.ajax({
            url: uri,
            type: "post",
            data: formdata,
            success: function (response) {
                var row = JSON.parse(response);
                $('input[name="create_date"]').val(row.create_date);
                $('input[name="code"]').val(row.code);
                $('input[name="ticket_id"]').val(row.id);
                $('input[name="ticket_status"]').val(row.ticket_status);
                $('input[name="job_id"]').val(row.job_id);
                $('textarea[name="content"]').val(row.content);
                $('textarea[name="description"]').val(row.description);
                return false;
            },
            error: function (jqXHR, textStatus, errorThrown) {
                App.stopPageLoading();
                console.log(textStatus, errorThrown);
                return false;
            }
        });
    };

    var index = function () {
        return {
            //main function to initiate the module
            init: function () {
                toastr.success('ticket view js ready!!!');
                fnAutoLoadMessage();
                fnLoadDetailTicket();

                if (ticket_status) {
                    fnTicketAuth(ticket_status);
                }
                $('.f_attachment').on('click', function () {
                    var id = $(this).data('id');
                    var path = static_url + 'tickets/' + $(this).data('path');
                    $('#media').html('<img style="width:100%; height:100%; margin:10px" src="' + path + '" />');
                });
                $('a.btn').on('click', function () {
                    var toggle = $(this).data('toggle');
                    var href = $(this).attr('href');
                    if (toggle == 'modal' && href == '#request') {
                        var job_id = $('input[name="job_id"]').val();
                        console.log(job_id);
                        var uri = base_url + 'vendor/tracking/get_job_desc';
                        var formdata = {
                            job_id: job_id
                        };
                        $.ajax({
                            url: uri,
                            type: "post",
                            data: formdata,
                            success: function (response) {
                                App.stopPageLoading();
                                console.log(response);
                                $('#job_list_').html(response);
                                return false;
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                App.stopPageLoading();
                                console.log(textStatus, errorThrown);
                                return false;
                            }
                        });
                    }
                });
                $('#sbmt_message').on('click', function () {
                    var uri = base_url + 'vendor/tracking/insert_message';
                    var formdata = {
                        ticket_id: Base64.encode(ticket_id),
                        ticket_code: code,
                        message: $('textarea[name="message"]').val()
                    };
                    $.ajax({
                        url: uri,
                        type: "post",
                        data: formdata,
                        success: function (response) {
                            App.stopPageLoading();
                            fnAutoLoadMessage();
                            $('textarea[name="message"]').val('');
                            return false;
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            App.stopPageLoading();
                            console.log(textStatus, errorThrown);
                            return false;
                        }
                    });
                });
            }
        };

    }();

    jQuery(document).ready(function () {
        index.init();
    });

</script>
