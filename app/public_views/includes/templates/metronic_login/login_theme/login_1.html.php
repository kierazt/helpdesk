<!--BEGIN LOGO -->
<div class="logo">
    <a href="index.html">
        <img style="width:10%"  src="<?php echo static_url('images\logo\logo-imigrasi.png') ?>" alt="" /> 
    </a> 
</div>
<!-- END LOGO -->
<!-- BEGIN LOGIN -->
<div class="content">
    <!-- BEGIN LOGIN FORM -->
    <h3 class="uppercase" style="color:#555"><b>Helpdesk Imigrasi<b></h3>
                <form class="login-form" method="post">
                    <div class="form-group">
                        <div class="input-icon" >
                            <i class="fa fa-user" style="color: #6666668a"></i>
                            <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="<?php echo $this->lang->line('global_username'); ?>" name="username"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-icon">
                            <i class="fa fa-lock" style="color: #6666668a"></i>
                            <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="<?php echo $this->lang->line('global_password'); ?>" name="password"/> 
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn green uppercase"><?php echo $this->lang->line('global_login'); ?></button>
                        <a href="javascript:;" id="forget-password" class="forget-password"><?php echo $this->lang->line('global_forgot_password'); ?></a> 
                    </div>        
                </form> 

                </div>
                <footer id="footer">
                    <div class="text-center padder clearfix">
                        <p>
                            <small style="color: #fff"><b>Direktorat Jenderal Imigrasi</b><br><b>&copy; 2019</b></small>
                        </p>
                    </div>
                </footer>
                <?php echo isset($_ajax_var_configs->copyright) ? $_ajax_var_configs->copyright : ''; ?> </div>