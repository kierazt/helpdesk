<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
            <li class="sidebar-toggler-wrapper hide">
                <div class="sidebar-toggler"> </div>
            </li>
            <li class="sidebar-search-wrapper">
                <div class="row" style="padding:30px;">
                    <label><?php echo $this->lang->line('global_language'); ?></label>
                    <?php $this->load->view('includes/tools/change_lang.html.php'); ?>
                </div>
            </li>
            <li class="nav-item start">
                <a href="<?php echo base_url($_var_template->_base_url . 'dashboard'); ?>" class="nav-link nav-toggle">
                    <i class="icon-home"></i>
                    <span class="title">Dashboard</span>
                </a>
            </li>	
            <?php $this->load->view('includes/templates/metronics/sidebar_menu/helpdesk.php') ?>	
            <?php $this->load->view('includes/templates/metronics/sidebar_menu/backend.php') ?>		
            <?php $this->load->view('includes/templates/metronics/sidebar_menu/vendor.php') ?>		
        </ul>
    </div>
</div>