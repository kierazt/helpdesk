<div class="page-header navbar navbar-fixed-top">
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner ">
        <!-- BEGIN LOGO -->
        <div class="page-logo">
            <p style="float:left; margin: 10px 0; width:80%">
                <a href="<?php echo base_url('dashboard'); ?>">
                    <img style="width:20%" src="<?php echo static_url('images\logo\logo-imigrasi.png') ?>" alt="logo" /> 
                    <small>HELPDESK</small>
                </a>
            </p>
            <div style="float:right; margin: 5px 0; width:20%" class="menu-toggler sidebar-toggler"> </div>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
        <!-- END RESPONSIVE MENU TOGGLER -->
        <!-- BEGIN TOP NAVIGATION MENU -->
        <div class="top-menu">
            <ul class="nav navbar-nav pull-right">
                <!-- BEGIN NOTIFICATION DROPDOWN -->
                <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                <?php if ($_load_auth_config_var['group_name'] == 'superuser'): ?>
                    <!--<li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <i class="icon-bell"></i>
                            <span class="badge badge-default"> <?php //echo isset($_ajax_var_ticket) ? $_ajax_var_ticket->open : 0; ?> </span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="external">
                                <h3>
                                    <span class="bold"><?php //echo isset($_ajax_var_ticket) ? $_ajax_var_ticket->open : 0; ?> Open</span> Ticket
                                </h3>
                                <a href="<?php //echo base_backend_url('ticket/master/view/open'); ?>">view all</a>
                            </li>
                            <li>
                                <ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283">
                                    <?php //echo isset($_ajax_var_notif_ticket_open) ? $_ajax_var_notif_ticket_open : ''; ?>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <i class="icon-bell"></i>
                            <span class="badge badge-default"> <?php //echo isset($_ajax_var_ticket) ? $_ajax_var_ticket->progress : 0; ?> </span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="external">
                                <h3>
                                    <span class="bold"><?php //echo isset($_ajax_var_ticket) ? $_ajax_var_ticket->progress : 0; ?> Progress</span> Ticket
                                </h3>
                                <a href="<?php //echo base_backend_url('ticket/master/view/progress'); ?>">view all</a>
                            </li>
                            <li>
                                <ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283">
                                    <?php// echo isset($_ajax_var_notif_ticket_progress) ? $_ajax_var_notif_ticket_progress : ''; ?>
                                </ul>
                            </li>
                        </ul>
                    </li>-->
                    <li class="dropdown dropdown-user">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <img alt="" class="img-circle" src="<?php echo isset($_load_auth_config_var['img']) ? static_url($_load_auth_config_var['img']) : static_url('templates/metronics/assets/layouts/layout/img/avatar3_small.jpg'); ?>" />
                            <span class="username username-hide-on-mobile"> <?php echo isset($_load_auth_config_var['email']) ? $_load_auth_config_var['email'] : ''; ?> </span>
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-default">
                            <li>
                                <a href="<?php echo base_url($_var_template->_module . '/my-profile'); ?>">
                                    <i class="icon-user"></i> My Profile 
                                </a>
                            </li>
                            <li class="divider"> </li>
                            <li>
                                <a href="<?php echo base_url($_var_template->_module . '/lock-screen'); ?>">
                                    <i class="icon-lock"></i> Lock Screen 
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url($_var_template->_module . '/logout'); ?>">
                                    <i class="icon-logout"></i> Log Out 
                                </a>
                            </li>
                        </ul>
                    </li>
                <?php else : ?>
                    <li class="dropdown dropdown-user">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <img alt="" class="img-circle" src="<?php echo isset($_load_auth_config_var['img']) ? static_url($_load_auth_config_var['img']) : static_url('templates/metronics/assets/layouts/layout/img/avatar3_small.jpg'); ?>" />
                            <span class="username username-hide-on-mobile"> <?php echo isset($_load_auth_config_var['email']) ? $_load_auth_config_var['email'] : ''; ?> </span>
                        </a>
                    </li>
                <?php endif; ?>
            </ul>
        </div>
        <!-- END TOP NAVIGATION MENU -->
    </div>
    <!-- END HEADER INNER -->
</div>
