<!-- /.modal -->
<div class="modal fade bs-modal-lg" id="request" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Request ticket to close...</h4>
            </div>
            <div class="modal-body">
                <div class="form-group form-md-checkboxes">
                    <label>Job Lists</label>
                    <div class="md-checkbox-inline" id="job_list_"></div>
                </div>
                <div class="form-group">
                    <label>Message</label>
                    <textarea class="form-control" rows="3" name="msg_close_ticket"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn green" id="finish_solving_ticket">Submit</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>