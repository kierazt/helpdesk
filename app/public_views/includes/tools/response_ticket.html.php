<div class="modal fade bs-modal-lg" id="response" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form class="form-horizontal" role="form" id="submit_response">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                </div>
                <div class="modal-body">
                    <center><h4 class="modal-title"><?php echo $this->lang->line('global_response_take_open_ticket'); ?></h4></center>

                    <div class="col-md-12 col-sm-12" style="padding:20px">
                        <div class="col-md-4">
                            <div class="form-group form-md-line-input">
                                <label for="form_control_1"><?php echo $this->lang->line('global_ticket_number'); ?></label>
                                <h4 id="code"></h4>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group form-md-line-input">
                                <label for="form_control_1"><?php echo $this->lang->line('global_ticket_create_date'); ?></label>
                                <h4 id="create_date"></h4>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group form-md-line-input">
                                <label>Issue</label>
                                <blockquote style="border:1px dashed #ccc; border-radius:7px">
                                    <p id="content"></p>
                                </blockquote>
                            </div>
                        </div>


                        <div class="col-md-12">
                            <div class="form-group form-md-line-input">
                                <div class="md-checkbox-inline">
                                    <div class="md-checkbox ">
                                        <input type="checkbox" id="insert_by2" name="insert_by" value="2" class="md-check">
                                        <label for="insert_by2">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span> <?php echo $this->lang->line('global_insert_msg_frm_iss_suggest'); ?>  
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <hr/>
                            <div class="form-group form-md-line-input has-info" hidden id="insert_from_temp">
                                <select class="form-control" id="issue_template"></select><br/>
                                <label for="issue_template"><?php echo $this->lang->line('global_iss_suggest_msg'); ?></label>
                            </div>
                            <div class="form-group form-md-line-input form-md-floating-label">
                                <textarea class="form-control" rows="3" id="message" name="message" placeholder="<?php echo $this->lang->line('global_iss_suggest_msg'); ?>"></textarea>
                            </div>
                            <hr/>
                            <div class="form-group form-md-checkboxes">
                                <label><?php echo $this->lang->line('global_aggreement'); ?></label>
                                <div class="md-checkbox-inline">
                                    <div class="md-checkbox has-success">
                                        <input type="checkbox" name="agree" id="agree" class="md-check">
                                        <label for="agree">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span> <?php echo $this->lang->line('global_aggree_response_ticket'); ?>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="text" name="ticket_id" hidden />
                    <input type="text" name="ticket_code" hidden />
                    <button type="button" class="btn green" id="sbmt_form" style="display:none"><?php echo $this->lang->line('global_submit'); ?></button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>