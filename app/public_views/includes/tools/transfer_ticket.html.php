<!-- /.modal -->
<div class="modal fade bs-modal-lg" id="transfer" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <form id="frmTransfer">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title"><?php //echo $this->lang->line('');  ?> Transfer Ticket</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label class="control-label"><?php echo $this->lang->line('global_ticket_code'); ?></label>
                                <div class="input-icon right">
                                    <i class="fa fa-info-circle tooltips" data-original-title="<?php echo $this->lang->line('global_ticket_code'); ?>" data-container="body"></i>
                                    <input class="form-control" type="text" name="code" readonly=""/> 
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Group</label>
                                <select class="form-control" name="group" id="group">
                                    <option>-- select one --</option>
                                    <option value="1">TimTik</option>
                                    <option value="2">Vendor</option>
                                </select>
                            </div>
                            <div class="form-group" hidden id="timtik_frm">
                                <label>TimTik User</label>
                                <select class="form-control timtik" name="timtik" id="timtik"></select>
                            </div>
                            <div class="form-group" hidden id="vendor_frm">
                                <label>Vendor User</label>
                                <select class="form-control vendor" name="vendor" id="vendor"></select>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="form-group">
                                <label>Note</label>
                                <textarea class="form-control" rows="3" name="note"></textarea>
                            </div>
                        </div>
                    </div>
                </div>		
                <div class="modal-footer">
                    <input type="text" name="ticket_id" hidden />
                    <button type="button" class="btn green" id="sbmt_form_transfer"><?php echo $this->lang->line('global_submit'); ?></button>
                </div>
            </div>
        </form>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>