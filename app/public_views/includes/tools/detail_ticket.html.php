<!-- /.modal -->
<div class="modal fade bs-modal-lg" id="detail" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><?php echo $this->lang->line('global_view_detail_ticket'); ?><small style="float:right; margin-right:20px" class="handle_by"></small></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label"><?php echo $this->lang->line('global_ticket_code'); ?></label>
                            <div class="input-icon right">
                                <i class="fa fa-info-circle tooltips" data-original-title="<?php echo $this->lang->line('global_ticket_code'); ?>" data-container="body"></i>
                                <input class="form-control" type="text" name="code" readonly=""/> 
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label"><?php echo $this->lang->line('global_ticket_create_date'); ?></label>
                            <div class="input-icon right">
                                <i class="fa fa-info-circle tooltips" data-original-title="<?php echo $this->lang->line('global_ticket_create_date'); ?>" data-container="body"></i>
                                <input class="form-control" type="text" name="create_date"  readonly=""/> 
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label"><?php echo $this->lang->line('global_ticket_status'); ?></label>
                            <div class="input-icon right">
                                <i class="fa fa-info-circle tooltips" data-original-title="<?php echo $this->lang->line('global_ticket_status'); ?>" data-container="body"></i>
                                <input class="form-control" type="text" name="ticket_status"  readonly=""/> 
                            </div>
                        </div>

                        <div class="form-group">
                            <label><?php echo $this->lang->line('global_description'); ?></label>
                            <textarea class="form-control" rows="3" name="description" readonly=""></textarea>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label class="control-label"><?php echo $this->lang->line('global_category'); ?></label>
                            <div class="input-icon right">
                                <i class="fa fa-info-circle tooltips" data-original-title="<?php echo $this->lang->line('global_ticket_status'); ?>" data-container="body"></i>
                                <input class="form-control" type="text" name="category_name"  readonly=""/> 
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label"><?php echo $this->lang->line('global_job_category'); ?></label>
                            <div class="input-icon right">
                                <i class="fa fa-info-circle tooltips" data-original-title="<?php echo $this->lang->line('global_ticket_status'); ?>" data-container="body"></i>
                                <input class="form-control" type="text" name="job_category_name"  readonly=""/> 
                            </div>
                        </div>
                        <div class="form-group">
                            <label><?php echo $this->lang->line('global_content'); ?></label>
                            <textarea class="form-control" rows="3" name="content" readonly=""></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>