<?php

$lang['global_submit'] = 'Submit';
$lang['global_cancel'] = 'Cancel';
$lang['global_open'] = 'Open';
$lang['global_name'] = 'Name';
$lang['global_username'] = 'Username';
$lang['global_first_name'] = 'First Name';
$lang['global_last_name'] = 'Last Name';
$lang['global_id'] = 'ID';
$lang['global_no'] = 'No';
$lang['global_email'] = 'Email';
$lang['global_address'] = 'Address';
$lang['global_password'] = 'Password';
$lang['global_register'] = 'Register';
$lang['global_country'] = 'Country';
$lang['global_state'] = 'State';
$lang['global_city'] = 'City';
$lang['global_area'] = 'Area';
$lang['global_zip_code'] = 'Zip Code';
$lang['global_birth_place'] = 'Birth Place';
$lang['global_birth_day'] = 'Birth Day';
$lang['global_birth_date'] = 'Birth Date';
$lang['global_date'] = 'Date';
$lang['global_month'] = 'Month';
$lang['global_year'] = 'Year';
$lang['global_day'] = 'Day';
$lang['global_mobile_phone'] = 'Mobile Phone';
$lang['global_phone_number'] = 'Phone Number';
$lang['global_update'] = 'Update';
$lang['global_delete'] = 'Delete';
$lang['global_edit'] = 'Edit';
$lang['global_add'] = 'Add';
$lang['global_create'] = 'Create';
$lang['global_title'] = 'Title';
$lang['global_group'] = 'Group';
$lang['global_select'] = 'Select';
$lang['global_send'] = 'Send';
$lang['global_next'] = 'Next';
$lang['global_finish'] = 'Finish';
$lang['global_description'] = 'Description';
$lang['global_ticket'] = 'Ticket';
$lang['global_question'] = 'Question';
$lang['global_answer'] = 'Answer';
$lang['global_level'] = 'Level';
$lang['global_login'] = 'Login';
$lang['global_logout'] = 'Logout';
$lang['global_status'] = 'Status';
$lang['global_first'] = 'First';
$lang['global_second'] = 'Second';
$lang['global_third'] = 'Third';
$lang['global_fourth'] = 'Fourth';
$lang['global_fifth'] = 'Fifth';
$lang['global_image'] = 'Image';
$lang['global_category'] = 'Category';
$lang['global_message'] = 'Message';
$lang['global_page'] = 'Page';
$lang['global_session'] = 'Session';
$lang['global_profile'] = 'Profile';
$lang['global_save'] = 'Save';
$lang['global_quick'] = 'Quick';
$lang['global_language'] = 'Language';
$lang['global_log_in_to_account'] = 'Login to your account';
$lang['global_enter_username_password'] = 'Enter any username and password';
$lang['global_remembered_me'] = 'Remembered Me';
$lang['global_forgot_password'] = 'Forgot your password ?';
$lang['global_click'] = 'Click';
$lang['global_here'] = 'Here';
$lang['global_reset_pass'] = 'to reset your password';
$lang['global_sign_in'] = 'Sign In';
$lang['global_create_ticket'] = 'Create Tickets';
$lang['global_open_form'] = 'Open Form';
$lang['global_view_more'] = 'View more';
$lang['global_progress'] = 'Progress';
$lang['global_close'] = 'Close';
$lang['global_save_changes'] = 'Save changes';
$lang['global_priority'] = 'Priority';
$lang['global_repassword'] = 'Re-Password';

//dashboard datatable tickets
$lang['global_code'] = 'Code';
$lang['global_content'] = 'Content';
$lang['global_vendor_code'] = 'Vendor Code';
$lang['global_vendor_name'] = 'Vendor Name';
$lang['global_create_date'] = 'Create Date';
$lang['global_status'] = 'Status';
$lang['global_active'] = 'Active';
$lang['global_action'] = 'Action';
$lang['global_ticket_code'] = 'Ticket Code';
$lang['global_open_ticket'] = 'Open Ticket';
$lang['global_issue'] = 'Issue';
$lang['global_status'] = 'Status';
$lang['global_view_detail_ticket'] = 'View Detail Ticket';
$lang['global_ticket_create_date'] = 'Ticket Create Date';
$lang['global_ticket_status'] = 'Ticket Status';
$lang['global_branch_code'] = 'Branch Code';
$lang['global_current_ticket'] = 'Current Tickets';

//datatable ticket
$lang['global_no_ticket'] = 'No Ticket';
$lang['global_ticket_list'] = 'Ticket List';
$lang['global_ticket_create'] = 'Create New Ticket';

//report
$lang['global_report_from'] = 'From';
$lang['global_report_to'] = 'To';
$lang['global_report_helpdesk_team'] = 'Helpdesk Team';
$lang['global_report_categories'] = 'Ticket Categories';
$lang['global_report_status'] = 'Ticket Status';
$lang['global_report_priority'] = 'Ticket Priority';
$lang['global_report_title'] = 'Report';
$lang['global_btn_view'] = 'View';
$lang['global_report_category_title'] = 'Ticket Report By Category';
$lang['global_report_date_title'] = 'Ticket Report By Date';
$lang['global_start_date'] = 'Start Date';
$lang['global_end_date'] = 'End Date';
$lang['global_report_filter'] = 'Data Filter';

//dashboard recent activities
$lang['global_recent_activities'] = 'Recent Activities';

//profile
$lang['global_profile_title'] = 'My Profile';
$lang['global_img_upload'] = 'Image Upload';
$lang['global_remove'] = 'Remove';
$lang['global_enter_txt'] = 'Enter Text';
$lang['global_email_address'] = 'Email Address';


//all ticket lang
$lang['global_response_take_open_ticket'] = 'Response and Take an open ticket';
$lang['global_ticket_number'] = 'Ticket Number';
$lang['global_insert_msg_frm_iss_suggest'] = 'Insert message from issue suggestions';
$lang['global_iss_suggest_msg'] = 'Issue Suggestion message';
$lang['global_aggreement'] = 'Aggreement';
$lang['global_aggree_response_ticket'] = 'aggre to take this open ticket, and make a quick response';
$lang['global_job_category'] = 'Sub Category';
$lang['global_ticket_create'] = 'Create New Ticket';
$lang['global_problem_subject'] = 'Problem Subject';
$lang['global_issue_msg'] = 'Please describe your issue here...';
$lang['global_upload_msg'] = 'Drop files here';
$lang['global_or_msg'] = 'or';
$lang['global_select_msg'] = 'Select Files';
$lang['global_'] = '';
$lang['global_'] = '';
$lang['global_'] = '';
$lang['global_'] = '';
$lang['global_'] = '';
$lang['global_'] = '';
$lang['global_'] = '';
$lang['global_'] = '';
$lang['global_'] = '';
$lang['global_'] = '';
$lang['global_'] = '';
$lang['global_'] = '';
$lang['global_'] = '';
$lang['global_'] = '';
$lang['global_'] = '';
$lang['global_'] = '';
$lang['global_'] = '';
$lang['global_'] = '';
$lang['global_'] = '';
$lang['global_'] = '';